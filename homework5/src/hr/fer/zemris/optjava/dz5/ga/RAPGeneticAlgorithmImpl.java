package hr.fer.zemris.optjava.dz5.ga;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import hr.fer.zemris.optjava.dz4.ga.model.IPopulation;
import hr.fer.zemris.optjava.dz4.ga.model.ISelection;
import hr.fer.zemris.optjava.dz4.ga.model.ISingleEntity;


/**
 * Implementation of the Relevant Allele Preservation Genetic Algorithm.
 * 
 * @author vjeran
 * 
 */
public class RAPGeneticAlgorithmImpl {

	private ISelection selection1;
	private ISelection selection2;
	private IPopulation population;
	private double compFactor;
	private int maxPopulation;
	private int minPopulation;
	private int maxEffort;
	
	public RAPGeneticAlgorithmImpl(ISelection selection1, ISelection selection2, IPopulation population,
			double compFactor, int maxPopulation, int minPopulation, int maxEffort) {
		super();
		this.selection1 = selection1;
		this.selection2 = selection2;
		this.population = population;
		this.compFactor = compFactor;
		this.maxPopulation = maxPopulation;
		this.minPopulation = minPopulation;
		this.maxEffort = maxEffort;
	}
	
	public ISingleEntity start() {
		Double evaluation = population.getBestEntity().fitness();
		System.out.println(evaluation + " " + population.getBestEntity());
		while (population.getSize() >= minPopulation) {
			population = buildNewPopulation(population.getSize());
			evaluation = population.getBestEntity().fitness();
			System.out.println(evaluation + " " + population.getBestEntity() + " " + population.getSize());
			compFactor += 1.0*minPopulation/maxPopulation;
		}
		
		return population.getBestEntity();
	}

	private IPopulation buildNewPopulation(Integer size) {
		Set<ISingleEntity> entities = new TreeSet<>();
		// elitism
		entities.add(population.getBestEntity());
		entities.add(population.getNthBestEntity(1));
		int count = 1;
		while(entities.size() < maxPopulation && count++ <= maxEffort) {
			ISingleEntity parent1 = selection1.selectFrom(population);
			ISingleEntity parent2 = selection2.selectFrom(population);
			List<ISingleEntity> children = parent1.crossOver(parent2);
			mutateChildren(children); // mutation
			pickHealthy(children, 
					Math.min(parent1.fitness(), parent2.fitness()), 
					Math.max(parent1.fitness(), parent2.fitness()), 
					entities);
		}
		
		return population.getNewInstance(new ArrayList<>(entities));
	}
	
	private void pickHealthy(List<ISingleEntity> children, Double fit1, Double fit2, Set<ISingleEntity> entities) {
		for (ISingleEntity child : children) {
			if (fit1+compFactor*(fit2 - fit1) <= child.fitness()) {
				entities.add(child);
			}
		}
	}

	private void mutateChildren(List<ISingleEntity> children) {
		for (ISingleEntity child : children) {
			child.mutate(0.0);
		}
	}
	
	
	
	
	
	
	
}
