package hr.fer.zemris.optjava.dz5.part1;

import hr.fer.zemris.optjava.dz4.ga.impl.MaxOnesEntity;
import hr.fer.zemris.optjava.dz4.ga.impl.MaxOnesPopulation;
import hr.fer.zemris.optjava.dz4.ga.impl.TournamentSelection;
import hr.fer.zemris.optjava.dz4.ga.model.IPopulation;
import hr.fer.zemris.optjava.dz4.ga.model.ISelection;
import hr.fer.zemris.optjava.dz5.ga.RAPGeneticAlgorithmImpl;

public class GeneticAlgorithm {
	public static void main(String[] args) {
		Integer vectorLength = Integer.parseInt(args[0]);
		Integer minPopulation = Integer.parseInt(args[1]);
		Integer maxPopulation = Integer.parseInt(args[2]);
		Integer tour1 = Integer.parseInt(args[3]);
		Integer tour2 = Integer.parseInt(args[4]);
		Integer maxEffort = Integer.parseInt(args[5]);
		MaxOnesEntity maxE = new MaxOnesEntity(vectorLength);
		IPopulation population = new MaxOnesPopulation(maxE, maxPopulation);
		
		ISelection selection1 = new TournamentSelection(tour1);
		ISelection selection2 = new TournamentSelection(tour2);
		
		RAPGeneticAlgorithmImpl ga = new RAPGeneticAlgorithmImpl(selection1, 
			selection2, 
			population, 
			0.0, 
			maxPopulation, 
			minPopulation, 
			maxEffort);
		ga.start();
	}
}
