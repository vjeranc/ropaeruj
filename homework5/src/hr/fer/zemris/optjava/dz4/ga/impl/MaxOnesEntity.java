package hr.fer.zemris.optjava.dz4.ga.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import hr.fer.zemris.optjava.dz4.ga.model.ISingleEntity;

public class MaxOnesEntity implements ISingleEntity,Comparable<ISingleEntity>{

	
	private Integer fitness;
	protected boolean[] vector;
	
	private static Random rand = new Random();
	
	public MaxOnesEntity(boolean[] vector) {
		this(calcFitness(vector), vector);
	}
	
	public MaxOnesEntity(Integer fitness, boolean[] vector) {
		super();
		this.fitness = fitness;
		this.vector = vector;
	}
	
	public MaxOnesEntity(Integer n) {
		this(getRandomVector(n));
	}

	@Override
	public Double fitness() {
		if (fitness == null) {
			fitness = calcFitness(vector);
			return fitness();
		} else {
			int n = vector.length;
			if (fitness <= 0.8*n) {
				return 1.0*fitness/n;
			} else if (fitness > 0.8*n && fitness < 0.9*n) {
				return 0.8;
			} else {
				return 2.0*fitness/n - 1;
			}
		}
	}

	private static int calcFitness(boolean[] vector) {
		int counter = 0;
		for (int i = 0; i < vector.length; i++) {
			if (vector[i]) {
				counter++;
			}
		}
		return counter;
	}

	@Override
	public ISingleEntity copy() {
		return new MaxOnesEntity(fitness, Arrays.copyOf(vector, vector.length));
	}

	@Override
	public ISingleEntity randomInstance() {
		return new MaxOnesEntity(getRandomVector(this.vector.length));
	}
	
	private static boolean[] getRandomVector(int n) {
		boolean[] vector = new boolean[n];
		for (int i = 0; i < vector.length; i++) {
			vector[i] = rand.nextBoolean();
		}
		return vector;
	}

	@Override
	public List<ISingleEntity> crossOver(ISingleEntity entity) {
		List<ISingleEntity> children = new ArrayList<>();
		
		int i = rand.nextInt(vector.length);
		MaxOnesEntity parent2 = (MaxOnesEntity) entity;
		boolean[] childVector1 = new boolean[vector.length];
		boolean[] childVector2 = new boolean[vector.length];
		for(int j = 0; j < i; j++) {
			childVector1[j] = this.vector[j];
			childVector2[j] = parent2.vector[j];
		}
		for(int j = i; j < vector.length; j++) {
			childVector2[j] = this.vector[j];
			childVector1[j] = parent2.vector[j];
		}
		children.add(new MaxOnesEntity(childVector1));
		children.add(new MaxOnesEntity(childVector2));
		
		return children;
	}

	@Override
	public void mutate(double sigma) {
		List<Integer> indices = getRandomIndices(5);
		for (Integer i : indices) {
			this.vector[i] = !this.vector[i];
		}
	}

	private List<Integer> getRandomIndices(int n) {
		List<Integer> indices = new ArrayList<>();
		for(int i = 0; i < n; i++) {
			indices.add(rand.nextInt(vector.length));
		}
		return indices;
	}

	@Override
	public Comparator<ISingleEntity> getComparator() {
		return new Comparator<ISingleEntity>() {

			@Override
			public int compare(ISingleEntity o1, ISingleEntity o2) {
				return o1.fitness().compareTo(o2.fitness());
			}
		};
	}


	@Override
	public int compareTo(ISingleEntity o) {
		MaxOnesEntity that = (MaxOnesEntity) o;
		for(int i = 0; i < vector.length; i++) {
			if (this.vector[i] != that.vector[i])
			return rand.nextBoolean() ? 1 : -1;
		}
		return 0;
	}

}
