package hr.fer.zemris.optjava.dz4.ga.model;

import java.util.Comparator;
import java.util.List;

public interface ISingleEntity {

	Double fitness();
	
	ISingleEntity copy();
	
	ISingleEntity randomInstance();

	List<ISingleEntity> crossOver(ISingleEntity entity);
	
	void mutate(double sigma);
	
	Comparator<ISingleEntity> getComparator();
}
