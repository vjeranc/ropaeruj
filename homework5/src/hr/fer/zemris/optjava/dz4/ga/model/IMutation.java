package hr.fer.zemris.optjava.dz4.ga.model;

import java.util.ArrayList;

public interface IMutation {

	<T> void mutate(ArrayList<T> solution);
	
}
