package hr.fer.zemris.optjava.dz4.ga.impl;

import hr.fer.zemris.optjava.dz4.ga.model.IPopulation;
import hr.fer.zemris.optjava.dz4.ga.model.ISingleEntity;

import java.util.ArrayList;
import java.util.List;

public class MaxOnesPopulation extends AbstractPopulation {

	public MaxOnesPopulation(List<ISingleEntity> entities) {
		super(entities);
	}
	
	public MaxOnesPopulation(ISingleEntity e, Integer n) {
		this(getRandomEntities(e, n));
	}

	private static List<ISingleEntity> getRandomEntities(ISingleEntity e, Integer n) {
		List<ISingleEntity> entities = new ArrayList<>();
		for(int i = 0; i < n; i++) {
			entities.add(e.randomInstance());
		}
		
		return entities;
	}

	@Override
	public IPopulation getNewInstance(List<ISingleEntity> entities) {
		return new MaxOnesPopulation(entities);
	}

}
