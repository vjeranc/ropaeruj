package hr.fer.zemris.optjava.dz4.ga.model;

public interface ISteadyPopulation extends IPopulation {

	void setEntity(int i, ISingleEntity entity);
	
}
