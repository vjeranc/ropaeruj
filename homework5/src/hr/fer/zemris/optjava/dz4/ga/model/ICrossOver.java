package hr.fer.zemris.optjava.dz4.ga.model;

import java.util.List;

public interface ICrossOver {

	List<ISingleEntity> crossOver(ISingleEntity entity1, ISingleEntity entity2);
	
}
