package hr.fer.zemris.optjava.dz7.pso;

import java.util.Arrays;

import hr.fer.zemris.optjava.nnet.IFunction;

public class Particle {

	private int dim;
	private double[] personalBest;
	private double[] current;
	private double[] velocity;

	private Double persBestVal;
	private Double currentVal;
	/**
	 * Constructs a particle.
	 * 
	 * @param function
	 *            to be minimized
	 * @param n
	 *            dimension of the input vector
	 */
	public Particle(int n) {
		this.dim = n;
		this.current = random(new double[n]);
		this.velocity = new double[n];
		this.personalBest = Arrays.copyOf(current, current.length);
	}
	
	private Particle(double[] personalBest, double[] current, double[] velocity){
		this.personalBest = personalBest;
		this.current = current;
		this.velocity = velocity;
	}

	public Double evaluate(IFunction function) {
		if (currentVal == null) {
			currentVal = function.calculate(current);
		}
		if (persBestVal != null) {
			if (currentVal < persBestVal) {
				persBestVal = currentVal;
				personalBest = Arrays.copyOf(current, current.length);
			}
		} else {
			persBestVal = currentVal;
		}
		return persBestVal;
	}
	
	public double[] getSolution() {
		return personalBest;
	}
	
	
	public void update(double inertialWeight, double c1, double c2, Particle best) {
		for (int i = 0; i < velocity.length; i++) {
			velocity[i] = inertialWeight*velocity[i] + 
					c1*Math.random()*(personalBest[i]-current[i])+
					c2*Math.random()*(best.personalBest[i]-current[i]);
		}
		for (int i = 0; i < current.length; i++) {
			current[i] += velocity[i];
		}
		currentVal = null;
	}
	
	public Particle copy() {
		return new Particle(
				Arrays.copyOf(personalBest, dim), 
				Arrays.copyOf(current, dim), 
				Arrays.copyOf(velocity, dim));
	}

	private static double[] random(double[] arr) {
		for (int i = 0; i < arr.length; i++) {
			arr[i] = Math.random();
		}
		return arr;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return persBestVal.toString();
	}
}
