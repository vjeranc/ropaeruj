package hr.fer.zemris.optjava.dz7.pso;

import hr.fer.zemris.optjava.nnet.IFunction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LocalRingPSO extends ParticleSwarmOptimization {
	private int numOfNeighbors;
	
	
	public LocalRingPSO(double c1, double c2, double merr, int popSize, int maxiter, double wmin, double wmax,
			IFunction function, int numberOfNeighbors) {
		super(c1, c2, merr, popSize, maxiter, wmin, wmax, function);
		this.numOfNeighbors = numberOfNeighbors;
	}

	

	@Override
	public void changePopulation(int t) {
		int i = 0;
		for (Particle p : particles) {
			Particle bestNeighbor = getBestNeighbor(p, i);
			p.update(getWeight(t), c1, c2, bestNeighbor);
			i++;
		}

	}

	private Particle getBestNeighbor(Particle p, int ind) {
		List<Particle> neighbors = new ArrayList<>();
		for (int i = -numOfNeighbors; i < numOfNeighbors; i++) {
			int neighborIndex = (ind + i + getPopulationSize()) % getPopulationSize();
			neighbors.add(particles.get(neighborIndex));
		}

		return Collections.min(neighbors, comp);
	}

}
