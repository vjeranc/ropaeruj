package hr.fer.zemris.optjava.dz7.pso;

import hr.fer.zemris.optjava.nnet.IFunction;

public class GlobalBestPSO extends ParticleSwarmOptimization {

	public GlobalBestPSO(double c1, double c2, double merr, int popSize, int maxiter, double wmin, double wmax,
			IFunction function) {
		super(c1, c2, merr, popSize, maxiter, wmin, wmax, function);
	}

	@Override
	public void changePopulation(int t) {
		for (Particle p : particles) {
			p.update(getWeight(t), c1, c2, best);

		}

	}

}
