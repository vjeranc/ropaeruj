package hr.fer.zemris.optjava.dz7.pso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import hr.fer.zemris.optjava.nnet.IFunction;

public abstract class ParticleSwarmOptimization {
	
	/** Constants for the speed calculation and minimum error needed for the termination. */
	protected double c1;

	protected double c2;

	private double merr;
	
	/** Population size. */
	protected int popSize;
	
	/** Maximum number of iterations. */
	private int maxiter; 

	/** Weight bounds. */
	private double wmin, wmax;
	
	protected Particle best;
	/** Function which is being minimized. */
	final private IFunction function;

	protected List<Particle> particles;
	
	protected Comparator<Particle> comp;
	
	public ParticleSwarmOptimization(double c1, double c2, double merr, int popSize, int maxiter, double wmin,
			double wmax, IFunction function1) {
		this.c1 = c1;
		this.c2 = c2;
		this.merr = merr;
		this.popSize = popSize;
		this.maxiter = maxiter;
		this.wmin = wmin;
		this.wmax = wmax;
		this.function = function1;
		this.particles = new ArrayList<>();
		for (int i = 0; i < popSize; i++) {
			particles.add(new Particle(function.getInputDim()));
		}
		
		this.comp = new Comparator<Particle>() {

			@Override
			public int compare(Particle o1, Particle o2) {
				return o1.evaluate(function).compareTo(o2.evaluate(function));
			}
		};
	}

	public double getWeight(int t) {
		return t < maxiter ? 1.0 * t/maxiter * (wmin - wmax) + wmax : wmin;
	}
	
	public double[] start() {
		int iter = 0;
		best = Collections.min(particles, comp);
		while(iter++ <= maxiter && best.evaluate(function) > merr) {
			for (Particle p : particles) {
				p.evaluate(function);
			}
			best = Collections.min(particles, comp).copy();
			changePopulation(iter);
		}
		
		return best.getSolution();
	}

	abstract public void changePopulation(int iteration);
	
	public int getPopulationSize() {
		return popSize;
	}
	
	
}
