package hr.fer.zemris.optjava.dz7.clonalg;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import hr.fer.zemris.optjava.dz7.pso.Particle;
import hr.fer.zemris.optjava.nnet.IFunction;

public class ClonalSelectionAlgorithm {
	
	
	private int n;
	private int d;
	private int maxiter;
	private double merr;
	private double beta;
	private Comparator<Antigen> comp;
	private IFunction function;
	private Antigen best;
	private List<Antigen> antigens;
	
	public ClonalSelectionAlgorithm(Antigen antigen, int n, int d, double beta, double rho, final IFunction function, int maxiter, double merr) {
		this.n = n;
		this.d = d;
		this.beta = beta;
		this.function = function;
		this.maxiter = maxiter;
		this.merr = merr;
		
		this.antigens = new ArrayList<>();
		
		this.comp = new Comparator<Antigen>() {

			@Override
			public int compare(Antigen o1, Antigen o2) {
				return o1.evaluate(function).compareTo(o2.evaluate(function));
			}
		};
	}
	
	public double[] start() {
		int iter = 0;
		best = Collections.min(antigens, comp);
		while(iter++ < maxiter && best.evaluate(function) > merr) {
			
		}
		
		return best.getSolution();
	}
}
