package hr.fer.zemris.optjava.dz7.clonalg;

import hr.fer.zemris.optjava.nnet.IFunction;

public class Antigen {

	private double[] vector;
	private Double value;
	
	public Antigen(int dim) {
		this.vector = random(new double[dim]);
	}
	
	public Double evaluate(IFunction function) {
		if (value == null) {
			value = function.calculate(vector);
		}
		return value;
	}
	
	public void hypermutate(IFunction function) {
		if (value == null) {
			evaluate(function);
		}
		
	}
	
	public double[] getSolution() {
		return vector;
	}
	
	private static double[] random(double[] arr) {
		for (int i = 0; i < arr.length; i++) {
			arr[i] = Math.random();
		}
		return arr;
	}
}
