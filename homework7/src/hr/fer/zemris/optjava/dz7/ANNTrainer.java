package hr.fer.zemris.optjava.dz7;

import hr.fer.zemris.optjava.dz7.clonalg.Antigen;
import hr.fer.zemris.optjava.dz7.clonalg.ClonalSelectionAlgorithm;
import hr.fer.zemris.optjava.dz7.pso.GlobalBestPSO;
import hr.fer.zemris.optjava.dz7.pso.LocalRingPSO;
import hr.fer.zemris.optjava.dz7.pso.ParticleSwarmOptimization;
import hr.fer.zemris.optjava.nnet.FFANN;
import hr.fer.zemris.optjava.nnet.IFunction;
import hr.fer.zemris.optjava.nnet.ITransferFunction;
import hr.fer.zemris.optjava.nnet.SigmoidTransferFunction;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.OperationNotSupportedException;

public class ANNTrainer {
	
	private static Pattern nums = Pattern.compile("(\\d+\\.\\d+)|(\\d+)");
	private static IFunction sqrerr;
	
	public static void main(String[] args) throws IOException, OperationNotSupportedException {
		final List<Tuple<double[], double[]>> input = readData(args[0]);
		Collections.shuffle(input);
		String algorithm = args[1];
		Integer popSize = Integer.parseInt(args[2]);
		Double minErr = Double.parseDouble(args[3]);
		Integer maxIter = Integer.parseInt(args[4]);
		
		final int in = input.get(0).getX().length;
		final int out = input.get(0).getY().length;
		final FFANN neuralNetwork = new FFANN(new int[] {in,7,out}, new ITransferFunction[]{
				new SigmoidTransferFunction()
				,new SigmoidTransferFunction()
				//,new SigmoidTransferFunction()
				//,new SigmoidTransferFunction()
				//,new SigmoidTransferFunction()
				//,new SigmoidTransferFunction()
		});
		
		sqrerr = new IFunction() {
			
			@Override
			public double calculate(double[] x) {
				double[] outputs = new double[out];
				double sum = 0.0;
				for (Tuple<double[], double[]> tup : input.subList(0, input.size())) {
					neuralNetwork.calcOutputs(tup.getX(), x, outputs);
					double[] y = tup.getY();
					for (int i = 0; i < outputs.length; i++) {
						sum += (y[i]-outputs[i])*(y[i]-outputs[i]);
					}
				}
				return sum/input.size();
			}

			@Override
			public int getInputDim() {
				return neuralNetwork.getWeightsCount();
			}
		};
		double[] solution = null;
		switch (algorithm.substring(0, 3)) {
		case "clo":
			ClonalSelectionAlgorithm csa = new ClonalSelectionAlgorithm(
					new Antigen(neuralNetwork.getWeightsCount()), popSize, popSize/5, 0.5, 0.3, sqrerr, maxIter, minErr);
		case "pso": {
			ParticleSwarmOptimization pso;
			if (algorithm.contains("a")) {
				pso = new GlobalBestPSO(2, 2, minErr, popSize, maxIter, 0.1, 0.6, sqrerr);
			} else {
				// number of neighbors
				Integer nborNum = Integer.parseInt(algorithm.substring(algorithm.lastIndexOf('-') + 1));
				pso = new LocalRingPSO(2, 2, minErr, popSize, maxIter, 0.1, 0.7, sqrerr, nborNum);
			}
			solution = pso.start();
		}
		}
		
		printClassificationResults(solution, neuralNetwork, input);
	}

	private static void printClassificationResults(double[] solution, FFANN neuralNetwork, List<Tuple<double[], double[]>> input) {
		int d = neuralNetwork.getNumberOfOutputs();
		
		int correct = 0;
		System.out.println("Netočno klasificirani uzorci: ");
		
		for (Tuple<double[], double[]> tuple : input) {
			double[] outputs = new double[d];
			neuralNetwork.calcOutputs(tuple.getX(), solution, outputs);
			for (int i = 0; i < outputs.length; i++) {
				outputs[i] = outputs[i] > 0.5 ? 1.0 : 0.0;
			}
			double[] realOut = tuple.getY();
			for(int i = 0; i < outputs.length; i++) {
				if (outputs[i] != realOut[i]) {
					System.out.print(Arrays.toString(tuple.getX()) + "\t");
					System.out.println(printArray(outputs) + "\t" + printArray(realOut));
					correct--;
					break;
				}
			}
			correct++;
		}

		System.out.println("Točno klasificiranih uzoraka: " + correct);
		System.out.println("Pogreška: " + sqrerr.calculate(solution));
		System.out.println("Težine: " + Arrays.toString(solution));
		
	}

	private static String printArray(double[] outputs) {
		int[] arr = new int[outputs.length];
		for(int i = 0; i < outputs.length; i++)	{
			arr[i] = (int) outputs[i];
		}
		
		return Arrays.toString(arr);
	}

	private static List<Tuple<double[], double[]>> readData(String string) throws IOException {
		List<String> lines = Files.readAllLines(Paths.get(string), StandardCharsets.UTF_8);
		List<Tuple<double[], double[]>> list = new ArrayList<>();
		for (String line : lines) {
			Tuple<double[], double[]> as = parseLine(line);
			list.add(as);
		}
		return list;
	}
	
	private static double[] parseVector(String vector) {
		Matcher input = nums.matcher(vector);
		List<Double> helper = new ArrayList<>();
		while(input.find()) {
			Double comp = Double.parseDouble(input.group());
			helper.add(comp);
		}
		double[] arr = new double[helper.size()];
		for(int i = 0; i < arr.length; i++) {
			arr[i] = helper.get(i);
		}
		
		return arr;
	}

	private static Tuple<double[], double[]> parseLine(String line) {
		String[] vectors = line.split(":");
		double[] in = parseVector(vectors[0]);
		double[] out = parseVector(vectors[1]);
		
		return new Tuple<double[], double[]>(in, out);
	}
}
