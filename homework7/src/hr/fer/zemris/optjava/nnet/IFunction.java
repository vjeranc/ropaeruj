package hr.fer.zemris.optjava.nnet;

public interface IFunction {

	double calculate(double[] x);

	int getInputDim();
}
