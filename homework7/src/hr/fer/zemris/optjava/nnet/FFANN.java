package hr.fer.zemris.optjava.nnet;

public class FFANN {


	int[] layers;
	int weightsCount;
	int outputCount;
	ITransferFunction[] transferFunctions;
	int nodes;
	
	public FFANN(int[] layers, ITransferFunction[] transferFunctions) {
		if(layers.length-1 != transferFunctions.length) {
			throw new IllegalArgumentException("Number of transfer functions must be equal to the number of hidden layers + 1!");
		}
		this.layers = layers;
		int n = layers.length;
		int edgeWeights = 0, outputs = layers[0], nodeWeights = 0;
		for(int i = 1; i < n; i++) {
			edgeWeights += layers[i-1]*layers[i];
			outputs += layers[i];
			nodeWeights += layers[i];
		}
		this.outputCount = outputs;
		this.weightsCount = edgeWeights+nodeWeights;
		this.nodes = nodeWeights;
		this.transferFunctions = transferFunctions;
	}
	
	public int getNumberOfOutputs() {
		return layers[layers.length-1];
	}
	
	public int getWeightsCount() {
		return weightsCount;
	}
	
	/**
	 * Not good for parallelization.
	 * @param inputs input vector (zeroth layer)
	 * @param weights array of weights of the neural network containing node weights and edge weights
	 * @param outputs output vector (last layer)
	 */
	public void calcOutputs(double[] inputs, double[] weights, double[] outputs) {
		double[] out = new double[outputCount];
		for(int i = 0; i < inputs.length; i++){
			out[i] = inputs[i];
		}
		int outIndex = layers[0];
		int ewInd = nodes; // index of edge weights, starts after node weights
		int nwInd = 0; // index of node weights
		for(int i = 1; i < layers.length; i++) {
			int prevOuts = outIndex-layers[i-1];
			for(int j = 0; j < layers[i]; j++) {
				// calculating the weighted sum
				for(int k = 0; k < layers[i-1]; k++) {
					out[outIndex] += weights[ewInd+k]*out[prevOuts+k];
				}
				out[outIndex] += weights[nwInd]; nwInd++;
				out[outIndex] = transferFunctions[i-1].calculate(out[outIndex]);
				outIndex++;
				ewInd += layers[i-1]; // skip over k weights of one input node
			}
		}
		// copying the real output
		int n = out.length;
		for(int i = outputs.length-1; i >= 0; i--) {
			--n;
			outputs[i] = out[n];
		}
	}
	
}
