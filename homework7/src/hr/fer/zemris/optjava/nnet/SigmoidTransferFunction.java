package hr.fer.zemris.optjava.nnet;

public class SigmoidTransferFunction implements ITransferFunction {

	@Override
	public double calculate(double x) {
		return 1/(1 + Math.exp(-x));
	}

}
