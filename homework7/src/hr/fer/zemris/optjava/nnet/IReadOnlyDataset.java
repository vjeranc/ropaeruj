package hr.fer.zemris.optjava.nnet;

public interface IReadOnlyDataset {

	int numberOfSamples();
}
