package hr.fer.zemris.optjava.dz8.de;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import hr.fer.zemris.optjava.nnet.IFunction;

public class DifferentialEvolution {
	
	private class Vector {
		double[] vector;
		Double eval;
		public Vector(int size, boolean random) {
			super();
			this.vector = new double[size];
			if (random) randomArr(vector);
		}
		public Vector(double[] trialField) {
			this.vector = trialField;
		}
		private void randomArr(double[] vector2) {
			for(int i = 0; i < vector2.length; i++) {
				vector2[i] = Math.random();
			}
		}
		
		public double get(int i) {
			return vector[i];
		}
		
		public double[] getArr() {
			return Arrays.copyOf(vector, vector.length);
		}
		
		public Double evaluate(IFunction function) {
			if (eval == null) {
				eval = function.calculate(vector);
			}
			return eval;
		}
		
	}
	
	/** Some weird constant. */
	private double F;
	/** Crossover probability. */
	private double Cr;
	/** Population size. */
	private int Np;
	
	private List<Vector> x;
	
	private IFunction function;
	/** Dimension of the input vector */
	int D;
	private int maxiter;
	private double merr;
	private Vector best;
	private Comparator<Vector> comp;

	public DifferentialEvolution(double f, double cr, int np, final IFunction function, int maxiter, double merr) {
		super();
		F = f;
		Cr = cr;
		Np = np;
		this.function = function;
		D = function.getInputDim();
		this.maxiter = maxiter;
		this.merr = merr;
		this.x = new ArrayList<>();
		for(int i = 0; i < Np; i++) {
			x.add(new Vector(D, true));
		}
		
		this.comp = new Comparator<DifferentialEvolution.Vector>() {
			
			@Override
			public int compare(Vector o1, Vector o2) {
				return o1.evaluate(function).compareTo(o2.evaluate(function));
			}
		};
	}
	
	private Random rand = new Random();
	
	/**
	 * Starts the algorithm and returns the solution.
	 * @return solution vector in a double array form
	 */
	public double[] start() {
		//TODO instead of current crossover and selection strategy allow for different strategies
		best = Collections.min(x, comp);
		int iter = 0;
		while(iter++ <= maxiter && best.evaluate(function) > merr) {
			List<Vector> trial = new ArrayList<>(Np);
			
			for (int i = 0; i < Np; i++) {
				int r0, r1, r2, jrand;
				do {r0 = rand.nextInt(Np);} while (r0 == i);
				do {r1 = rand.nextInt(Np);} while (r1 == i || r1 == r0);
				do {r2 = rand.nextInt(Np);} while (r2 == i || r2 == r1 || r2 == r0);
				
				jrand = rand.nextInt(D);
				double[] trialField = new double[D]; // construct trial vector
				for(int j = 0; j < D; j++) {
					if (Math.random() < Cr || j == jrand) {
						trialField[j] = x.get(r0).get(j) + F*(x.get(r1).get(j) - x.get(r2).get(j));
						// u_{j, i} = x_{j, r0} + F * (x_{j, r1} - x_{j, r2})
					} else {
						trialField[j] = x.get(i).get(j); // u_{j, i} = x_{j, i}
					}
				}
				trial.add(i, new Vector(trialField));
			}
			
			// select the next generation
			for(int i = 0; i < Np; i++) {
				if (comp.compare(x.get(i), trial.get(i)) >= 0) {
					x.set(i, trial.get(i));
				}
			}
			
			best = Collections.min(x, comp);
			if (iter % 100 == 0) System.out.println(best.evaluate(function));
		}
		
		return best.getArr();
	}
	
	
}
