package hr.fer.zemris.optjava.dz8;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.naming.OperationNotSupportedException;

import hr.fer.zemris.optjava.dz8.de.DifferentialEvolution;
import hr.fer.zemris.optjava.nnet.ElmanNN;
import hr.fer.zemris.optjava.nnet.FFANN;
import hr.fer.zemris.optjava.nnet.HyperTangensTransferFunction;
import hr.fer.zemris.optjava.nnet.IFunction;
import hr.fer.zemris.optjava.nnet.ITransferFunction;
import hr.fer.zemris.optjava.nnet.NeuralNetwork;

public class ANNTrainer {

	private static IFunction sqrerr;

	public static void main(String[] args) throws IOException, OperationNotSupportedException {
		List<Double> data = normalizeData(readData(args[0]));
		Tuple<String, int[]> algNArh = parseAlgNArh(args[1]);
		String alg = algNArh.getX();
		int[] layers = algNArh.getY();
		final List<Tuple<double[], double[]>> input = prepareInput(data, layers[0], 600);
		Integer popSize = Integer.parseInt(args[2]);
		Double minErr = Double.parseDouble(args[3]);
		Integer maxIter = Integer.parseInt(args[4]);

		final int out = input.get(0).getY().length;
		
		ITransferFunction[] tfuncs = new ITransferFunction[layers.length-1];
		for (int i = 0; i < tfuncs.length; i++) {
			tfuncs[i] = new HyperTangensTransferFunction();
		}
		
		final NeuralNetwork NN = alg.equals("tdnn") ? new FFANN(layers, tfuncs) : new ElmanNN(layers, tfuncs);

		sqrerr = new IFunction() {

			@Override
			public double calculate(double[] x) {
				double[] outputs = new double[out];
				double sum = 0.0;
				NN.resetState(); // needed if elman
				for (Tuple<double[], double[]> tup : input) {
					NN.calcOutputs(tup.getX(), x, outputs);
					double[] y = tup.getY();
					for (int i = 0; i < outputs.length; i++) {
						sum += (y[i] - outputs[i]) * (y[i] - outputs[i]);
					}
				}
				return sum / input.size();
			}

			@Override
			public int getInputDim() {
				return NN.getWeightsCount();
			}
		};
		
		DifferentialEvolution de = new DifferentialEvolution(0.31, 0.15, popSize, sqrerr, maxIter, minErr);
		
		
		double[] solution = de.start();
		printClassificationResults(solution, NN, input);
	}

	private static List<Double> normalizeData(List<Double> data) {
		Double max = Collections.max(data);
		Double min = Collections.min(data);
		List<Double> ndata = new ArrayList<>();
		for (Double d : data) {
			ndata.add(2/(max-min) * (d - min) - 1);
		}
		return ndata;
	}

	private static List<Tuple<double[], double[]>> prepareInput(List<Double> data, int in, int n) {
		List<Tuple<double[], double[]>> tdata = new ArrayList<>();
		for(int i = 0; i < n; i++) {
			double[] inputV = toDoubleArray(data.subList(i, i+in));
			double[] outV = toDoubleArray(data.subList(i+in, i+in+1));
			tdata.add(new Tuple<double[], double[]>(inputV, outV));
		}
		return tdata;
	}

	private static double[] toDoubleArray(List<Double> subList) {
		double[] arr = new double[subList.size()];
		int i = 0;
		for (Double d : subList) {
			arr[i] = d.doubleValue();
			i++;
		}
		return arr;
	}
	
	private static void printClassificationResults(double[] solution, NeuralNetwork nn,
			List<Tuple<double[], double[]>> input) {
		int d = nn.getNumberOfOutputs();

		int correct = 0;
		System.out.println("Netočno klasificirani uzorci: ");

		for (Tuple<double[], double[]> tuple : input) {
			double[] outputs = new double[d];
			nn.calcOutputs(tuple.getX(), solution, outputs);
			for (int i = 0; i < outputs.length; i++) {
				outputs[i] = outputs[i] > 0.5 ? 1.0 : 0.0;
			}
			double[] realOut = tuple.getY();
			for (int i = 0; i < outputs.length; i++) {
				if ((outputs[i] - realOut[i]) < 1E-6) {
					System.out.print(Arrays.toString(tuple.getX()) + "\t");
					System.out.println(printArray(outputs) + "\t" + printArray(realOut));
					correct--;
					break;
				}
			}
			correct++;
		}

		System.out.println("Točno klasificiranih uzoraka: " + correct);
		System.out.println("Pogreška: " + sqrerr.calculate(solution));
		System.out.println("Težine: " + Arrays.toString(solution));

	}

	private static String printArray(double[] outputs) {
		int[] arr = new int[outputs.length];
		for (int i = 0; i < outputs.length; i++) {
			arr[i] = (int) outputs[i];
		}

		return Arrays.toString(arr);
	}

	private static List<Double> readData(String string) throws IOException {
		List<String> lines = Files.readAllLines(Paths.get(string), StandardCharsets.UTF_8);
		List<Double> list = new ArrayList<>();
		for (String line : lines) {
			list.add(Double.parseDouble(line));
		}
		return list;
	}

	private static Tuple<String, int[]> parseAlgNArh(String string) {
		String[] kk = string.split("-");
		String[] arh = kk[1].split("x");
		int[] layers = new int[arh.length];
		for (int i = 0; i < arh.length; i++) {
			layers[i] = Integer.parseInt(arh[i]);
		}
		return new Tuple<String, int[]>(kk[0], layers);
	}
}
