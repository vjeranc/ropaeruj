package hr.fer.zemris.optjava.nnet;

public abstract class NeuralNetwork {
	public abstract void calcOutputs(double[] inputs, double[] weights, double[] outputs);
	public abstract int getWeightsCount();
	public abstract int getNumberOfOutputs();
	public abstract void resetState();
}
