package hr.fer.zemris.optjava.nnet;

public class ElmanNN extends NeuralNetwork {
	int[] layers;
	int weightsCount;
	int outputCount;
	ITransferFunction[] transferFunctions;
	int nodes;
	
	double[] elmanNodes;

	public ElmanNN(int[] layers, ITransferFunction[] transferFunctions) {
		if (layers.length - 1 != transferFunctions.length) {
			throw new IllegalArgumentException(
					"Number of transfer functions must be equal to the number of hidden layers + 1!");
		}
		this.layers = layers;
		layers[0] += layers[1]; // elman nodes
		int n = layers.length;
		int edgeWeights = 0, outputs = layers[0], nodeWeights = 0;
		for (int i = 1; i < n; i++) {
			edgeWeights += layers[i - 1] * layers[i];
			outputs += layers[i];
			nodeWeights += layers[i];
		}
		this.outputCount = outputs;
		this.weightsCount = edgeWeights + nodeWeights;
		this.nodes = nodeWeights;
		this.transferFunctions = transferFunctions;
		this.elmanNodes = new double[layers[1]]; // first hidden layer length, all initialized to 0
	}

	@Override
	public int getNumberOfOutputs() {
		return layers[layers.length - 1];
	}

	@Override
	public int getWeightsCount() {
		return weightsCount;
	}

	/**
	 * Not good for parallelization.
	 * 
	 * @param inputs
	 *            input vector (zeroth layer)
	 * @param weights
	 *            array of weights of the neural network containing node weights
	 *            and edge weights
	 * @param outputs
	 *            output vector (last layer)
	 */
	@Override
	public void calcOutputs(double[] inputs, double[] weights, double[] outputs) {
		double[] out = new double[outputCount];
		for (int i = 0; i < inputs.length; i++) {
			out[i] = inputs[i];
		}
		// elmanNodes contain previous layer[1] outputs, we're copying them too
		for(int i = 0; i < elmanNodes.length; i++) {
			out[i+inputs.length] = elmanNodes[i];
		}
		int outIndex = layers[0];
		int ewInd = nodes; // index of edge weights, starts after node weights
		int nwInd = 0; // index of node weights
		for (int i = 1; i < layers.length; i++) {
			int prevOuts = outIndex - layers[i - 1];
			for (int j = 0; j < layers[i]; j++) {
				// calculating the weighted sum
				for (int k = 0; k < layers[i - 1]; k++) {
					out[outIndex] += weights[ewInd + k] * out[prevOuts + k];
				}

				
				out[outIndex] += weights[nwInd];
				nwInd++;
				out[outIndex] = transferFunctions[i - 1]
						.calculate(out[outIndex]);
				outIndex++;
				ewInd += layers[i - 1]; // skip over k weights of one input node
			}
			if (i == 1) {
				int k = outIndex - layers[1];
				for (int j = 0; j < layers[1]; j++) {
					elmanNodes[j] = out[k+j];
				}
			}
		}
		// copying the real output
		int n = out.length;
		for (int i = outputs.length - 1; i >= 0; i--) {
			--n;
			outputs[i] = out[n];
		}
	}
	
	@Override
	public void resetState() {
		this.elmanNodes = new double[elmanNodes.length];
	}
}
