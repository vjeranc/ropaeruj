package hr.fer.zemris.optjava.nnet;

public interface ITransferFunction {
	
	double calculate(double x);
	
}
