package hr.fer.zemris.optjava.nnet;

public class HyperTangensTransferFunction implements ITransferFunction {

	@Override
	public double calculate(double x) {
		return (1-Math.exp(x))/(1+Math.exp(x));
	}

}
