package hr.fer.zemris.optjava.dz13.gui;

import hr.fer.zemris.optjava.dz13.data.impl.AntState.Orientation;

import javax.swing.*;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;

public class FoodBoard extends JPanel {

	private static final long serialVersionUID = -2871268857284720831L;

	private byte[][] board;
	private int width; // board width
	private int height; // board height

	// constants describing state of board cells
	public static final byte EMPTY = 0; // empty cell
	public static final byte FOOD = 1; // contains food
	public static final byte ANT = 2; // contains food
	private Orientation orientation = Orientation.DOWN;
	private List<String> moves;
	int currentMove = 0;
	int i = 0, j = 0;
	
	public FoodBoard(boolean[][] map, List<String> moves) {
		this.width = map[0].length;
		this.height =  map.length;
		this.board = new byte[height][width];
		for(int i = 0; i < height; i++) {
			for(int j = 0; j < width; j++) {
				if (map[i][j]) {
					board[i][j] = FOOD;
				} else {
					board[i][j] = EMPTY;
				}
			}
		}
		this.moves = moves;
		board[i][j] = ANT;

		setBackground(Color.gray);
		setSize(400, 400);
		
	}

	public void doNextMove() {
		if (currentMove == moves.size())
			return;
		
		switch (moves.get(currentMove++)) {
		case "Move":
			move(); break;
		case "Right":
			turnRight(); break;
		case "Left":
			turnLeft(); break;
		}
		
	}

	
	/** 
	 * Moves ant forward.
	 */
	public void move() {
		board[i][j] = EMPTY;
		switch (orientation) {
		case UP:
			i = (i + height - 1) % height;
			break;
		case DOWN:
			i = (i + 1) % height;
			break;
		case LEFT:
			j = (j + width - 1) % width;
			break;
		case RIGHT:
			j = (j + 1) % width;
			break;
		}
		board[i][j] = ANT;
	}
	
	public void turnLeft() {
		switch (orientation) {
		case UP:
			orientation = Orientation.LEFT;
			break;
		case DOWN:
			orientation = Orientation.RIGHT;
			break;
		case LEFT:
			orientation = Orientation.DOWN;
			break;
		case RIGHT:
			orientation = Orientation.UP;
			break;
		}
	}
	
	public void turnRight() {
		switch (orientation) {
		case UP:
			orientation = Orientation.RIGHT;
			break;
		case DOWN:
			orientation = Orientation.LEFT;
			break;
		case LEFT:
			orientation = Orientation.UP;
			break;
		case RIGHT:
			orientation = Orientation.DOWN;
			break;
		}
	}
	/**
	 * tells how many cells high this board is
	 * 
	 * @return board height
	 */
	public int getBoardHeight() {
		return height;
	}

	/**
	 * tells how many cells wide this board is
	 * 
	 * @return board width
	 */
	public int getBoardWidth() {
		return width;
	}

	/**
	 * for each cell in the board, draw necessary shapes for the marker type.
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Dimension d = getSize();
		int cellwidth = d.width / width;
		int cellheight = d.height / height;
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < width; y++) {
				if (board[x][y] == EMPTY) {
					// do nothing, but avoid evaluating further conditions
				} else if (board[x][y] == FOOD) {
					drawFood(g, x * cellwidth, y * cellheight, cellwidth, cellheight);
				} else if (board[x][y] == ANT) {
					drawAnt(g, x * cellwidth, y * cellheight, cellwidth, cellheight);
				}
			}
		}
	}

	private void drawFood(Graphics g, int x, int y, int width, int height) {
		g.setColor(Color.BLACK);
		g.fillOval(x, y, width - 1, height - 1);
	}

	private void drawAnt(Graphics g, int x, int y, int width, int height) {
		g.setColor(Color.red);
		g.fillOval(x, y, width-2, height-2);
	}
}