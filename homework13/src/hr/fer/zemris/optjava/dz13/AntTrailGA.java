package hr.fer.zemris.optjava.dz13;

import hr.fer.zemris.optjava.dz13.data.impl.AntGUIState;
import hr.fer.zemris.optjava.dz13.data.impl.AntState;
import hr.fer.zemris.optjava.dz13.data.impl.IfFoodAhead;
import hr.fer.zemris.optjava.dz13.data.impl.Left;
import hr.fer.zemris.optjava.dz13.data.impl.Move;
import hr.fer.zemris.optjava.dz13.data.impl.Prog2;
import hr.fer.zemris.optjava.dz13.data.impl.Prog3;
import hr.fer.zemris.optjava.dz13.data.impl.Right;
import hr.fer.zemris.optjava.dz13.data.impl.AntState.Orientation;
import hr.fer.zemris.optjava.dz13.data.model.Function;
import hr.fer.zemris.optjava.dz13.data.model.SyntaxTree;
import hr.fer.zemris.optjava.dz13.data.model.Terminal;
import hr.fer.zemris.optjava.dz13.gp.impl.AntEvaluator;
import hr.fer.zemris.optjava.dz13.gp.impl.AntSolution;
import hr.fer.zemris.optjava.dz13.gp.impl.SubTreeMutation;
import hr.fer.zemris.optjava.dz13.gp.impl.SyntaxTreeCrossOver;
import hr.fer.zemris.optjava.dz13.gp.impl.TournamentSelection;
import hr.fer.zemris.optjava.dz13.gp.model.GP;
import hr.fer.zemris.optjava.dz13.gp.model.GPSolution;
import hr.fer.zemris.optjava.dz13.gp.model.ICrossOver;
import hr.fer.zemris.optjava.dz13.gp.model.IMutation;
import hr.fer.zemris.optjava.dz13.gp.model.ISelection;
import hr.fer.zemris.optjava.dz13.gui.FoodBoard;

import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

public class AntTrailGA {
	public static int maxNodes = 200;
	public static int startDepth = 6;
	public static int maxDepth = 20;
	public static int tournamentSize = 7;
	public static int maxMoves = 600;
	public static int i = 0, j = 0;
	private static double mutationProb = 0.14;
	private static double recombinationProb = 0.01;
	private static double crossoverProb = 1;

	public static List<Function<AntState>> functions = Arrays.asList(new IfFoodAhead(), new Prog2(), new Prog3());
	public static List<Terminal<AntState>> terminals = Arrays.asList(new Move(), new Left(), new Right());

	public static void main(String[] args) throws IOException {

		final boolean[][] map = parseMap(Files.readAllLines(Paths.get(args[0]), StandardCharsets.UTF_8));
		Integer maxIter = Integer.parseInt(args[1]);
		Integer popSize = Integer.parseInt(args[2]);
		Integer maxFitnessBound = Integer.parseInt(args[3]);

		List<Function<AntState>> functions = Arrays.asList(new IfFoodAhead(), new Prog2(), new Prog3());
		List<Terminal<AntState>> terminals = Arrays.asList(new Move(), new Left(), new Right());

		final AntState state = new AntState(0, 0, Orientation.DOWN, map, maxMoves);
		ISelection<SyntaxTree<AntState>> selection = new TournamentSelection<>(tournamentSize);
		IMutation<SyntaxTree<AntState>> mutation = new SubTreeMutation<>(maxDepth, maxNodes, functions, terminals);
		AntEvaluator evaluator = 
				new AntEvaluator(maxMoves, i, j, Orientation.DOWN, map, state);
		ICrossOver<SyntaxTree<AntState>> crossover = new SyntaxTreeCrossOver<>(maxDepth, maxNodes);

		GP<SyntaxTree<AntState>> gp = new GP<>(
				createNewPopulation(popSize, maxNodes, startDepth, functions, terminals), 
				selection, mutation, crossover, evaluator, 
				popSize, maxIter, maxFitnessBound, 
				mutationProb, recombinationProb, crossoverProb);
		GPSolution<SyntaxTree<AntState>> sol = gp.start();
		Files.write(Paths.get(args[4]), sol.getData().toString().getBytes());
		System.out.println(sol.getData());
		System.out.println(sol);
		
		final AntGUIState guiState = new AntGUIState(0, 0, Orientation.DOWN, map, maxMoves);
		evaluator = new AntEvaluator(maxMoves, i, j, Orientation.DOWN, map, guiState);
		evaluator.oneTimeEvaluation(sol, guiState);
		
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				startGUIapp(map, guiState.getMoves());
				
			}
		});
	}
	
	/**
	 * Starts the GUI.
	 */
	protected static void startGUIapp(boolean[][] map, List<String> moves) {
		final JFrame f = new JFrame();
		f.setTitle("Mrav jede...");
		f.setSize(400, 400);
		f.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		final FoodBoard p = new FoodBoard(map, moves);
		f.setLayout(new BorderLayout());
		f.getContentPane().add(p, BorderLayout.CENTER);
		f.setVisible(true);
		f.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
					p.doNextMove();
					f.repaint();
			}
			
		});
	}

	private static List<GPSolution<SyntaxTree<AntState>>> createNewPopulation(Integer popSize, Integer maxNodes,
			Integer maxDepth, List<Function<AntState>> functions, List<Terminal<AntState>> terminals) {

		List<GPSolution<SyntaxTree<AntState>>> pop = new ArrayList<>();
		Integer ndepths = maxDepth - 1;
		Integer ntreesd = popSize / ndepths;
		for (int i = 1; i <= ndepths; i++) {
			for (int k = 0; k < ntreesd / 2; k++) {
				pop.add(new AntSolution(SyntaxTree.newRandomInstance(i, maxNodes, functions, terminals, true)));
				pop.add(new AntSolution(SyntaxTree.newRandomInstance(i, maxNodes, functions, terminals, false)));
			}

		}

		return pop;
	}

	private static boolean[][] parseMap(List<String> lines) {
		String[] split = lines.get(0).split("x");

		int n = Integer.parseInt(split[0]);
		int m = Integer.parseInt(split[1]); // map dimensions

		boolean[][] map = new boolean[n][m];
		lines = lines.subList(1, lines.size());

		for (int i = 0; i < n; i++) {
			String line = lines.get(i);
			for (int j = 0; j < m; j++) {
				switch (line.charAt(j)) {
				case '.':
				case '0':
					map[i][j] = false;
					break;
				case '1':
					map[i][j] = true;
					break;
				}
			}
		}
		return map;
	}
}
