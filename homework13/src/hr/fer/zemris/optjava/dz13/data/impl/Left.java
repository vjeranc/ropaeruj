package hr.fer.zemris.optjava.dz13.data.impl;

import java.util.List;

import hr.fer.zemris.optjava.dz13.data.model.SyntaxTree;
import hr.fer.zemris.optjava.dz13.data.model.Terminal;

public class Left extends Terminal<AntState>{

	private static final long serialVersionUID = -6838655803613867299L;

	@Override
	public void execute(AntState ant, List<SyntaxTree<AntState>> kids) {
		if (ant.canMove()) {
			ant.turnLeft();
		}
	}

	@Override
	public String toString() {
		return "Left";
	}
}
