package hr.fer.zemris.optjava.dz13.data.impl;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

public class AntState {
	
	/** Coordinates of ant. */
	private int i, j;
	/** Orientation of ant, direction in which the ant is looking. */
	private Orientation orientation;
	/** Map with scattered food. */
	private boolean[][] map;
	/** Visited fields. */
	private boolean[][] visited;
	/** Number of food pieces eaten. */
	private int foodEaten = 0;
	private int movesLeft = 0;
	private int maxFood;
	
	/**
	 * Direction in which the Ant is looking.
	 * @author vjeran
	 *
	 */
	public enum Orientation {
		UP, DOWN, LEFT, RIGHT
	}
	
	public AntState(int i, int j, Orientation orientation, boolean[][] map, int maxMoves) {
		this.i = i;
		this.j = j;
		this.orientation = orientation;
		this.map = map;
		this.visited = new boolean[map.length][map[0].length];
		this.movesLeft = maxMoves;
		for(int k = 0; k < map.length; k++) {
			for(int l = 0; l < map[0].length; l++) {
				if (map[k][l]) maxFood++;
			}
		}
	}
	
	/** 
	 * Moves ant forward.
	 */
	public void move() {
		movesLeft--;
		switch (orientation) {
		case UP:
			i = (i + map.length - 1) % map.length;
			break;
		case DOWN:
			i = (i + 1) % map.length;
			break;
		case LEFT:
			j = (j + map[0].length - 1) % map[0].length;
			break;
		case RIGHT:
			j = (j + 1) % map[0].length;
			break;
		}
		if (!visited[i][j] && map[i][j]) { foodEaten++; visited[i][j] = true; }
	}
	
	public void turnLeft() {
		movesLeft--;
		switch (orientation) {
		case UP:
			orientation = Orientation.LEFT;
			break;
		case DOWN:
			orientation = Orientation.RIGHT;
			break;
		case LEFT:
			orientation = Orientation.DOWN;
			break;
		case RIGHT:
			orientation = Orientation.UP;
			break;
		}
	}
	
	public void turnRight() {
		movesLeft--;
		switch (orientation) {
		case UP:
			orientation = Orientation.RIGHT;
			break;
		case DOWN:
			orientation = Orientation.LEFT;
			break;
		case LEFT:
			orientation = Orientation.UP;
			break;
		case RIGHT:
			orientation = Orientation.DOWN;
			break;
		}
	}
	
	public int movesLeft() {
		return movesLeft;
	}
	
	public boolean canMove() {
		if (foodEaten == maxFood) return false;
		return movesLeft > 0;
	}
	
	public boolean isFoodAhead() {
		if (orientation == Orientation.UP) {
			return map[(i + map.length - 1) % map.length][j];
		} else if (orientation == Orientation.DOWN) {
			return map[(i + 1) % map.length][j];
		} else if (orientation == Orientation.LEFT) {
			return map[i][(j + map[0].length - 1) % map[0].length];
		} else { // orientation is right
			return map[i][(j + 1) % map[0].length];
		}
	}
	
	public int foodEaten() {
		return foodEaten;
	}
	
	@Override
	public String toString() {
		return "" + i + " " + j + " " + foodEaten + " " + movesLeft;
	}

	public AntState newInstance(int i2, int j2, Orientation orient, boolean[][] map2, int maxMoves) {
		return new AntState(i2, j2, orient, map2, maxMoves);
	}
}
