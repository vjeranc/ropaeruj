package hr.fer.zemris.optjava.dz13.data.impl;

import hr.fer.zemris.optjava.dz13.data.model.Function;

public class Prog3 extends Function<AntState>{

	private static final long serialVersionUID = -5841966418855863615L;

	@Override
	public int arity() {
		return 3;
	}
	
	@Override
	public String toString() {
		return "Prog3";
	}
	
}
