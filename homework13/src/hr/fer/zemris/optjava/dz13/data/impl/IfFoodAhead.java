package hr.fer.zemris.optjava.dz13.data.impl;

import java.util.List;

import hr.fer.zemris.optjava.dz13.data.model.Function;
import hr.fer.zemris.optjava.dz13.data.model.SyntaxTree;

public class IfFoodAhead extends Function<AntState> {

	private static final long serialVersionUID = 4581544724262656029L;

	@Override
	public int arity() {
		return 2;
	}
	
	@Override
	public void execute(AntState state, List<SyntaxTree<AntState>> kids) {
		if(state.isFoodAhead()) {
			kids.get(0).root.execute(state, kids.get(0).subtrees);
		} else {
			kids.get(1).root.execute(state, kids.get(1).subtrees);
		}
	}
	
	@Override
	public String toString() {
		return "IfFoodAhead";
	}
}
