package hr.fer.zemris.optjava.dz13.data.model;

import java.util.List;

import hr.fer.zemris.optjava.dz13.data.model.SyntaxTree.Node;

public abstract class Function<T> implements Node<T> {

	private static final long serialVersionUID = -8367532979725546069L;

	public abstract int arity();
	
	@Override
	public void execute(T state, List<SyntaxTree<T>> kids) {
		for(SyntaxTree<T> kid : kids) {
			kid.root.execute(state, kid.subtrees);
		}
	}
	
	
	 
}
