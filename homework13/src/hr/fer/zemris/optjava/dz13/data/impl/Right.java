package hr.fer.zemris.optjava.dz13.data.impl;

import java.util.List;

import hr.fer.zemris.optjava.dz13.data.model.SyntaxTree;
import hr.fer.zemris.optjava.dz13.data.model.Terminal;

public class Right extends Terminal<AntState> {


	private static final long serialVersionUID = 1695397422043051852L;

	@Override
	public void execute(AntState ant, List<SyntaxTree<AntState>> kids) {
		if (ant.canMove()) {
			ant.turnRight();
		}		
	}

	@Override
	public String toString() {
		return "Right";
	}
	
}
