package hr.fer.zemris.optjava.dz13.data.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SyntaxTree<T> implements Serializable {

	private static final long serialVersionUID = 4888585172536579031L;

	public static interface Node<T> extends Serializable {
		void execute(T state, List<SyntaxTree<T>> kids);
	}

	public SyntaxTree<T> parent;
	public Node<T> root;
	public List<SyntaxTree<T>> subtrees;
	private int size = 1; // node has to be present
	private int depth= 0;
	private int level= 0;

	private SyntaxTree(Node<T> root, List<SyntaxTree<T>> subtrees) {
		this.root = root;
		this.subtrees = subtrees;
		for(SyntaxTree<T> kid : subtrees) this.depth = Math.max(this.depth, kid.depth);
		this.depth++;
		for(SyntaxTree<T> kid : subtrees) this.size += kid.size;
		recalculateLevel(this, 0);
	}
	
	public int size() {
		return size;
	}
	
	public int depth() {
		return depth;
	}
	
	public int level() {
		return level;
	}
	
	public void recalcDepth() {
		this.depth = 0;
		this.size = 1;
		for(SyntaxTree<T> kid : this.subtrees) this.depth = Math.max(this.depth, kid.depth);
		this.depth++;
		for(SyntaxTree<T> kid : this.subtrees) this.size += kid.size;
	}
	
	private static <T> void recalculateLevel(SyntaxTree<T> tree, int level) {
		tree.level = level;
		for(SyntaxTree<T> kid : tree.subtrees) {
			recalculateLevel(kid, level+1);
		}
	}

	public static <T> SyntaxTree<T> newRandomInstance(int depth, int numberOfNodes, List<Function<T>> functions,
			List<Terminal<T>> terminals, boolean fullOrGrow) {
		return SyntaxTree.randomTree(functions, terminals, depth, numberOfNodes, fullOrGrow, new Random());
	}

	private static <T> SyntaxTree<T> randomTree(List<Function<T>> functions, List<Terminal<T>> terminals, int leftDepth,
			int nodes, boolean fullOrGrow, Random rand) {
		Node<T> node = null; List<SyntaxTree<T>> kids = new ArrayList<>();
		if (leftDepth <= 0 || nodes <= 0 || (!fullOrGrow && (rand.nextDouble() < 1.0 * terminals.size() / (terminals.size() + functions.size())))) {
			node = terminals.get(rand.nextInt(terminals.size()));
		} else {
			Function<T> func = functions.get(rand.nextInt(functions.size()));
			for (int i = 0; i < func.arity(); i++) {
				SyntaxTree<T> tree = SyntaxTree.randomTree(functions, terminals, leftDepth - 1, 
				        								   nodes, fullOrGrow, rand);
				kids.add(tree);
				nodes -= tree.size;
			}
			node = func;
		}
		return new SyntaxTree<T>(node, kids);
	}

	public void deepRecalc() {
		recalculateLevel(this, 0);
		recalculate(this);
	}
	
	private static <T> void recalculate(SyntaxTree<T> tree) {
		for(SyntaxTree<T> kid : tree.subtrees) {
			recalculate(kid);
		}
		tree.depth = 0;
		tree.size = 1;
		for(SyntaxTree<T> kid : tree.subtrees) tree.depth = Math.max(tree.depth, kid.depth);
		tree.depth++;
		for(SyntaxTree<T> kid : tree.subtrees) tree.size += kid.size;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		toStringOrder(this, sb);
		return sb.toString();
	}

	private void toStringOrder(SyntaxTree<T> syntaxTree, StringBuilder sb) {
		if (syntaxTree.root instanceof Function) {
			sb.append(syntaxTree.root.toString()).append("(");
			for(SyntaxTree<T> kid : syntaxTree.subtrees) {
				toStringOrder(kid, sb);
				sb.append(", ");
			}
			sb.delete(sb.length()-2, sb.length());
			sb.append(")");
		} else {
			sb.append(syntaxTree.root.toString());
		}
	}
}
