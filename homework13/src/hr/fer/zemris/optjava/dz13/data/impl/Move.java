package hr.fer.zemris.optjava.dz13.data.impl;

import java.util.List;


import hr.fer.zemris.optjava.dz13.data.model.SyntaxTree;
import hr.fer.zemris.optjava.dz13.data.model.Terminal;

public class Move extends Terminal<AntState> {

	private static final long serialVersionUID = 7849057298397436038L;

	@Override
	public void execute(AntState ant, List<SyntaxTree<AntState>> kids) {
		if(ant.canMove()) {
			ant.move();
		}
	}
	
	@Override
	public String toString() {
		return "Move";
	}

}
