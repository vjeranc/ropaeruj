package hr.fer.zemris.optjava.dz13.data.model;

import hr.fer.zemris.optjava.dz13.data.model.SyntaxTree.Node;

public abstract class Terminal<T> implements Node<T> {

	private static final long serialVersionUID = -3958786832462587539L;

}
