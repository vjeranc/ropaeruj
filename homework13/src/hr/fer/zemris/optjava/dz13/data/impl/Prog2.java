package hr.fer.zemris.optjava.dz13.data.impl;

import hr.fer.zemris.optjava.dz13.data.model.Function;

public class Prog2 extends Function<AntState> {

	private static final long serialVersionUID = 6366988979468474504L;

	@Override
	public int arity() {
		return 2;
	}
	
	@Override
	public String toString() {
		return "Prog2";
	}
	
}
