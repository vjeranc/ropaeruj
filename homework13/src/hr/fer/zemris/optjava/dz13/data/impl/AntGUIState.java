package hr.fer.zemris.optjava.dz13.data.impl;

import java.util.ArrayList;
import java.util.List;

public class AntGUIState extends AntState {

	private List<String> moves = new ArrayList<>();
	
	public AntGUIState(int i, int j, Orientation orientation, boolean[][] map, int maxMoves) {
		super(i, j, orientation, map, maxMoves);
		System.out.println("EVO!");
	}

	
	public List<String> getMoves() {
		return moves; 
	}
	
	@Override
	public void move() {
		moves.add("Move");
		super.move();
	}
	
	@Override
	public void turnLeft() {
		moves.add("Left");
		super.turnLeft();
	}
	
	@Override
	public void turnRight() {
		moves.add("Right");
		super.turnRight();
	}
	
	@Override
	public AntState newInstance(int i2, int j2, Orientation orient, boolean[][] map2, int maxMoves) {
		return new AntGUIState(i2, j2, orient, map2, maxMoves);
	}
}
