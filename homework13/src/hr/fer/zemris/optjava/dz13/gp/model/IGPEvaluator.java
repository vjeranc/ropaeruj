package hr.fer.zemris.optjava.dz13.gp.model;

public interface IGPEvaluator<T> {
	public void evaluate(GPSolution<T> p);
}
