package hr.fer.zemris.optjava.dz13.gp.model;

import java.util.List;

public interface ICrossOver<T> {
	List<GPSolution<T>> crossOver(GPSolution<T> p1, GPSolution<T> p2);
}
