package hr.fer.zemris.optjava.dz13.gp.model;


import java.util.List;

public interface ISelection<T> {
	/**
	 * Selects a single {@link GPSolution} out of a given population.
	 * @param population of solutions
	 * @return single solution
	 */
	GPSolution<T> select(List<GPSolution<T>> population);
	
	/**
	 * Selects {@code n} solutions out of a given population.
	 * @param population of solutions
	 * @param n number of solutions to return
	 * @return list of selected solutions
	 */
	List<GPSolution<T>> select(List<GPSolution<T>> population, int n);
}
