package hr.fer.zemris.optjava.dz13.gp.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import hr.fer.zemris.optjava.dz13.data.impl.AntState;
import hr.fer.zemris.optjava.dz13.data.model.SyntaxTree;
import hr.fer.zemris.optjava.dz13.gp.model.GPSolution;

public class AntSolution extends GPSolution<SyntaxTree<AntState>> {

	private static final long serialVersionUID = 6268494526064833831L;

	public int foodEaten;
	
	public AntSolution(SyntaxTree<AntState> data) {
		this.data = data;
		this.fitness = Double.NEGATIVE_INFINITY;
	}
	
	@Override
	public GPSolution<SyntaxTree<AntState>> duplicate() {
		Object obj = null;
		try {
			// Write the object out to a byte array
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream out = new ObjectOutputStream(bos);
			out.writeObject(AntSolution.this);
			out.flush();

			// Make an input stream from the byte array and read
			// a copy of the object back in.
			ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bos.toByteArray()));
			obj = in.readObject();
			out.close(); in.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
		return (GPSolution<SyntaxTree<AntState>>) obj;
	}
	
	@Override
	public String toString() {
		return ""+foodEaten + " " + data.size() + " " + data.depth() + " " + this.hashCode();
	}

}
