package hr.fer.zemris.optjava.dz13.gp.impl;

import hr.fer.zemris.optjava.dz13.data.impl.AntState;
import hr.fer.zemris.optjava.dz13.data.impl.AntState.Orientation;
import hr.fer.zemris.optjava.dz13.data.model.SyntaxTree;
import hr.fer.zemris.optjava.dz13.gp.model.GPSolution;
import hr.fer.zemris.optjava.dz13.gp.model.IGPEvaluator;

public class AntEvaluator implements IGPEvaluator<SyntaxTree<AntState>> {

	private int maxMoves;
	private int i, j;
	private Orientation orient;
	private boolean[][] map;
	private AntState state;
	
	/**
	 * 
	 * @param maxMoves maximum number of moves
	 * @param i starting row
	 * @param j starting column
	 * @param orient starting orientation of the ant
	 * @param map map containing the scattered food information
	 */
	public AntEvaluator(int maxMoves, int i, int j, Orientation orient, boolean[][] map, AntState state) {
		this.maxMoves = maxMoves;
		this.i = i;
		this.j = j;
		this.orient = orient;
		this.map = map;
		this.state = state;
	}



	@Override
	public void evaluate(GPSolution<SyntaxTree<AntState>> p) {
		SyntaxTree<AntState> data = p.getData();
		state = state.newInstance(i, j, orient, map, maxMoves);
		while(state.canMove()) data.root.execute(state, data.subtrees);
		((AntSolution) p).foodEaten = state.foodEaten();
		p.fitness = new Double(state.foodEaten());
	}
	
	public void oneTimeEvaluation(GPSolution<SyntaxTree<AntState>> p, AntState state) {
		SyntaxTree<AntState> data = p.getData();
		while(state.canMove()) data.root.execute(state, data.subtrees);
		((AntSolution) p).foodEaten = state.foodEaten();
		p.fitness = new Double(state.foodEaten());
	}

}
