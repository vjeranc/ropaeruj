package hr.fer.zemris.optjava.dz13.gp.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import hr.fer.zemris.optjava.dz13.data.model.Function;
import hr.fer.zemris.optjava.dz13.data.model.SyntaxTree;
import hr.fer.zemris.optjava.dz13.data.model.SyntaxTree.Node;
import hr.fer.zemris.optjava.dz13.data.model.Terminal;
import hr.fer.zemris.optjava.dz13.gp.model.GPSolution;
import hr.fer.zemris.optjava.dz13.gp.model.ICrossOver;

public class SyntaxTreeCrossOver<T> implements ICrossOver<SyntaxTree<T>> {

	private static Random rand = new Random();
	private int maxDepth;
	private int maxNodes;
	
	
	

	public SyntaxTreeCrossOver(int maxDepth, int maxNodes) {
		super();
		this.maxDepth = maxDepth;
		this.maxNodes = maxNodes;
	}

	@Override
	public List<GPSolution<SyntaxTree<T>>> crossOver(GPSolution<SyntaxTree<T>> p1, GPSolution<SyntaxTree<T>> p2) {
		GPSolution<SyntaxTree<T>> dp1 = p1.duplicate(), dp2 = p2.duplicate();
		SyntaxTree<T> tree1 = dp1.getData();
		SyntaxTree<T> tree2 = dp2.getData();
		int i = rand.nextInt(tree1.size());
		int j = rand.nextInt(tree2.size());
		
		SyntaxTree<T> sub1 = getSubtree(tree1, 0, i);
		SyntaxTree<T> sub2 = getSubtree(tree2, 0, j);
		List<GPSolution<SyntaxTree<T>>> list = new ArrayList<>(Arrays.asList(dp1,dp2));
		if (sub1.level() + sub2.depth() > maxDepth ||
			sub2.level() + sub1.depth() > maxDepth ||
			tree1.size() - sub1.size() + sub2.size() > maxNodes ||
			tree2.size() - sub2.size() + sub1.size() > maxNodes) {
			return new ArrayList<>();
		}
		
		Node<T> help = sub1.root;
		sub1.root = sub2.root;
		sub2.root = help;
		
		List<SyntaxTree<T>> helpK = sub1.subtrees;
		sub1.subtrees = sub2.subtrees;
		sub2.subtrees = helpK;
		
		tree1.deepRecalc();
		tree2.deepRecalc();

		return list;
	}

	public SyntaxTree<T> getSubtree(SyntaxTree<T> tree, int i, int n) {
		if (i == n) {
			return tree;
		} else {
			List<SyntaxTree<T>> kids = tree.subtrees;
			int curi = i + 1;
			for (int j = 0; j < kids.size(); j++) {
				SyntaxTree<T> kid = kids.get(j);
				if (curi == n) { // his child is the mutation point
					return kid;
				} else if (n > curi && n < curi + kid.size()) {
					return getSubtree(kid, curi, n);
				} else {
					curi += kid.size();
				}
			}
		}
		return tree;

	}

}
