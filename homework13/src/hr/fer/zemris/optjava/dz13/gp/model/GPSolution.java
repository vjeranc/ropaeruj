package hr.fer.zemris.optjava.dz13.gp.model;

import java.io.Serializable;

public abstract class GPSolution<T> implements Comparable<GPSolution<T>>,Serializable {

	private static final long serialVersionUID = 4862299040287496232L;
	
	protected T data;
	public Double fitness;

	public GPSolution() {
	}

	public T getData() {
		return data;
	}

	public abstract GPSolution<T> duplicate();

	@Override
	public int compareTo(GPSolution<T> o) {
		return this.fitness.compareTo(o.fitness);
	}
}
