package hr.fer.zemris.optjava.dz13.gp.model;

public interface IMutation<T> {
	void mutate(GPSolution<T> solution);
}
