package hr.fer.zemris.optjava.dz13.gp.impl;

import hr.fer.zemris.optjava.dz13.gp.model.GPSolution;
import hr.fer.zemris.optjava.dz13.gp.model.ISelection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class TournamentSelection<T> implements ISelection<T>{

	private static final Random rand = new Random();
	int k;
	public TournamentSelection(int size) {
		this.k = size;
	}
	
	/**
	 * Selects {@code b} best from population. 
	 * @param population
	 * @param k tournament size
	 * @param b
	 * @return
	 */
	@Override
	public GPSolution<T> select(List<GPSolution<T>> population) {
		Collections.shuffle(population);
		population = population.subList(0, k);

		return Collections.max(population);
	}

	@Override
	public List<GPSolution<T>> select(List<GPSolution<T>> population, int n) {
		Collections.shuffle(population);
		population = population.subList(0, k);
		Collections.sort(population, new Comparator<GPSolution<T>>() {

			@Override
			public int compare(GPSolution<T> o1, GPSolution<T> o2) {
				// TODO Auto-generated method stub
				return -o1.compareTo(o2);
			}
		});
		return population.subList(0, n);
	}

}
