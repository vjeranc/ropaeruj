package hr.fer.zemris.optjava.dz13.gp.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class GP<T> {

	private List<GPSolution<T>> population;
	private ISelection<T> selection;
	private IMutation<T> mutation;
	private ICrossOver<T> crossover;
	private IGPEvaluator<T> evaluator;
	private Integer popSize;
	private Integer maxiter;
	private double maxFitness;
	private double mutationBar;
	private double reproductionBar;
	private GPSolution<T> best;
	//private double crossoverBar;
	
	public GP(List<GPSolution<T>> population, ISelection<T> selection, IMutation<T> mutation,
			ICrossOver<T> crossover, IGPEvaluator<T> evaluator, 
			Integer popSize, Integer maxiter, double minFitness,
			double mutationProbability, double reproductionProbability, double crossoverProbability) {
		this.population = population;
		this.selection = selection;
		this.mutation = mutation;
		this.crossover = crossover;
		this.evaluator = evaluator;
		this.popSize = popSize;
		this.maxiter = maxiter;
		this.maxFitness = minFitness;
		this.mutationBar = mutationProbability;
		this.reproductionBar = reproductionProbability + mutationBar;
		//this.crossoverBar = crossoverProbability + reproductionBar; // should be 1.0
	}
	
	public GPSolution<T> start() {
		evaluate(population);
		best = Collections.max(population);
		Integer iter = 0;
		System.out.println(population);
		while(best.fitness < maxFitness && iter++ < maxiter) {
			population = buildNewPopulation();
			evaluate(population);
			Collections.shuffle(population);
			best = Collections.max(population);
			System.out.println(best);
		}
		
		return best;
	}
	
	private List<GPSolution<T>> buildNewPopulation() {
		List<GPSolution<T>> nPop = new ArrayList<>();
		nPop.add(best);
		// selecting operator probabilistically
		Random rand = new Random();
		
		while (nPop.size() < popSize) {
			double prob = rand.nextDouble();
			// mutation, reproduction, crossover bars are sorted
			if (prob < mutationBar) { // mutation it is
				GPSolution<T> p = selection.select(population);
				GPSolution<T> kid = p.duplicate();
				mutation.mutate(kid);
				if (kid.fitness.equals(p.fitness)) {
					kid.fitness *= 0.9;
				}
				nPop.add(kid);
			} else if (prob < reproductionBar) {
				GPSolution<T> unit = selection.select(population);
				nPop.add(unit);
			} else { // crossover
				List<GPSolution<T>> parents = selection.select(population, 2);
				GPSolution<T> parent1 = parents.get(0), parent2 = parents.get(1);
				for(GPSolution<T> kid : crossover.crossOver(parent1, parent2)) {
					evaluator.evaluate(kid);
					if (kid.fitness.equals(parent1.fitness) || kid.fitness.equals(parent2.fitness)) {
						kid.fitness *= 0.9;
					}
					nPop.add(kid);
				}
			}
		}
		
		return nPop;
	}

	public void evaluate(List<GPSolution<T>> population) {
		for (GPSolution<T> gaSolution : population) {
			evaluator.evaluate(gaSolution);
		}
	}
	
	
	
}
