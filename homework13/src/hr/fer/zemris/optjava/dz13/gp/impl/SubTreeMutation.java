package hr.fer.zemris.optjava.dz13.gp.impl;

import java.util.List;
import java.util.Random;

import hr.fer.zemris.optjava.dz13.data.model.Function;
import hr.fer.zemris.optjava.dz13.data.model.SyntaxTree;
import hr.fer.zemris.optjava.dz13.data.model.Terminal;
import hr.fer.zemris.optjava.dz13.gp.model.GPSolution;
import hr.fer.zemris.optjava.dz13.gp.model.IMutation;

public class SubTreeMutation<T> implements IMutation<SyntaxTree<T>> {

	private static Random rand = new Random();
	private int maxDepth;
	private int maxNodes;
	private List<Function<T>> functions;
	private List<Terminal<T>> terminals;

	public SubTreeMutation(int maxDepth, int maxNodes, List<Function<T>> functions, List<Terminal<T>> terminals) {
		this.maxDepth = maxDepth;
		this.maxNodes = maxNodes;
		this.functions = functions;
		this.terminals = terminals;
	}

	@Override
	public void mutate(GPSolution<SyntaxTree<T>> solution) {
		SyntaxTree<T> tree = solution.getData();
		int n = rand.nextInt(tree.size()); // random node
		if (n == 0) {
			SyntaxTree<T> rtree = SyntaxTree.newRandomInstance(maxDepth, maxNodes, functions, terminals,
					false);
			tree.root = rtree.root;
			tree.subtrees = rtree.subtrees;
		} else {
			postOrderInsert(tree, 0, n, tree.size());
		}
	}

	public void postOrderInsert(SyntaxTree<T> tree, int i, int n, int treeSize) {
		List<SyntaxTree<T>> kids = tree.subtrees;
		int curi = i + 1;
		for (int j = 0; j < kids.size(); j++) {
			SyntaxTree<T> kid = kids.get(j);
			if (curi == n) {
				int depth = maxDepth - kid.level();
				int numberOfNodes = maxNodes - treeSize;
				if (numberOfNodes <= 0 || depth < 0)
					return;
				SyntaxTree<T> randTree = SyntaxTree
						.newRandomInstance(depth, numberOfNodes, functions, terminals, false);
				kid.root = randTree.root;
				kid.subtrees = randTree.subtrees;
				break;
			} else if (n > curi && n < curi + kid.size()) {
				postOrderInsert(kid, curi, n, treeSize);
				break;
			} else {
				curi += kid.size();
			}
		}
		tree.recalcDepth();
	}

}
