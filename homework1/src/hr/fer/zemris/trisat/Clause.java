package hr.fer.zemris.trisat;

import java.util.Arrays;

public class Clause {

	private int[] indexes;

	public Clause(int[] indexes) {
		this.indexes = Arrays.copyOf(indexes, indexes.length);
	}

	// vraća broj literala koji čine klauzulu
	public int getSize() {
		return indexes.length;
	}

	// vraća indeks varijable koja je index-ti član ove klauzule
	public int getLiteral(int index) {
		return indexes[index];
	}

	// vraća true ako predana dodjela zadovoljava ovu klauzulu
	public boolean isSatisfied(BitVector assignment) {
		int n = getSize();
		boolean result = false;
		for (int i = 0; i < n; i++) {
			int literalIndex = getLiteral(i); // literals are indexed from 1 to n
			int absIndex = Math.abs(literalIndex)-1; // adjust for 0 indexing
			boolean asgn = assignment.get(absIndex);
			boolean literalValue = (literalIndex < 0 ? !asgn : asgn);
			result = result || literalValue;
			if (result) {
				return true;
			}
		}
		return false;
	}

	static char[] unicode_overline = Character.toChars(0x0305);
	static char[] unicode_x = Character.toChars(0x0058);
	static int unicode_subscript = 0x2080;
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		int n = getSize();
		sb.append('(');
		for(int i = 0; i < n; i++) {
			int literalIndex = getLiteral(i);
			if (literalIndex < 0) {
				sb.append(unicode_overline);
			}
			sb.append(unicode_x).append(getUnicodeSubscript(Math.abs(literalIndex)));
			if (i != n-1) sb.append('+');
		}
		sb.append(')');
		return sb.toString();
	}

	private String getUnicodeSubscript(int a) {
		int num = a;
		StringBuilder sb = new StringBuilder();
		while(num != 0) {
			int digit = num % 10;
			sb.append(Character.toChars(unicode_subscript + digit));
			num /= 10;
		}

		return sb.reverse().toString();
	}

	public static Clause parseClause(String line) {
		String[] args = line.split("\\s+");
		int n = args.length - 1; // zero at the end is ignored
		int[] indexes = new int[n];
		for(int i = 0; i < n; i++) {
			indexes[i] = Integer.parseInt(args[i]);
		}
		return new Clause(indexes);
	}
}
