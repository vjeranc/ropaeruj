package hr.fer.zemris.trisat;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class TriSatSolver {

	public static void main(String[] args) {
		if (args.length != 2) {
			throw new IllegalArgumentException(
					"Arguments needed are a flag for which algorithm to use and input file.");
		}

		Integer algFlag = Integer.parseInt(args[0]);
		Path filePath = Paths.get(args[1]);
		System.out.println(algFlag + " " + filePath);
		try {
			SATFormula formula = SATFormula.parseSATFormula(Files.readAllLines(
					filePath, StandardCharsets.UTF_8));
			System.out.println(formula);
			BitVector solution = getSolution(algFlag, formula);
			showSolution(solution, formula.isSatisfied(solution));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void showSolution(BitVector solution, boolean isSatisfied) {
		if (isSatisfied) {
			System.out.println("Zadovoljivo: " + solution.toString());
		} else {
			System.out.println("Formula nije zadovoljena.");
		}
	}

	private static BitVector getSolution(Integer algFlag, SATFormula formula) {
		switch (algFlag) {
		case 1:
			return algorithm1(formula);
		case 2:
			return algorithm2(formula);
		case 3:
			return algorithm3(formula);
		default:
			return null;
		}

	}

	/**
	 * Returns a solution of 3-SAT problem. Uses probabilistic algorithm
	 * favoring solutions of previously unsatisfied clauses.
	 * 
	 * @param formula
	 *            3-SAT formula
	 * @return solution to the satisfiability problem.
	 */
	private static BitVector algorithm3(SATFormula formula) {
		final SATFormulaStats stats = new SATFormulaStats(formula);
		BitVector curBest = new BitVector(formula.getNumberOfVariables());
		stats.setAssignment(curBest, false);
		int goal = formula.getNumberOfClauses();
		int iter = 0;
		Random rand = new Random();
		do {
			BitVectorNGenerator nborhood = new BitVectorNGenerator(curBest);
			List<BitVector> nextMoves = new ArrayList<>();
			final Map<BitVector, Double> value = new HashMap<>();

			for (BitVector nbor : nborhood) {
				nextMoves.add(nbor);
				stats.setAssignment(nbor, true);
				double satisfied = stats.getNumberOfSatisfied();
				if (goal == satisfied) {
					return nbor;
				}
				value.put(
						nbor,
						stats.getNumberOfSatisfied()
								+ stats.getPercentageBonus());
			}

			Collections.sort(nextMoves, new Comparator<BitVector>() {

				@Override
				public int compare(BitVector o1, BitVector o2) {
					double result = value.get(o2) - value.get(o1);
					if (result > 0) {
						return 1;
					} else if (result < 0) {
						return -1;
					} else {
						return 0;
					}
				}
			});
			int n = nextMoves.size() % 3 + 1;
			curBest = nextMoves.subList(0, n).get(rand.nextInt(n));
		} while (++iter < 100000);

		throw new IllegalStateException(
				"Solution was not found using algorithm 3!");
	}

	/**
	 * Hill-climbing algorithm which doesn't always succeed.
	 * 
	 * @param formula
	 *            3SAT formula
	 * @return boolean assignment which solves the problem
	 */
	private static BitVector algorithm2(SATFormula formula) {
		final SATFormulaStats stats = new SATFormulaStats(formula);
		BitVector curBest = new BitVector(formula.getNumberOfVariables());
		stats.setAssignment(curBest, false);
		int goal = formula.getNumberOfClauses();
		int curBestSatisfied = stats.getNumberOfSatisfied();
		int iter = 0;
		Random rand = new Random();
		do {
			BitVectorNGenerator nborhood = new BitVectorNGenerator(curBest);
			List<BitVector> nextMoves = new ArrayList<>();
			final Map<BitVector, Integer> value = new HashMap<>();
			int lessCounter = 0; // counts bitvectors which satisifed less
									// variables than curBest;
			for (BitVector nbor : nborhood) {
				nextMoves.add(nbor);
				stats.setAssignment(nbor, false);
				int satisfied = stats.getNumberOfSatisfied();
				if (goal == satisfied) {
					return nbor;
				}
				if (satisfied <= curBestSatisfied) {
					lessCounter++;
				}
				value.put(nbor, stats.getNumberOfSatisfied());
			}
			if (lessCounter == nextMoves.size()) {
				break;
			}
			Collections.sort(nextMoves, new Comparator<BitVector>() {

				@Override
				public int compare(BitVector o1, BitVector o2) {
					return value.get(o2) - value.get(o1);
				}
			});
			int n = nextMoves.size() % 5 + 1;
			curBest = nextMoves.subList(0, n).get(rand.nextInt(n));
			curBestSatisfied = value.get(curBest);
		} while (++iter < 100000);

		throw new IllegalStateException(
				"Solution was not found using algorithm 2!");
	}

	/**
	 * Algorithm which searches the whole space for a solution.
	 * 
	 * @param formula
	 *            which must be satisfied
	 * @return first found solution
	 */
	private static BitVector algorithm1(SATFormula formula) {
		Set<BitVector> wholeNeighborhood = getWholeNeighborhood(formula
				.getNumberOfVariables());

		BitVector solution = null;
		for (BitVector bv : wholeNeighborhood) {
			if (formula.isSatisfied(bv)) {
				solution = bv;
				break;
			}
		}

		return solution;
	}

	/**
	 * Returns the whole neighborhood of the {@link BitVector} length {@code n}.
	 * This function is memory intensive and complexity is exponential in
	 * {@link BitVector} length.
	 * 
	 * @param n
	 *            length of {@link BitVector}
	 * @return whole neighborhood
	 */
	private static Set<BitVector> getWholeNeighborhood(int n) {
		BitVector bv = new BitVector(n);
		Set<BitVector> wholeNeighborhood = new HashSet<>();
		Set<BitVector> leftOver = new HashSet<>();
		leftOver.add(bv);
		wholeNeighborhood.add(bv);
		while (leftOver.size() != 0) {
			Set<BitVector> neighbors = new HashSet<>();
			for (BitVector bitv : leftOver) {
				BitVectorNGenerator nbors = new BitVectorNGenerator(bitv);
				for (BitVector nbitv : nbors) {
					neighbors.add(nbitv);
				}
			}
			// all leftOvers done, now clear
			leftOver.clear();
			for (BitVector neighbor : neighbors) {
				if (wholeNeighborhood.add(neighbor)) {
					leftOver.add(neighbor);
				}
			}
		}

		return wholeNeighborhood;
	}

}
