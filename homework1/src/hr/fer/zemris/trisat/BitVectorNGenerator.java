package hr.fer.zemris.trisat;

import java.util.Iterator;


public class BitVectorNGenerator implements Iterable<MutableBitVector> {

	private BitVector assignment;

	public BitVectorNGenerator(BitVector assignment) {
		this.assignment = assignment;
	}

	// Vraća iterator koji na svaki next() računa sljedećeg susjeda
	@Override
	public Iterator<MutableBitVector> iterator() {
		return new Iterator<MutableBitVector>() {

			private int nextIndex = 0;

			@Override
			public boolean hasNext() {
				return nextIndex < assignment.getSize();
			}

			@Override
			public MutableBitVector next() {

				MutableBitVector mbv = assignment.copy();
				mbv.set(nextIndex, !assignment.get(nextIndex));
				nextIndex++;
				return mbv;
			}

			@Override
			public void remove() {
				// not supported
			}
		};
	}

	// Vraća kompletno susjedstvo kao jedno polje
	public MutableBitVector[] createNeighborhood() {
		MutableBitVector[] neighborhood = new MutableBitVector[assignment
				.getSize()];
		int i = 0;
		for (Iterator<MutableBitVector> iterator = this.iterator(); iterator
				.hasNext();) {
			MutableBitVector mbv = iterator.next();
			neighborhood[i++] = mbv;
		}
		return neighborhood;
	}
}
