package hr.fer.zemris.trisat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SATFormula {

	private int numberOfVariables;
	private Clause[] clauses;

	public SATFormula(int numberOfVariables, Clause[] clauses) {
		this.numberOfVariables = numberOfVariables;
		this.clauses = Arrays.copyOf(clauses, clauses.length);
	}

	public int getNumberOfVariables() {
		return numberOfVariables;
	}

	public int getNumberOfClauses() {
		return clauses.length;
	}

	public Clause getClause(int index) {
		return clauses[index];
	}

	public boolean isSatisfied(BitVector assignment) {
		int n = getNumberOfClauses();
		for (int i = 0; i < n; i++) {
			if (!getClause(i).isSatisfied(assignment)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public String toString() {
		int n = getNumberOfClauses();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < n; i++) {
			sb.append(getClause(i).toString());
		}
		return sb.toString();
	}

	public static SATFormula parseSATFormula(List<String> lines) {

		int varNum = 0;
		List<Clause> clauses = new ArrayList<>();
		for (String line : lines) {
			String trim = line.trim();
			if (!trim.isEmpty()) {
				if (trim.charAt(0) == '%') {
					break;
				} else if (trim.charAt(0) == 'p') {
					varNum = parseNumberOfVars(trim);
				} else if (trim.charAt(0) != 'c') {
					Clause clause = Clause.parseClause(trim);
					clauses.add(clause);
				}
			}
		}

		return new SATFormula(varNum, clauses.toArray(new Clause[0]));
	}

	private static int parseNumberOfVars(String line) {
		String[] split = line.split("\\s+");
		return Integer.parseInt(split[2]);
	}
}
