package hr.fer.zemris.trisat;

public class MutableBitVector extends BitVector {

	public MutableBitVector(boolean... bits) {
		super(bits);
	}

	public MutableBitVector(int n) {
		super(n);
	}

	// zapisuje predanu vrijednost u zadanu varijablu
	public void set(int index, boolean value) {
		if (index >= this.bits.length) {
			throw new IndexOutOfBoundsException(
					"Can't set a bit if given index is out of bounds. ("
							+ index + ")");
		}

		this.bits[index] = value;
	}
}
