package hr.fer.zemris.trisat;

public class SATFormulaStats {

	private SATFormula formula;
	private BitVector assignment;
	private double[] percentages;
	private boolean update;

	public SATFormulaStats(SATFormula formula) {
		this.formula = formula;
		this.percentages = new double[formula.getNumberOfClauses()];
	}

	// analizira se predano rješenje i pamte svi relevantni pokazatelji
	public void setAssignment(BitVector assignment, boolean updatePercentages) {
		this.assignment = assignment;
		this.update = updatePercentages;
	}

	/**
	 * Returns the number of satisfied clauses.
	 * 
	 * @return number of satisfied clauses.
	 */
	public int getNumberOfSatisfied() {
		int num = 0;
		int n = formula.getNumberOfClauses();
		for (int i = 0; i < n; i++) {
			boolean result = formula.getClause(i).isSatisfied(assignment);
			if (result) {
				num++;
			}
			if (update) {
				if (result) {
					percentages[i] += (1 - percentages[i])
							* percentageConstantUp;
				} else {
					percentages[i] += -percentages[i] * percentageConstantDown;
				}
			}
		}
		return num;
	}

	// vraća temeljem onoga što je setAssignment zapamtio
	public boolean isSatisfied() {
		return formula.isSatisfied(assignment);
	}

	private static double percentageConstantUp = 0.01;
	private static double percentageConstantDown = 0.1;
	private static double percentageUnitAmount = 50;

	// vraća temeljem onoga što je setAssignment zapamtio: suma korekcija
	// klauzula
	public double getPercentageBonus() {
		double bonus = 0.0;

		for (int i = 0; i < percentages.length; i++) {
			double percent = percentages[i];
			if (formula.getClause(i).isSatisfied(assignment)) {
				bonus += percentageUnitAmount * (1 - percent);
			} else {
				bonus -= percentageUnitAmount * (1 - percent);
			}
		}
		return bonus;
	}

	// vraća temeljem onoga što je setAssignment zapamtio: procjena postotka za
	// klauzulu
	public double getPercentage(int index) {
		return percentages[index];
	}
}
