package hr.fer.zemris.trisat;

import java.util.Arrays;
import java.util.Random;

public class BitVector {

	protected boolean[] bits;

	public BitVector(Random rand, int numberOfBits) {
		bits = new boolean[numberOfBits];
		for (int i = 0; i < numberOfBits; i++) {
			bits[i] = rand.nextBoolean();
		}
	}

	public BitVector(boolean... bits) {
		this.bits = Arrays.copyOf(bits, bits.length);
	}

	public BitVector(int n) {
		this(new Random(), n);
	}

	// vraća vrijednost index-te varijable
	public boolean get(int index) {
		if (index >= bits.length) {
			throw new IndexOutOfBoundsException("No bit on index " + index
					+ ".");
		}
		return bits[index];
	}

	// vraća broj varijabli koje predstavlja
	public int getSize() {
		return bits.length;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		int n = bits.length;
		for (int i = 0; i < n; i++) {
			sb.append(bits[i] ? '1' : '0');
		}

		return sb.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		return Arrays.equals(this.bits, ((BitVector) obj).bits);
	}
	
	@Override
	public int hashCode() {
		return Arrays.hashCode(this.bits);
	}

	// vraća promjenjivu kopiju trenutnog rješenja
	public MutableBitVector copy() {
		return new MutableBitVector(Arrays.copyOf(bits, bits.length));
	}
}
