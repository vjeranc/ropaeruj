package hr.fer.zemris.optjava.dz6.ant;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import hr.fer.zemris.optjava.dz6.ant.model.Ant;
import hr.fer.zemris.optjava.dz6.ant.model.AntColonyProblem;
import hr.fer.zemris.optjava.dz6.ant.model.AntColonySystem;

public class TSPAntColonySystem extends AntColonySystem {
	
	private double a;
	private double tauMax;
	private double tauMin;
	
	private Ant bestCurrent = null;
	
	private int stagnation = 0;

	public TSPAntColonySystem(AntColonyProblem model, int numOfAnts, int numOfIter) {
		super(model, numOfAnts, numOfIter);
		a = determineA();
		updateTauMaxAndMin(model.getSolutionCost(model.getInitialSolution()));
	}

	private double determineA() {
		return model.getPheromoneMatrixDimension();
	}
	
	@Override
	public void globalPheromoneUpdate(List<Ant> ants) {
		bestCurrent = findBestCurrent(ants);
		double bestCost = bestCurrent.getCost();
		if (bestCost == this.bestCost) stagnation++;
		if (stagnation > this.numOfIter*0.05) {
			initPheromoneMatrix(getTauZero());
			return;
		}
		List<Integer> bestRoute = bestCurrent.getMoves();
		int n = bestRoute.size();
		for(int i = 0; i < n; i++) {
			int from = bestRoute.get(i);
			int to = bestRoute.get((i+1) % n);
			double updateValue = this.pheromoneMatrix[from][to] + 1/bestCost;
			updateValue = updateValue > tauMax ? tauMax : updateValue;
			this.pheromoneMatrix[from][to] = updateValue;
			this.pheromoneMatrix[to][from] = updateValue;
		}		
	}

	private Ant findBestCurrent(List<Ant> ants) {
		
		Ant bestAnt = Collections.min(ants, new Comparator<Ant>() {

			@Override
			public int compare(Ant o1, Ant o2) {
				return Double.valueOf(o1.getCost()).compareTo(o2.getCost());
			}
		});
		
		return bestAnt;
	}

	@Override
	public void globalEvaporationUpdate() {
		int n = pheromoneMatrix.length;
		for(int i = 0; i < n; i++) {
			for(int j = i; j < n; j++) {
				double updateValue = (1-ro)*this.pheromoneMatrix[i][j];
				updateValue = updateValue < tauMin ? tauMin : updateValue;
				this.pheromoneMatrix[i][j] = updateValue;
				this.pheromoneMatrix[j][i] = updateValue;
			}
		}
		
	}

	@Override
	public double getTauZero() {
		return tauMax;
	}

	@Override
	public void updateSolutionToBest() {
		double cost = bestCurrent.getCost();
		if (this.bestCost > cost) {
			this.bestCost = cost;
			this.bestSolution = bestCurrent.getMoves();
			updateTauMaxAndMin(cost);
		}
	}

	private void updateTauMaxAndMin(double cost) {
		tauMax = 1/(this.ro*cost);
		tauMin = tauMax/a;
	}

}
