package hr.fer.zemris.optjava.dz6.ant;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import hr.fer.zemris.optjava.dz6.ant.model.AntColonyProblem;

public class TSProblem implements AntColonyProblem {
	
	double ro;
	double alpha;
	double beta;
	
	double[][] heuristic;
	double[][] distance;
	
	private List<Integer> initialSolution;
	private Map<Integer, List<Integer>> searchSpace;
	private Set<Integer> leftOver;
	/** Number of cities. */
	int N;
	
	/** Random generator for the next move. */
	Random rand = new Random();
	
	
	
	public TSProblem(double ro, double alpha, double beta, double[][] distance, Map<Integer, List<Integer>> searchSpace) {
		this.ro = ro;
		this.alpha = alpha;
		this.beta = beta;
		this.distance = distance;
		this.heuristic = calculateHeuristicData();
		this.searchSpace = searchSpace;
		this.initialSolution = getInitialSolution(distance.length);
		this.leftOver = new HashSet<>(initialSolution); // all of the different nodes
		this.N = initialSolution.size();
	}
	
	private double[][] calculateHeuristicData() {
		int n = distance.length;
		heuristic = new double[n][n];
		for(int i = 0; i < n; i++) {
			for (int j = i; j < n; j++) {
				heuristic[i][j] = 1.0/Math.pow(distance[i][j], beta);
				heuristic[j][i] = heuristic[i][j];
			}
		}
		return heuristic;
	}

	public TSProblem(double ro, double alpha, double beta, double[][] distance, double[][] heuristic, Map<Integer, List<Integer>> searchSpace, List<Integer> initialSolution) {
		this.ro = ro;
		this.alpha = alpha;
		this.beta = beta;
		this.distance = distance;
		this.heuristic = heuristic;
		this.searchSpace = searchSpace;
		this.initialSolution = initialSolution;
		this.leftOver = new HashSet<>(initialSolution); // all of the different nodes
		this.N = initialSolution.size();
	}

	private List<Integer> getInitialSolution(int n) {
		List<Integer> sol = getListFrom(0, n);
		Collections.shuffle(sol);
		return sol;
	}
	
	public static List<Integer> getListFrom(int i, int n) {
		List<Integer> list = new ArrayList<>(n-i+1);
		for (int j = i; j < n; j++) {
				list.add(j);
		}
		return list;
	}

	@Override
	public List<Integer> getInitialSolution() {
		return initialSolution;
	}

	@Override
	public int getPheromoneMatrixDimension() {
		return N;
	}

	@Override
	public double ro() {
		return ro;
	}

	@Override
	public AntColonyProblem copy() {
		return new TSProblem(ro, alpha, beta, distance, heuristic, searchSpace, initialSolution);
	}

	@Override
	public boolean canWalk() {
		return !leftOver.isEmpty();
	}

	
	@Override
	public Integer getNextMove(double[][] pheromoneMatrix, int currentPosition) {
		List<Integer> possibleMoves = getPossibleMoves(currentPosition);
		if (possibleMoves.contains(currentPosition)) throw new IllegalArgumentException();
		List<Double> probs = calculateProbabilities(possibleMoves, currentPosition, pheromoneMatrix);
		// proportional selection
		double sum = 0.0;
		double index = rand.nextDouble();
		int n = probs.size();
		for(int i = 0; i < n; i++) {
			sum += probs.get(i);
			if (index < sum) {
				Integer nextMove = possibleMoves.get(i);
				leftOver.remove(nextMove);
				return nextMove;
			}
		}
		throw new IllegalArgumentException();
	}

	private List<Double> calculateProbabilities(List<Integer> possibleMoves, int currentPosition,
			double[][] pheromoneMatrix) {
		List<Double> weights = calculateWeights(possibleMoves, currentPosition, pheromoneMatrix);
		double sum = sumProbs(weights);
		int n = weights.size();
		for (int i = 0; i < n; i++) {
			weights.set(i, weights.get(i)/sum);
		}
		return weights; // now contains probabilities
	}

	private double sumProbs(List<Double> weights) {
		double sum = 0.0;
		for (Double weight : weights) {
			sum += weight;
		}
		return sum;
	}

	private List<Double> calculateWeights(List<Integer> possibleMoves, int currentPosition, double[][] pheromoneMatrix) {
		List<Double> weights = new ArrayList<>();
		for (Integer nextPosition : possibleMoves) {
			weights.add(calculateWeight(currentPosition, nextPosition, pheromoneMatrix));
		}
		return weights;
	}

	private Double calculateWeight(int i, Integer j, double[][] pheromoneMatrix) {
		return Math.pow(pheromoneMatrix[i][j], alpha)*getEta(i, j);
	}

	private List<Integer> getPossibleMoves(int i) {
		List<Integer> neighbors = new ArrayList<>(searchSpace.get(i));
		if (Collections.disjoint(neighbors, leftOver)) {
			return new ArrayList<>(leftOver);
		} else { 
			neighbors.retainAll(leftOver);
			return neighbors;
		}
	}

	@Override
	public double getSolutionCost(List<Integer> path) {
		int n = path.size();
		double cost = 0.0;
		for (int i = 0; i < n; i++) {
			cost += getDistance(path.get(i), path.get((i+1)%n));
		}
		return cost;
	}
	
	private double getDistance(int i, int j) {
		return distance[i][j];
	}
	
	private double getEta(int i, int j) {
		return heuristic[i][j];
	}

	@Override
	public int getStartingPosition() {
		int start = rand.nextInt(N);
		leftOver.remove(initialSolution.get(start));
		return initialSolution.get(start);
	}

}
