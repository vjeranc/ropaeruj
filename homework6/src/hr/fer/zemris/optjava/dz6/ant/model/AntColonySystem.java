package hr.fer.zemris.optjava.dz6.ant.model;

import hr.fer.zemris.optjava.dz6.TSPSolver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class AntColonySystem {

	/** Number of ants. */
	protected int numOfAnts;
	/** Pheromone evaporation parameter. */
	protected double ro;
	/** Number of search iterations. */
	protected int numOfIter;
	/** Model by which the search is guided. */
	protected AntColonyProblem model;
	/** Matrix of the pheromones between the pair transitions. */
	protected double[][] pheromoneMatrix;
	/** Service used to parallelize the ants. */
	protected ExecutorService pool;
	protected int numberOfCores = Runtime.getRuntime().availableProcessors();
	
	/** Keeps the best solution found so far. */
	protected List<Integer> bestSolution;
	protected double bestCost;
	
	/**
	 * Constructs the {@link AntColonySystem} from the given parameters.
	 * 
	 * @param model
	 *            by which the search is guided
	 * @param numOfAnts
	 *            number of ants
	 * @param numOfIter
	 *            number of search iterations
	 */
	public AntColonySystem(AntColonyProblem model, int numOfAnts, int numOfIter) {
		this.model = model;
		this.numOfAnts = numOfAnts;
		this.numOfIter = numOfIter;
		this.ro = model.ro();
		// pheromone matrix is initialized so that every location has its
		// pairing one
		int n = model.getPheromoneMatrixDimension();
		this.pheromoneMatrix = new double[n][n];
		this.pool = Executors.newFixedThreadPool(numberOfCores);
		this.bestSolution = model.getInitialSolution();
		this.bestCost = model.getSolutionCost(bestSolution);
	}

	protected void initPheromoneMatrix(double tauZero) {
		int n = pheromoneMatrix.length;
		for(int i = 0; i < n;i++){
			for(int j = i; j < n; j++) {
				pheromoneMatrix[i][j] = tauZero;
				pheromoneMatrix[j][i] = tauZero;
			}
		}
		
	}
	
	public List<Integer> solveGivenProblem() {		
		initPheromoneMatrix(getTauZero());
		TSPSolver.showSolution(bestSolution);
		System.out.println(model.getSolutionCost(bestSolution));
		while(numOfIter-- > 0) {
			List<Ant> ants = new ArrayList<>();

			for (int i = 0; i < numOfAnts; ++i) {
				Ant ant = new Ant(model.copy(), pheromoneMatrix);
				ants.add(ant);
			}

			// blocks this main thread and after the ants finish
			// this thread is unblocked.
			try {
				pool.invokeAll(ants);
			} catch (InterruptedException e) {
				System.err.println("Ants failed and program too. ABORTING...");
				e.printStackTrace();
				System.exit(-1);
			}

			globalEvaporationUpdate();
			globalPheromoneUpdate(ants);
			updateSolutionToBest();
		}
		
		pool.shutdown();
		
		return bestSolution;
	}

	public abstract void updateSolutionToBest();

	public abstract void globalEvaporationUpdate();

	public abstract void globalPheromoneUpdate(List<Ant> ants);
	
	public abstract double getTauZero();
}
