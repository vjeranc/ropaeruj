package hr.fer.zemris.optjava.dz6.ant.model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class Ant implements Callable<String>{

	private AntColonyProblem model;
	private double[][] pheromoneMatrix;
	
	private List<Integer> moves;
	Double cost = null;
	
	
	public Ant(AntColonyProblem model, double[][] pheromoneMatrix) {
		super();
		this.model = model;
		this.pheromoneMatrix = pheromoneMatrix;
		this.moves = new ArrayList<>();
	}

	public void startWalk(){
		int current = model.getStartingPosition();
		moves.add(current);
		while(model.canWalk()) {
			Integer nextMove = model.getNextMove(pheromoneMatrix, current);
			moves.add(nextMove);
			current = nextMove;
		}
	};
	
	public List<Integer> getMoves() {
		return moves;
	}
	
	public AntColonyProblem getModel() {
		return model;
	}
	
	public double getCost() {
		if (cost == null) {
			cost = model.getSolutionCost(moves);
		}
		return cost;
	}
	
	@Override
	public String call() throws Exception {
		try {
		startWalk();
		} catch (Exception ex) {
			ex.printStackTrace(System.err);
		}
		return "DONE!";
	}

}
