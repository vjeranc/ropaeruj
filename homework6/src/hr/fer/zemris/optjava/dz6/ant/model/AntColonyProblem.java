package hr.fer.zemris.optjava.dz6.ant.model;

import java.util.List;

public interface AntColonyProblem {
	
	List<Integer> getInitialSolution();
	
	int getPheromoneMatrixDimension();

	double ro();
	
	/** 
	 * Must be a true copy of itself, no shared state between copies.
	 * @return copy of this AntColonyProblem formulation
	 */
	AntColonyProblem copy();

	boolean canWalk();

	Integer getNextMove(double[][] pheromoneMatrix, int currentPosition);
	
	double getSolutionCost(List<Integer> path);

	int getStartingPosition();

}
