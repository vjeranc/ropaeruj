package hr.fer.zemris.optjava.dz6;

import hr.fer.zemris.optjava.dz6.ant.TSPAntColonySystem;
import hr.fer.zemris.optjava.dz6.ant.TSProblem;
import hr.fer.zemris.optjava.dz6.ant.model.AntColonyProblem;
import hr.fer.zemris.optjava.dz6.ant.model.AntColonySystem;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TSPSolver {
	public static void main(String[] args) throws IOException {
		Path distances = Paths.get(args[0]);
		Integer k = Integer.parseInt(args[1]); // number of nearest neighbors
		Integer l = Integer.parseInt(args[2]); // number of ants in colony
		Integer maxiter = Integer.parseInt(args[3]); // maximum number of
														// iterations

		double[][] distanceMatrix = parseDistanceMatrix(Files.readAllLines(distances, StandardCharsets.UTF_8));
		System.out.println(Arrays.deepToString(distanceMatrix));
		Map<Integer, List<Integer>> nearestKNeighbors = getNearestKNeigbors(k, distanceMatrix);
		
		AntColonyProblem tspModel = new TSProblem(0.02, 1.5, 3, distanceMatrix, nearestKNeighbors);
		AntColonySystem colony = new TSPAntColonySystem(tspModel, l, maxiter);
		
/*att48*///List<Integer> optSol = new ArrayList<>(Arrays.asList(new Integer[]{1,8,38,31,44,18,7,28,6,37,19,27,17,43,30,36,46,33,20,47,21,32,39,48,5,42,24,10,45,35,4,26,2,29,34,41,16,22,3,23,14,25,13,11,12,15,40,9}));
/*ch150*///List<Integer> optSol = new ArrayList<>(Arrays.asList(new Integer[]{1,98,103,82,95,107,5,100,143,97,146,26,75,18,142,85,65,132,137,50,55,58,141,83,56,90,46,92,54,138,134,131,32,23,38,67,43,109,51,20,25,110,81,29,86,135,70,108,102,114,99,19,2,37,6,28,9,42,120,47,139,40,53,118,24,12,116,101,41,57,39,127,69,36,61,11,148,130,17,66,60,140,117,129,27,31,123,74,13,106,91,119,68,128,45,71,44,64,112,136,145,144,49,147,72,80,14,122,77,133,15,78,21,150,115,4,104,22,125,149,62,3,113,10,94,88,121,79,59,16,111,105,33,126,52,93,124,35,96,89,8,7,84,30,63,48,73,76,34,87}));
			
//		for (int i = 0; i < optSol.size(); i++) {
//			optSol.set(i, optSol.get(i)-1);
//		}
//		showSolution(optSol);
//		System.out.println(tspModel.getSolutionCost(optSol));
		List<Integer> solution = colony.solveGivenProblem();
		showSolution(solution);
		System.out.println(tspModel.getSolutionCost(solution));
	}

	public static void showSolution(List<Integer> solution) {
		int x = solution.indexOf(0);
		Collections.rotate(solution, -x);
		for (Integer integer : solution) {
			System.out.print((integer+1) + " ");
		}
		System.out.println();
	}

	private static Map<Integer, List<Integer>> getNearestKNeigbors(Integer k, final double[][] distanceMatrix) {
		int n = distanceMatrix.length;
		Map<Integer, List<Integer>> map = new HashMap<Integer, List<Integer>>();
		for(int i = 0; i < n; i++) {
			final int base = i;
			List<Integer> nums = getListFromWithout(0, n, i);
			Collections.sort(nums, new Comparator<Integer>() {

				@Override
				public int compare(Integer o1, Integer o2) {
					Double dist1 = distanceMatrix[base][o1];
					Double dist2 = distanceMatrix[base][o2];
					return dist1.compareTo(dist2);
				}
				
			});
			// take k closest
			map.put(i, new ArrayList<>(nums.subList(0, k)));
		}
		
		return map;
	}

	private static List<Integer> getListFromWithout(int i, int n, int x) {
		List<Integer> list = new ArrayList<>(n-i+1);
		for (int j = i; j < n; j++) {
			if (j != x) {
				list.add(j);
			}
		}
		return list;
	}

	private static double[][] parseDistanceMatrix(List<String> lines) {
		int n = lines.size();
		int dim = 0;
		int startIndex = 0;
		boolean isEdge = false;
		for (int i = 0; i < n; i++) {
			String line = lines.get(i);
			if (line.contains("EDGE_WEIGHT_SECTION")) {
				startIndex = i+1;
				isEdge = true;
				break;
			} else if (line.contains("NODE_COORD_SECTION")) {
				startIndex = i+1;
				break;
			} else if (line.contains("DIMENSION")) {
				String[] split = line.trim().split("\\s+");
				dim = Integer.parseInt(split[split.length - 1]);
			}
		}
		if (isEdge) {
			return parseEdgeMatrix(dim, lines.subList(startIndex, startIndex+dim));
		} else {
			return parseNodeMatrix(dim, lines.subList(startIndex, startIndex+dim-1));
		}

	}

	private static double[][] parseNodeMatrix(int dim, List<String> lines) {

		double[][] xyCooOfNodes = parseCoordinatesOfNodes(lines, dim);
		double[][] matrix = calculateDistanceMatrixFromNodes(xyCooOfNodes, dim);
		return matrix;
	}

	private static double[][] calculateDistanceMatrixFromNodes(double[][] nodes, int dim) {
		double[][] matrix = new double[dim][dim];
		for(int i = 0; i < dim; i++) {
			for(int j = i; j < dim; j++) {
				double x1 = nodes[i][0];
				double y1 = nodes[i][1];
				double x2 = nodes[j][0];
				double y2 = nodes[j][1];
				matrix[i][j] = Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
				matrix[j][i] = matrix[i][j];
			}
		}
		return matrix;
	}

	private static double[][] parseCoordinatesOfNodes(List<String> lines, int dim) {
		double[][] coo = new double[dim][2];
		for (int i = 0; i < lines.size(); i++) {
			String line = lines.get(i);
			String[] split = line.trim().split("\\s+");
			coo[i][0] = Double.parseDouble(split[1]);
			coo[i][1] = Double.parseDouble(split[2]);
		}
		return coo;
	}

	private static double[][] parseEdgeMatrix(int dim, List<String> lines) {
		double[][] matrix = new double[dim][dim];
		for (int i = 0; i < lines.size(); i++) {
			String line = lines.get(i);
			List<Double> doubles = parseEdgeLine(line);
			for(int j = 0; j < dim; j++) {
				matrix[i][j] = doubles.get(j);
			}
		}
		return matrix;
	}

	private static List<Double> parseEdgeLine(String line) {
		String[] split = line.trim().split("\\s+");
		List<Double> arr = new ArrayList<>();
		for (int i = 0; i < split.length; i++) {
			arr.add(Double.parseDouble(split[i]));
		}
		return arr;
	}

}
