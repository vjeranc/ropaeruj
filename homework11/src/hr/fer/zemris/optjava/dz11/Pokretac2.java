package hr.fer.zemris.optjava.dz11;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import hr.fer.zemris.art.GrayScaleImage;
import hr.fer.zemris.generic.ga.GA;
import hr.fer.zemris.generic.ga.GASolution;
import hr.fer.zemris.generic.ga.eval.evalimpl.Evaluator;
import hr.fer.zemris.generic.ga.impl.AdditionMutation;
import hr.fer.zemris.generic.ga.impl.BLXAlpha;
import hr.fer.zemris.generic.ga.impl.ImageSolution;
import hr.fer.zemris.generic.ga.impl.ParallelEvalGA;
import hr.fer.zemris.generic.ga.impl.ParallelGA;
import hr.fer.zemris.generic.ga.impl.SplitCrossOver;
import hr.fer.zemris.generic.ga.impl.SwapMutation;
import hr.fer.zemris.generic.ga.impl.TournamentSelection;
import hr.fer.zemris.generic.ga.service.impl.GAQueueService;
import hr.fer.zemris.generic.ga.service.model.JobService;

public class Pokretac2 {
	public static void main(String[] args) throws IOException {
		GrayScaleImage template = GrayScaleImage.load(Paths.get(args[0]).toFile());
		int n = Integer.parseInt(args[1]);
		Integer popSize = Integer.parseInt(args[2]);
		Integer maxIter = Integer.parseInt(args[3]);
		Double maxFitness = Double.parseDouble(args[4]);
		Path output = Paths.get(args[5]);

		JobService service = new GAQueueService(Runtime.getRuntime().availableProcessors(), template);

		GA<int[]> ga = new ParallelGA<int[]>(new ImageSolution(n), 
				new TournamentSelection<int[]>(7), new SwapMutation(), new SplitCrossOver(),
				new Evaluator(template),
				popSize, maxIter, maxFitness, service, 25);
		
		GASolution<int[]> image = ga.start();
		service.shutdown();
		GrayScaleImage out = new GrayScaleImage(template.getWidth(), template.getHeight());
		new Evaluator(template).draw(image, out);
		out.save(output.toFile());
	}
}
