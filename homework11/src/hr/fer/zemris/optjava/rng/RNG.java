package hr.fer.zemris.optjava.rng;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Properties;

public class RNG {
	private static IRNGProvider rngProvider;
	static {
		Properties props = new Properties();
		try (InputStream is = RNG.class.getClassLoader().getResourceAsStream("rng-config.properties")){
			props.load(is);
			is.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String rngProvider = props.getProperty("rng-provider");
		try {
			RNG.rngProvider = (IRNGProvider) RNG.class.getClassLoader().loadClass(rngProvider).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static IRNG getRNG() {
		return rngProvider.getRNG();
	}
}
