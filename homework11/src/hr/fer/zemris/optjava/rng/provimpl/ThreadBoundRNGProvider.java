package hr.fer.zemris.optjava.rng.provimpl;

import hr.fer.zemris.optjava.rng.IRNG;
import hr.fer.zemris.optjava.rng.IRNGProvider;
import hr.fer.zemris.optjava.rng.RNG;

public class ThreadBoundRNGProvider implements IRNGProvider {

	@Override
	public IRNG getRNG() {
		IRNGProvider thread = (IRNGProvider) Thread.currentThread();
		return thread.getRNG();
	}

}
