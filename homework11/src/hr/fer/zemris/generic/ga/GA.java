package hr.fer.zemris.generic.ga;

import hr.fer.zemris.generic.ga.eval.IGAEvaluator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GA<T> {

	protected List<GASolution<T>> population;
	protected ISelection<T> selection;
	protected IMutation<T> mutation;
	protected ICrossOver<T> crossover;
	private IGAEvaluator<T> evaluator;
	protected Integer popSize;
	private Integer maxiter;
	private double maxFitness;
	
	protected GASolution<T> best;
	
	public GA(GASolution<T> entity, ISelection<T> selection, IMutation<T> mutation,
			ICrossOver<T> crossover, IGAEvaluator<T> evaluator, Integer popSize, Integer maxiter, double minFitness) {
		super();
		this.population = createNewPopulation(entity, popSize);
		this.selection = selection;
		this.mutation = mutation;
		this.crossover = crossover;
		this.evaluator = evaluator;
		this.popSize = popSize;
		this.maxiter = maxiter;
		this.maxFitness = minFitness;
	}
	
	public GASolution<T> start() {
		evaluate(population);
		best = Collections.max(population);
		Integer iter = 0;
		while(best.fitness < maxFitness && iter++ < maxiter) {
			step(iter);
		}
		
		return best;
	}

	protected void step(Integer iter) {
		population = buildNewPopulation();
		evaluate(population);
		best = Collections.max(population);
		if (iter % 10 == 0) System.out.println(best.fitness + " " + iter);
	}
	
	protected List<GASolution<T>> buildNewPopulation() {
		List<GASolution<T>> nPop = new ArrayList<>();

		while(nPop.size() < this.popSize) {
			List<GASolution<T>> parents = selection.select(population, 2);
			GASolution<T> parent1 = parents.get(0);
			GASolution<T> parent2 = parents.get(1);
			List<GASolution<T>> children = crossover.crossOver(parent1, parent2);
			mutateChildren(children);
			nPop.addAll(children);
		}
		
		
		return nPop;
	}

	protected void mutateChildren(List<GASolution<T>> children) {
		for (GASolution<T> gaSolution : children) {
			mutation.mutate(gaSolution);
		}
		
	}

	protected void evaluate(List<GASolution<T>> population) {
		for (GASolution<T> gaSolution : population) {
			evaluator.evaluate(gaSolution);
		}
	}

	private List<GASolution<T>> createNewPopulation(GASolution<T> entity, Integer popSize) {
		List<GASolution<T>> pop = new ArrayList<>();
		for(int i = 0; i < popSize; i++) pop.add(entity.newRandomInstance());
		return pop;
	}
	
	
	
}
