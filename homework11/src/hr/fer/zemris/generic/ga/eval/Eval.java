package hr.fer.zemris.generic.ga.eval;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@SuppressWarnings("unchecked")
public class Eval {
	private static IGAEvaluatorProvider<?> evalProvider;
	static {
		Properties props = new Properties();
		try (InputStream is = Eval.class.getClassLoader().getResourceAsStream("eval-config.properties")){
			props.load(is);
			is.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String evalProvider = props.getProperty("eval-provider");
		try {
			Eval.evalProvider = (IGAEvaluatorProvider<?>) Eval.class.getClassLoader().loadClass(evalProvider).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static <T> IGAEvaluator<T> getEvaluator() {
		return (IGAEvaluator<T>) evalProvider.getEvaluator();
	}
}
