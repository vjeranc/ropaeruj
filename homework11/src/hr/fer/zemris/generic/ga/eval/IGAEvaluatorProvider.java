package hr.fer.zemris.generic.ga.eval;

public interface IGAEvaluatorProvider<T> {
	IGAEvaluator<T> getEvaluator();
}
