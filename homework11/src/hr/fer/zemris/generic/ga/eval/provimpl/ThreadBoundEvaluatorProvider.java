package hr.fer.zemris.generic.ga.eval.provimpl;

import hr.fer.zemris.generic.ga.eval.IGAEvaluator;
import hr.fer.zemris.generic.ga.eval.IGAEvaluatorProvider;

public class ThreadBoundEvaluatorProvider<T> implements IGAEvaluatorProvider<T> {


	@Override
	public IGAEvaluator<T> getEvaluator() {
		@SuppressWarnings("unchecked")
		IGAEvaluatorProvider<T> thread = (IGAEvaluatorProvider<T>) Thread.currentThread();
		return thread.getEvaluator();
	}



}
