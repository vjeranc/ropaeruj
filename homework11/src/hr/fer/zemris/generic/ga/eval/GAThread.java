package hr.fer.zemris.generic.ga.eval;

import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.sound.midi.VoiceStatus;

import hr.fer.zemris.art.GrayScaleImage;
import hr.fer.zemris.generic.ga.eval.evalimpl.Evaluator;
import hr.fer.zemris.optjava.rng.IRNG;
import hr.fer.zemris.optjava.rng.IRNGProvider;
import hr.fer.zemris.optjava.rng.rngimpl.RNGRandomImpl;

public class GAThread extends Thread implements IGAEvaluatorProvider<int[]>, IRNGProvider{

	private IRNG rng = new RNGRandomImpl();
	
	Evaluator eval;
	
	GrayScaleImage template;
	Queue<Callable<?>> pending;
	Queue results;
	
	public boolean isLive = true;
	
	

	public GAThread(GrayScaleImage template, ConcurrentLinkedQueue<Callable<?>> pending, ConcurrentLinkedQueue results) {
		super();
		this.template = template;
		this.eval = new Evaluator(template);
		this.pending = pending;
		this.results = results;
	}

	@Override
	public void run() {
		while(true) {
			while(isLive && pending.isEmpty());
			if (!isLive) return;
			Callable<?> task = pending.poll();
			if (task != null) {
				try {
					Object result = task.call();
					if (result != null) results.add(result);
					else                results.add(new Object());
				} catch (Exception e) {
					System.err.println("Calling failed!");
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public IGAEvaluator<int[]> getEvaluator() {
		return eval;
	}

	@Override
	public IRNG getRNG() {
		return rng;
	}

}
