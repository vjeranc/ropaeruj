package hr.fer.zemris.generic.ga;

import java.util.List;

public interface ISelection<T> {
	GASolution<T> select(List<GASolution<T>> population);

	List<GASolution<T>> select(List<GASolution<T>> population, int n);
}
