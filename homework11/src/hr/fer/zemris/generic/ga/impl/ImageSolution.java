package hr.fer.zemris.generic.ga.impl;

import java.util.Arrays;
import java.util.Random;

import hr.fer.zemris.generic.ga.GASolution;
import hr.fer.zemris.optjava.rng.RNG;

public class ImageSolution extends GASolution<int[]> {

	int n;
	
	
	/**
	 * Number of rectangles to approximate the image.
	 * @param n number of rectangles
	 */
	public ImageSolution(int n) {
		this.n = n;
	}
	
	private ImageSolution(int n, int[] data) {
		this.n = n;
		this.data = data;
	}

	@Override
	public GASolution<int[]> duplicate() {
		int[] ndata = Arrays.copyOf(data, n*5 + 1);
		return new ImageSolution(n, ndata);
	}

	
	@Override
	public GASolution<int[]> newRandomInstance() {
		int size = n * 5 + 1; // +1 for background color
		Random rand = new Random();
		int[] ndata = new int[size];
		for(int i = 0; i < size; i++) {
			ndata[i] = rand.nextInt(256);
		}
		return new ImageSolution(n, ndata);
	}

}
