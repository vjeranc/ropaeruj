package hr.fer.zemris.generic.ga.impl;

import java.util.Random;

import hr.fer.zemris.generic.ga.GASolution;
import hr.fer.zemris.generic.ga.IMutation;

public class AdditionMutation implements IMutation<int[]> {

	private static Random rand = new Random();
	@Override
	public void mutate(GASolution<int[]> solution) {
		int[] data = solution.getData();
		int i = rand.nextInt(data.length);
		data[i] = (data[i] + (rand.nextBoolean() ? -1 : 1) + 256) % 256;
	}

}
