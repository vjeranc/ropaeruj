package hr.fer.zemris.generic.ga.impl;

import java.util.Random;

import hr.fer.zemris.generic.ga.GASolution;
import hr.fer.zemris.generic.ga.IMutation;
import hr.fer.zemris.optjava.rng.IRNG;
import hr.fer.zemris.optjava.rng.RNG;

public class SwapMutation implements IMutation<int[]> {


	@Override
	public void mutate(GASolution<int[]> solution) {
		int[] data = solution.getData();
		IRNG rand = RNG.getRNG();
		if (rand.nextInt(0,100) < 40) {
			int i = rand.nextInt(0,data.length - 5);

			int k = rand.nextInt(0,data.length - 5);

			int[] d = new int[5];
			for (int in = 0; in < 5; in++) {
				d[in] = data[k + in];
				data[k + in] = data[i + in];
			}
			for (int in = 0; in < 5; in++) {
				data[i + in] = d[in];
			}
		} else {
			int i = rand.nextInt(0,data.length);
			data[i] += rand.nextBoolean() ? 1 : -1;
		}

	}

}
