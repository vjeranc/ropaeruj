package hr.fer.zemris.generic.ga.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import hr.fer.zemris.generic.ga.GASolution;
import hr.fer.zemris.generic.ga.ICrossOver;
import hr.fer.zemris.optjava.rng.RNG;

public class SplitCrossOver implements ICrossOver<int[]> {

	public static Random rand = new Random();

	@Override
	public List<GASolution<int[]>> crossOver(GASolution<int[]> p1, GASolution<int[]> p2) {
		GASolution<int[]> c1 = p1.duplicate();
		GASolution<int[]> c2 = p2.duplicate();
		int[] data1 = c1.getData();
		int[] data2 = c2.getData();
		int i = RNG.getRNG().nextInt(0,data1.length);
		int start = i > data1.length / 2 ? i : 0;
		int end = i > data1.length / 2 ? data1.length : i;
		for (int j = start; j < end; j++) {
			int h = data1[j];
			data1[j] = data2[j];
			data2[j] = h;
		}

		return new ArrayList<>(Arrays.asList(c1, c2));
	}
}
