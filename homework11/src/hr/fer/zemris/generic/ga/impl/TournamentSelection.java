package hr.fer.zemris.generic.ga.impl;


import hr.fer.zemris.generic.ga.GASolution;
import hr.fer.zemris.generic.ga.ISelection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class TournamentSelection<T> implements ISelection<T>{

	private static final Random rand = new Random();
	int k;
	public TournamentSelection(int size) {
		this.k = size;
	}
	
	/**
	 * Selects {@code b} best from population. 
	 * @param population
	 * @param k tournament size
	 * @param b
	 * @return
	 */
	@Override
	public GASolution<T> select(List<GASolution<T>> population) {
		Collections.shuffle(population);
		population = population.subList(0, k);

		return Collections.max(population);
	}

	@Override
	public List<GASolution<T>> select(List<GASolution<T>> population, int n) {
		Collections.shuffle(population);
		population = population.subList(0, k);
		Collections.sort(population, new Comparator<GASolution<T>>() {

			@Override
			public int compare(GASolution<T> o1, GASolution<T> o2) {
				// TODO Auto-generated method stub
				return -o1.compareTo(o2);
			}
		});
		return population.subList(0, n);
	}

}
