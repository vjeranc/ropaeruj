package hr.fer.zemris.generic.ga.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import hr.fer.zemris.generic.ga.GASolution;
import hr.fer.zemris.generic.ga.ICrossOver;
import hr.fer.zemris.optjava.rng.IRNG;
import hr.fer.zemris.optjava.rng.RNG;

public class BLXAlpha implements ICrossOver<int[]>{

	@Override
	public List<GASolution<int[]>> crossOver(GASolution<int[]> p1, GASolution<int[]> p2) {
		GASolution<int[]> child = p1.duplicate();
		int[] c = child.getData();
		int[] d1 = p1.getData(), d2 = p2.getData();
		int n = d1.length;
		for (int i = 0; i < n; i++) {
			int c1 = d1[i];
			int c2 = d2[i];
			int min = c1 > c2 ? c2 : c1;
			int max = c1 > c2 ? c1 : c2;
			int I = (max - min)/3;

			int val = getRandomFromInterval(RNG.getRNG(), Math.max(0, min - I), Math.min(max + I, 256));
			c[i] = val;
		}
		return Arrays.asList(child);
	}
	
	private int getRandomFromInterval(IRNG rand, int minimum, int maximum) {
		return rand.nextInt(minimum, maximum+1);
	}
}
