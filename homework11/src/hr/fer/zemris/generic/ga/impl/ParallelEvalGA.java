package hr.fer.zemris.generic.ga.impl;

import java.util.List;
import java.util.concurrent.Callable;

import hr.fer.zemris.generic.ga.GA;
import hr.fer.zemris.generic.ga.GASolution;
import hr.fer.zemris.generic.ga.ICrossOver;
import hr.fer.zemris.generic.ga.IMutation;
import hr.fer.zemris.generic.ga.ISelection;
import hr.fer.zemris.generic.ga.eval.Eval;
import hr.fer.zemris.generic.ga.eval.IGAEvaluator;
import hr.fer.zemris.generic.ga.service.model.JobService;

public class ParallelEvalGA<T> extends GA<T> {

	
	private JobService service;
	
	/**
	 * 
	 * @param entity solution entity
	 * @param selection operator
	 * @param mutation operator
	 * @param crossover operator
	 * @param evaluator of fitness
	 * @param popSize population size
	 * @param maxiter maximum iteration
	 * @param maxFitness maximum fitness
	 * @param n number of threads
	 */
	public ParallelEvalGA(GASolution<T> entity, ISelection<T> selection, IMutation<T> mutation,
			ICrossOver<T> crossover, IGAEvaluator<T> evaluator, Integer popSize, Integer maxiter, double maxFitness, JobService service) {
		super(entity, selection, mutation, crossover, evaluator, popSize, maxiter, maxFitness);
		this.service = service;
	}
	
	@Override
	protected void evaluate(List<GASolution<T>> population) {
		for(final GASolution<T> e : population)
		service.add(new Callable<Void>() {

			@SuppressWarnings("unchecked")
			@Override
			public Void call() throws Exception {
				Eval.getEvaluator().evaluate((GASolution<Object>) e);
				return null;
			}
			
		});
		service.getResult();
	}	 

}
