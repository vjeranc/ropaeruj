package hr.fer.zemris.generic.ga.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

import hr.fer.zemris.generic.ga.GA;
import hr.fer.zemris.generic.ga.GASolution;
import hr.fer.zemris.generic.ga.ICrossOver;
import hr.fer.zemris.generic.ga.IMutation;
import hr.fer.zemris.generic.ga.ISelection;
import hr.fer.zemris.generic.ga.eval.IGAEvaluator;
import hr.fer.zemris.generic.ga.service.model.JobService;

public class ParallelGA<T> extends GA<T> {

	JobService service;
	int size;
	/**
	 * 
	 * @param entity
	 * @param selection
	 * @param mutation
	 * @param crossover
	 * @param evaluator
	 * @param popSize
	 * @param maxiter
	 * @param minFitness
	 * @param service
	 * @param size of the job population
	 */
	public ParallelGA(GASolution<T> entity, ISelection<T> selection, IMutation<T> mutation, ICrossOver<T> crossover,
			IGAEvaluator<T> evaluator, Integer popSize, Integer maxiter, double minFitness, JobService service, int size) {
		super(entity, selection, mutation, crossover, evaluator, popSize, maxiter, minFitness);
		this.service = service;
		this.size = size;
	}
	
	public class ParJob implements Callable<List<GASolution<T>>> {

		int n;
		
		public ParJob(int n) {
			this.n = n;
		}
		@Override
		public List<GASolution<T>> call() throws Exception {
			List<GASolution<T>> nPop = new ArrayList<>();

			while(nPop.size() < n) {
				List<GASolution<T>> parents = selection.select(population, 2);
				GASolution<T> parent1 = parents.get(0);
				GASolution<T> parent2 = parents.get(1);
				List<GASolution<T>> children = crossover.crossOver(parent1, parent2);
				mutateChildren(children);
				nPop.addAll(children);
			}
			
			return nPop;
		}
		
	}
	
	@Override
	protected void step(Integer iter) {
		int sub = 0;
		while (sub < popSize) {
			service.add(new ParJob(Math.min(size, popSize-sub)));
			sub += size;
		}
		List<GASolution<T>> nPop = new ArrayList<>();
		for(Object piece : service.getResult()) {
			List<GASolution<T>> lPop = (List<GASolution<T>>) piece;
			nPop.addAll(lPop);
		}
		population = nPop;
		evaluate(population);
		best = Collections.max(population);
		if (iter % 10 == 0) System.out.println(best.fitness + " " + iter);
	}

}
