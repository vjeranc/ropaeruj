package hr.fer.zemris.generic.ga.service.model;

import java.util.List;
import java.util.concurrent.Callable;

public interface JobService {

	<T> void add(Callable<T> job);
	
	<T> List<T> getResult();

	void shutdown();
}
