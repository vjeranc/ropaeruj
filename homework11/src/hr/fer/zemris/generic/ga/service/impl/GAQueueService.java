package hr.fer.zemris.generic.ga.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;

import hr.fer.zemris.art.GrayScaleImage;
import hr.fer.zemris.generic.ga.eval.GAThread;
import hr.fer.zemris.generic.ga.eval.IGAEvaluatorProvider;
import hr.fer.zemris.generic.ga.service.model.JobService;

public class GAQueueService implements JobService {

	private List<GAThread> threads;
	
	private ConcurrentLinkedQueue<Callable<?>> pending;
	private ConcurrentLinkedQueue<?> results;
	
	int inQueue = 0;
	
	/**
	 * 
	 * @param n number of threads
	 */
	public GAQueueService(int n, GrayScaleImage template) {
		this.threads = new ArrayList<>();
		this.pending = new ConcurrentLinkedQueue<>();
		this.results = new ConcurrentLinkedQueue<>();
		for(int i = 0; i < n; i++) {
			threads.add(new GAThread(template, pending, results));
		}
		for(GAThread thread : threads) {
			thread.start();
		}
	}
	
	@Override
	public <T> void add(Callable<T> job) {
		pending.add(job); inQueue++;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public <T> List<T> getResult() {
		while(!pending.isEmpty());
		int k = 0;
		ArrayList list = new ArrayList<>();
		while (k < inQueue) {
			Object result = results.poll();
			if (result != null) {
				list.add(result);
				k++;
			}
		}
		inQueue = 0;
		return (List<T>) list;
	}

	@Override
	public void shutdown() {
		for(GAThread thread : threads) {
			thread.isLive = false;
		}
		
	}

}
