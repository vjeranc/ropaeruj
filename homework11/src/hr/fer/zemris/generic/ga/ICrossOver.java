package hr.fer.zemris.generic.ga;

import java.util.List;

public interface ICrossOver<T> {
	List<GASolution<T>> crossOver(GASolution<T> p1, GASolution<T> p2);
}
