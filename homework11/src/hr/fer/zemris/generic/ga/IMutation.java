package hr.fer.zemris.generic.ga;

public interface IMutation<T> {
	void mutate(GASolution<T> solution);
}
