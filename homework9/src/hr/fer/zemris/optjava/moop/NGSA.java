package hr.fer.zemris.optjava.moop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.text.html.MinimalHTMLWriter;

public class NGSA {
	private double sshare; // \sigma_{share}
	private double alpha;
	private int popSize;
	private boolean decision; // decision space or objective space distancing
	private List<Unit> population;
	private int maxiter;
	ISelection selection;
	ICrossOver crossover;
	IMutation mutation;
	MOOPProblem pp;
	
	
	
	public NGSA(int popSize, double sshare, double alpha, ISelection selection, ICrossOver crossover, IMutation mutation, MOOPProblem pp, int maxiter, boolean decision) {
		super();
		this.popSize = popSize;
		this.pp = pp;
		this.population = initializePopulation(popSize, pp.getNumberOfArguments(), pp.getNumberOfObjectives());
		this.sshare = sshare;
		this.alpha = alpha;
		this.selection = selection;
		this.crossover = crossover;
		this.mutation = mutation;
		this.maxiter = maxiter;
		this.decision = decision;
	}

	public List<Unit> start() {
		
		int iter = 0;
		while (iter++ < maxiter) {
			// 1. evaluate the population
			for (Unit unit : population) {
				unit.evaluate(pp);
			}
			// 2. now we can make pareto set
			List<List<Unit>> fronts = constructParetoSets();

			// 2.5 calculating the sharing function
			calculateSharingFunction();

			// 3. now we need to calculate the fitness

			setFitnessToUnits(fronts);

			// 4. create a new generation using various operators (selection,
			// mutation, crossover)

			createNewPopulation();
		}
		
		for (Unit unit : population) {
			unit.evaluate(pp);
		}
		return constructParetoSets().get(0);
	}



	private void createNewPopulation() {
		List<Unit> npop = new ArrayList<>();
	
		while(npop.size() < popSize) {
			Unit parent1 = selection.selectFrom(population);
			Unit parent2 = selection.selectFrom(population);
			List<Unit> children = crossover.crossOver(parent1, parent2);
			mutateChildren(children); // mutation
			npop.addAll(children);
		}
		
		population = npop;
		
	}

	private void mutateChildren(List<Unit> children) {
		for (Unit unit : children) {
			mutation.mutate(unit, pp.getMinBounds(), pp.getMaxBounds());
		}
		
	}

	private void calculateSharingFunction() {
		for(int i = 0; i < popSize-1; i++) {
			Unit unit1 = population.get(i);
			for (int j = i; j < popSize; j++) {
				Unit unit2 = population.get(j);
				double distance = unit1.distanceTo(unit2, pp.getMaxBounds(),  pp.getMinBounds(), decision);
				double sh = sharingFunction(distance);
				unit1.addSh(sh);
				unit2.addSh(sh);
			}
		}
	}

	private void setFitnessToUnits(List<List<Unit>> fronts) {
		double prevFitness = population.size();
		for (List<Unit> list : fronts) {
			for (Unit unit : list) {
				unit.setFitness(prevFitness);
				unit.changeFitness(); // scale according to summed up sharing functions
			}
			// find the minimum fitness, that's new fitness for the next front.
			
			prevFitness = 0.99*Collections.min(list).getFitness(); // reducing its value by a little
		}
		
	}

	private List<List<Unit>> constructParetoSets() {
		Map<Integer, Set<Integer>> sets = new HashMap<>();
		
		int n = population.size();
		
		for(int i = 0; i < n; i++) {
			sets.put(i, new HashSet<Integer>());
		}
		
		for (int i = 0; i < n; i++) {
			Unit unit1 = population.get(i);
			for (int j = 0; j < n; j++) {
				Unit unit2 = population.get(j);
				if (unit1 != unit2) {
					if (unit2.dominates(unit1)) {
						sets.get(i).add(j);
					}
				}
			}
		}
		
		// O(M*N^2) 
		List<List<Integer>> fronts = new ArrayList<>();
		// after we have for each unit a list of its dominators, we'll find the first front and continue from there
		int k = 0; // front index
		while(!sets.isEmpty()) {
			// building a front
			for (Entry<Integer, Set<Integer>> entry : sets.entrySet()) {
				Set<Integer> set = entry.getValue();
				if (set.isEmpty()) {
					Integer key = entry.getKey();
					if (fronts.size() == k) { // if front doesn't exist
						fronts.add(k, new ArrayList<Integer>());
					}
					fronts.get(k).add(key);
				}
			}
			if (fronts.size() > k) {
				// removing empty sets of added elements
				for (Integer num : fronts.get(k)) {
					sets.remove(num);
				}
				// removing elements in created front from sets
				for (Entry<Integer, Set<Integer>> entry : sets.entrySet()) {
					Set<Integer> set = entry.getValue();
					for (Integer num : fronts.get(k)) {
						set.remove(num);
					}
				}
			}
			k++; // next front
		}
		
		// mapping integer to Unit
		List<List<Unit>> frontsR = new ArrayList<>();
		int j = 0;
		for (List<Integer> list : fronts) {
			frontsR.add(new ArrayList<Unit>());
			for(Integer num : list) {
				frontsR.get(j).add(population.get(num));
			}
			j++;
		}
		// mapToUnit = map (map (get population)), beautiful haskell sintax, where get population index is the function
		return frontsR;
	}

	private List<Unit> initializePopulation(int popSize2, int n, int m) {
		List<Unit> pop = new ArrayList<>();
		for(int i = 0; i < popSize2; i++) {
			pop.add(new Unit(n, m, 
					pp.getMinBounds(), 
					pp.getMaxBounds())
			);
		}
		return pop;
	}




	private double sharingFunction(double distance) {
		if (distance < sshare) {
			return 1 - Math.pow(distance/sshare, alpha);
		} else {
			return 0;
		}
	}
}
