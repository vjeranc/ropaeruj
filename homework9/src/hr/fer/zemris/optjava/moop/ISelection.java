package hr.fer.zemris.optjava.moop;

import java.util.List;

public interface ISelection {

	Unit selectFrom(List<Unit> population);
}
