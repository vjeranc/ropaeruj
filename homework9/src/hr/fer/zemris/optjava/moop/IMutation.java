package hr.fer.zemris.optjava.moop;

public interface IMutation {
	void mutate(Unit u, double[] minBounds, double[] maxBounds);
}
