package hr.fer.zemris.optjava.moop;

import java.util.Collections;
import java.util.List;
import java.util.Random;

public class ProportionalSelection implements ISelection {

	private Random rand = new Random();
	
	public ProportionalSelection() {
		super();
	}
	
	@Override
	public Unit selectFrom(List<Unit> population) {
		Double[] scale = getScale(population);
		Double index = rand.nextDouble();
		for (int i = 0; i < scale.length; i++) {
			if (index < scale[i]) {
				return population.get(i);
			}
		}
		// never reached
		return population.get(scale.length-1);
	}

	
	private Double[] getScale(List<Unit> population) {
		Double worstFitness = Collections.min(population).getFitness();
		Double[] scale = new Double[population.size()];
		int i = 0;
		double sum = 0.0;
		for (Unit unit : population) {
			scale[i] = unit.getFitness() - worstFitness;
			sum += scale[i];
			i++;
		}

		scale[0] /= sum;
		for (int j = 1; j < scale.length; j++) {
			scale[j] /= sum;
			scale[j] += scale[j-1];
		}
		return scale;
	}
}