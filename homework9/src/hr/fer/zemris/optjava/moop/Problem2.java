package hr.fer.zemris.optjava.moop;


public class Problem2 implements MOOPProblem {

	private double[] minBounds = new double[] {0.1, 0};
	private double[] maxBounds = new double[] {1,5};
	
	public Problem2() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int getNumberOfArguments() {
		return 2;
	}

	@Override
	public int getNumberOfObjectives() {
		return 2;
	}

	@Override
	public void evaluateSolution(double[] solution, double[] objectives) {
		double x1 = solution[0];
		double x2 = solution[1];

		objectives[0] = x1;
		objectives[1] = (1+x2)/x1;

	}

	@Override
	public double[] getMaxBounds() {
		return maxBounds;
	}

	@Override
	public double[] getMinBounds() {
		return minBounds;
	}

}
