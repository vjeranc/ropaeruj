package hr.fer.zemris.optjava.moop;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Unit implements Comparable<Unit> {

	double[] solution;
	double[] objectives;
	private Double fitness;
	double sharingFunctionAcc;
	private static Random rand = new Random();
	
	/**
	 * 
	 * @param n size of domain
	 * @param m size of codomain
	 */
	public Unit(int n, int m, double[] minBounds, double[] maxBounds) {
		this.solution = random(new double[n], minBounds, maxBounds);
		this.objectives = new double[m];
	}
	
	private double[] random(double[] ds, double[] minBounds, double[] maxBounds) {
		for(int i = 0; i < ds.length; i++) {
			ds[i] = getRandomFromInterval(rand, minBounds[i], maxBounds[i]);
		}
		return ds;
	}
	
	private double getRandomFromInterval(Random rand, double minimum, double maximum) {
		return (minimum + (maximum - minimum)*rand.nextDouble());
	}

	Unit(double[] solution, int m) {
		this.solution = solution;
		this.objectives = new double[m];
	}

	public void evaluate(MOOPProblem pp) {
		pp.evaluateSolution(solution, objectives);
	}
	public double getFitness() {
		return fitness;
	}
	public void changeFitness() {
		fitness /= sharingFunctionAcc;
	}
	
	public void setFitness(double num) {
		fitness = num;
	}
	
	public void addSh(double val) {
		sharingFunctionAcc += val;
	}
	
	/**
	 * Distance from one unit to another.
	 * 
	 * @param that another unit
	 * @param max upper bounds on domain
	 * @param min lower bounds on codomain
	 * @param decision if {@code true} then distance on domain, otherwise on codomain
	 * @return distance
	 */
	public double distanceTo(Unit that, double[] max, double[] min, boolean decision) {
		double d = 0.0;
		if (decision) {
			for (int i = 0; i < solution.length; i++) {
				double dx = (this.solution[i] - that.solution[i]) / (max[i] - min[i]);
				d += dx * dx;
			}
		} else {
			for (int i = 0; i < solution.length; i++) {
				double dx = (this.objectives[i] - that.objectives[i]);
				d += dx * dx;
			}
		}

		return Math.sqrt(d);
	}
	
	public boolean dominates(Unit that) {
		int count = 0;
		for(int i = 0; i < objectives.length; i++) {
			if (this.objectives[i] > that.objectives[i]) {
				return false; // this means we cannot decide
			}
			if (this.objectives[i] == that.objectives[i]) {
				count++;
			}
		}
		return count != objectives.length; // this means that this dominates that, if they are equal then there's no domination
	}

	@Override
	public int compareTo(Unit that) {
		return this.fitness.compareTo(that.fitness);
	}

	
	public double[] getSolution() {
		return solution;
	}
	
	public double[] getObjectives() {
		return objectives;
	}
	
	@Override
	public String toString() {
		return fitness.toString() + " " + sharingFunctionAcc + "|";
	}
	
}
