package hr.fer.zemris.optjava.moop;


public class Problem1 implements MOOPProblem {

	private double[] minBounds = new double[] {-5,-5,-5,-5};
	private double[] maxBounds = new double[] {5,5,5,5};
	
	public Problem1() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int getNumberOfArguments() {
		return 4;
	}

	@Override
	public int getNumberOfObjectives() {
		return 4;
	}

	@Override
	public void evaluateSolution(double[] solution, double[] objectives) {
		double x1 = solution[0];
		double x2 = solution[1];
		double x3 = solution[2];
		double x4 = solution[3];
		
		// scala code val List(x1,x2,x3,x4,_) = solution

		objectives[0] = x1*x1;
		objectives[1] = x2*x2;
		objectives[2] = x3*x3;
		objectives[3] = x4*x4;
	}

	@Override
	public double[] getMaxBounds() {
		return maxBounds;
	}

	@Override
	public double[] getMinBounds() {
		return minBounds;
	}

}
