package hr.fer.zemris.optjava.dz2.function;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

/**
 * Represents a function: <br>
 * <br>
 * Latex: a \text{x1}+b \text{x1}^3 \text{x2}+c e^{d \text{x3}} (\cos (g
 * \text{x4})+1)+f \text{x4} \text{x5}^2-\text{y}
 * 
 * @author vjeran
 * 
 */
public class TransferEquationFunction extends EquationFunction {

	public TransferEquationFunction(double[] coeff) {
		super(coeff);
	}

	@Override
	public int getNumberOfVariables() {
		return 6;
	}

	/**
	 * a Subscript[x, 1] + b Subscript[x, 1]^3 Subscript[x, 2] + c E^(d
	 * Subscript[x, 3]) (1 + Cos[g Subscript[x, 4]]) + f Subscript[x, 4]
	 * Subscript[x, 5]^2 - Subscript[y, 1]
	 * 
	 * @param point
	 * @return
	 */
	@Override
	public double getEquationValueAt(RealVector point) {
		double a = point.getEntry(0);
		double b = point.getEntry(1);
		double c = point.getEntry(2);
		double d = point.getEntry(3);
		double e = point.getEntry(4);
		double f = point.getEntry(5);

		double part1 = a * coeff[0];
		double part2 = b * Math.pow(coeff[0], 3.0) * coeff[1];
		double part3 = c * Math.pow(Math.E, d * coeff[2]) * (1 + Math.cos(e * coeff[3]));
		double part4 = f * coeff[3] * Math.pow(coeff[4], 2.0);

		return part1 + part2 + part3 + part4 - y;
	}

	@Override
	public RealVector getSystemGradientComponent(RealVector point) {
		double a = point.getEntry(0);
		double b = point.getEntry(1);
		double c = point.getEntry(2);
		double d = point.getEntry(3);
		double e = point.getEntry(4);
		double f = point.getEntry(5);
		double x1 = coeff[0];
		double x2 = coeff[1];
		double x3 = coeff[2];
		double x4 = coeff[3];
		double x5 = coeff[4];
		double x5sqr = x5 * x5;
		double x1cub = x1 * x1 * x1;
		double edx3 = Math.pow(Math.E, d * x3);
		double cosex4 = Math.cos(e * x4);
		double longExpr1 = (a * x1 + b * x1cub * x2 + f * x4 * x5sqr - y + c * edx3 * (1 + cosex4));
		//@formatter:off
		double[] arr = new double[] { 
					2 * x1 * longExpr1, 
					2 * x1cub * x2 * longExpr1,
					2 * edx3 * (1 + cosex4) * longExpr1, 
					2 * c * edx3 * x3 * (1 + cosex4) * longExpr1,
					-2 * c * edx3 * x4 * longExpr1 * Math.sin(e * x4), 
					2 * x4 * x5sqr * longExpr1 
				};
		//@formatter:on
		return new ArrayRealVector(arr);
	}

	@Override
	public RealMatrix getSystemHessianComponent(RealVector point) {
		double a = point.getEntry(0);
		double b = point.getEntry(1);
		double c = point.getEntry(2);
		double d = point.getEntry(3);
		double e = point.getEntry(4);
		double f = point.getEntry(5);
		double x1 = coeff[0];
		double x2 = coeff[1];
		double x3 = coeff[2];
		double x4 = coeff[3];
		double x5 = coeff[4];
		double edx3 = Math.pow(Math.E, d * x3);
		double x1sqr = x1 * x1;
		double x1cub = x1sqr * x1;
		double x1qtr = x1cub * x1;
		double x2sqr = x2 * x2;
		double x3sqr = x3 * x3;
		double x4sqr = x4 * x4;
		double x5sqr = x5 * x5;
		double x5qtr = x5sqr * x5sqr;
		double cosex4 = Math.cos(e * x4);
		double sinex4 = Math.sin(e * x4);
		double longExpr1 = (2 * c * edx3 + a * x1 + b * x1cub * x2 + f * x4 * x5sqr - y + 2 * c * edx3 * cosex4);
		//@formatter:off
		double[][] arr = new double[][] {
				{
					2*x1sqr,
					2*x1qtr*x2,
					2*edx3*x1*(1 + cosex4),
					2*c*edx3*x1*x3*(1 + cosex4),
					-2*c*edx3*x1*x4*sinex4,
					2*x1*x4*x5sqr}
					,
				{
					2*x1qtr*x2,
					2*x1cub*x1cub*x2sqr,
					2*edx3*x1cub*x2*(1 + cosex4),
					2*c*edx3*x1cub*x2*x3*(1 + cosex4),
					-2*c*edx3*x1cub*x2*x4*sinex4,
					2*x1cub*x2*x4*x5sqr
				}
					,
				{
					2*edx3*x1*(1 + cosex4),
					2*edx3*x1cub*x2*(1 + cosex4),
					2*edx3*edx3*(1 + cosex4)*(1 + cosex4),
					2*edx3*x3*(1 + cosex4)*longExpr1,
					-2*edx3*x4*longExpr1*sinex4,
					2*edx3*x4*x5sqr*(1 + cosex4)
				}
					,
				{
					2*c*edx3*x1*x3*(1 + cosex4),
					2*c*edx3*x1cub*x2*x3*(1 + cosex4),
					2*edx3*x3*(1 + cosex4)*longExpr1,
					2*c*edx3*x3sqr*(1 + cosex4)*longExpr1,
					-2*c*edx3*x3*x4*longExpr1*sinex4,
					2*c*edx3*x3*x4*x5sqr*(1 + cosex4)
				}
					,
				{
					-2*c*edx3*x1*x4*sinex4,
					-2*c*edx3*x1cub*x2*x4*sinex4,
					-2*edx3*x4*longExpr1*sinex4,
					-2*c*edx3*x3*x4*longExpr1*sinex4,
					2*c*edx3*x4sqr*(-(cosex4*(a*x1 + b*x1cub*x2 + f*x4*x5sqr - y + c*edx3*(1 + cosex4))) + c*edx3*sinex4*sinex4),
					-2*c*edx3*x4sqr*x5sqr*sinex4
				}
					,
				{
					2*x1*x4*x5sqr,
					2*x1cub*x2*x4*x5sqr,
					2*edx3*x4*x5sqr*(1 + cosex4),
					2*c*edx3*x3*x4*x5sqr*(1 + cosex4),
					-2*c*edx3*x4sqr*x5sqr*sinex4,
					2*x4sqr*x5qtr
				}
			};
		//@formatter:on
		return new Array2DRowRealMatrix(arr);
	}

	@Override
	public RealVector getGradientValueAt(RealVector point) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RealMatrix getHessianMatrixOf(RealVector point) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EquationFunction parseEquation(String line) {
		String[] nums = line.substring(1, line.length() - 1).split(",");
		double[] numbers = new double[nums.length];
		for (int i = 0; i < nums.length; i++) {
			numbers[i] = Double.parseDouble(nums[i].trim());
		}
		return new TransferEquationFunction(numbers);
	}

}
