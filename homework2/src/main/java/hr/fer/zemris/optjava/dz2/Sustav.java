package hr.fer.zemris.optjava.dz2;

import hr.fer.zemris.optjava.dz2.function.LinearEquationFunction;
import hr.fer.zemris.optjava.dz2.function.SystemMinimizationFunction;
import hr.fer.zemris.optjava.dz2.function.model.IHFunction;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

public class Sustav {

	public static Set<String> tasks = new HashSet<>(Arrays.asList("grad", "newton"));

	public static void main(String[] args) {
		if (args.length != 3) {
			throw new IllegalArgumentException("Number of arguments should be three!");
		}

		String task;
		int iterNum;
		Path filePath;

		if (!tasks.contains(args[0])) {
			throw new IllegalArgumentException("Invalid task " + args[0] + ". \"grad\" and \"newton\" supported");
		} else {
			task = args[0];
		}

		iterNum = Integer.parseInt(args[1]);
		filePath = Paths.get(args[2]);
		IHFunction function = null;
		try {
			function = SystemMinimizationFunction.parseSystem(Files.readAllLines(filePath,
					StandardCharsets.UTF_8), new LinearEquationFunction(new double[]{1.0,1.0}));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		RealVector startPoint = getRandomRealVector(function.getNumberOfVariables());
		RealVector minimum = null;
		
		switch (task) {
		case "grad":
			minimum = NumOptAlgorithms.getMinimumGradientDescent(function, iterNum, startPoint);
			break;
		case "newton":
			minimum = NumOptAlgorithms.getMinimumNewtonMethod(function, iterNum, startPoint);
			break;

		}
		System.out.println(minimum);
		System.out.println(function.getValueAt(minimum));

	}

	public static RealVector getRandomRealVector(int n) {
		Random rand = new Random();
		RealVector rv = new ArrayRealVector(n);
		for (int i = 0; i < n; i++) {
			rv.setEntry(i, Jednostavno.getRandomFromInterval(rand, -5, 5));
		}
		return rv;
	}
}
