package hr.fer.zemris.optjava.dz2;

import hr.fer.zemris.optjava.dz2.function.model.IFunction;
import hr.fer.zemris.optjava.dz2.function.model.IHFunction;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

public class NumOptAlgorithms {

	/**
	 * Returns the minimum of the function using the gradient descent algorithm.
	 * 
	 * @param function
	 *            multivariate function
	 * @param iterNum
	 *            maximum number of iterations
	 * @return point of minimum
	 */
	public static RealVector getMinimumGradientDescent(IFunction function,
			int iterNum, RealVector startPoint) {
		int iter = 0;
		RealVector point = startPoint;
		while (iter++ < iterNum) {
			RealVector nGradient = function.getGradientValueAt(point)
					.mapMultiplyToSelf(-1.0).unitVector();
			double lambda = findLambdaBisection(function, point, nGradient);
			point = point.add(nGradient.mapMultiply(lambda));
		}
		
		return point;
	}

	private static double findLambdaBisection(IFunction function,
			RealVector point, RealVector nGradient) {
		double l = 0.0;
		double u = findUpper(function, point, nGradient);
		double lambda = 1.0;
		while (true) {
			RealVector npoint = point.add(nGradient.mapMultiply(lambda));
			RealVector valueAtGradient = function.getGradientValueAt(npoint);
			double value = valueAtGradient.dotProduct(nGradient);
			if (u-l < 1E-10) {
				return lambda;
			} else if (value > 0) {
				u = lambda;
			} else {
				l = lambda;
			}
			lambda = (l + u) / 2.0;
		}
	}

	private static double findUpper(IFunction function, RealVector point,
			RealVector nGradient) {
		double upper = 0.00001;
		double maxValue = function.getValueAt(point);
		double current = maxValue;
		while (maxValue >= current) {
			upper *= 2.0;
			current = function.getValueAt(point.add(nGradient.mapMultiply(upper)));
		}
		return upper;
	}

	/**
	 * Returns the minimum of the function using the Newton's method.
	 * 
	 * @param function
	 *            differentiable function
	 * @param iterNum
	 *            maximum number of iterations
	 * @return point of minimum
	 */
	public static RealVector getMinimumNewtonMethod(IHFunction function,
			int iterNum, RealVector startPoint) {
		int iter = 0;
		RealVector point = startPoint;

		while (iter++ < iterNum) {
			RealMatrix iHessian = new LUDecomposition(function.getHessianMatrixOf(point)).getSolver().getInverse();
			RealMatrix nGrad = iHessian.multiply(new Array2DRowRealMatrix(function.getGradientValueAt(point).toArray()));
			RealVector nGradient = nGrad.getColumnVector(0).mapMultiply(-1.0).unitVector();
			double lambda = findLambdaBisection(function, point, nGradient);
			point = point.add(nGradient.mapMultiply(lambda));
		}
		
		return point;
	}
}
