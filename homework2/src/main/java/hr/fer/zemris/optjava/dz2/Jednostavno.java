package hr.fer.zemris.optjava.dz2;

import hr.fer.zemris.optjava.dz2.function.Function1;
import hr.fer.zemris.optjava.dz2.function.Function2;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

public class Jednostavno {

	private static Set<String> validArgs = new HashSet<>(
			Arrays.asList(new String[] { "1a", "1b", "2a", "2b" }));

	private static int minimum = -5;
	private static int maximum = 5;

	public static void main(String[] args) {
		if (args.length != 2 && args.length != 4) {
			throw new IllegalArgumentException(
					"Number of arguments is wrong, should be two or four not "
							+ args.length + ".");
		}

		if (!validArgs.contains(args[0])) {
			throw new IllegalArgumentException(
					"First argument should be the number and letter of the task. Example: \"1a\"");
		}

		String task = args[0];
		Integer iterNum = Integer.parseInt(args[1]);
		RealVector startPoint;

		if (args.length == 4) {
			startPoint = new ArrayRealVector(new double[] {
					Double.parseDouble(args[2]), Double.parseDouble(args[3]) });
		} else {
			Random rand = new Random();
			startPoint = new ArrayRealVector(new double[] {
					getRandomFromInterval(rand, minimum, maximum),
					getRandomFromInterval(rand, minimum, maximum) });
		}

		switch (task) {
		case "1a":
			NumOptAlgorithms.getMinimumGradientDescent(new Function1(),
					iterNum, startPoint);
			break;
		case "1b":
			NumOptAlgorithms.getMinimumNewtonMethod(new Function1(), iterNum,
					startPoint);
			break;
		case "2a":
			NumOptAlgorithms.getMinimumGradientDescent(new Function2(),
					iterNum, startPoint);
			break;
		case "2b":
			NumOptAlgorithms.getMinimumNewtonMethod(new Function2(), iterNum,
					startPoint);
			break;
		}

	}

	public static double getRandomFromInterval(Random rand, int minimum,
			int maximum) {
		return minimum + rand.nextInt(maximum - minimum + 1)
				- rand.nextDouble() + 1;
	}
}
