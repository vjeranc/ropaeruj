package hr.fer.zemris.optjava.dz4.part2;

import hr.fer.zemris.optjava.dz4.ga.GeneticAlgorithmSteadyImpl;
import hr.fer.zemris.optjava.dz4.ga.impl.BoxFillingEntity;
import hr.fer.zemris.optjava.dz4.ga.impl.BoxFillingPopulation;
import hr.fer.zemris.optjava.dz4.ga.impl.EmptyCrossOver;
import hr.fer.zemris.optjava.dz4.ga.impl.ExchangeMutation;
import hr.fer.zemris.optjava.dz4.ga.impl.TournamentSelection;
import hr.fer.zemris.optjava.dz4.ga.impl.TournamentWorstSelection;
import hr.fer.zemris.optjava.dz4.ga.model.ICrossOver;
import hr.fer.zemris.optjava.dz4.ga.model.IMutation;
import hr.fer.zemris.optjava.dz4.ga.model.ISelection;
import hr.fer.zemris.optjava.dz4.ga.model.ISingleEntity;
import hr.fer.zemris.optjava.dz4.ga.model.ISteadyPopulation;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


public class BoxFilling {

	public static void main(String[] args) {
		Path filePath = Paths.get(args[0]);
		ArrayList<Integer> sticks = parseSticks(filePath);
		Integer populationSize = Integer.parseInt(args[1]);
		Integer nBest = Integer.parseInt(args[2]);
		Integer mWorst = Integer.parseInt(args[3]);
		Boolean testChild = Boolean.parseBoolean(args[4]);
		Integer maxIterNum = Integer.parseInt(args[5]);
		Double  acceptableFitness = Double.parseDouble(args[6]);
		
		IMutation mutator = new ExchangeMutation();
		ICrossOver crosser = new EmptyCrossOver();
		
		ISelection selectionOfBest = new TournamentSelection(nBest);
		ISelection selectionOfWorst = new TournamentWorstSelection(mWorst);
		
		ISingleEntity entity = new BoxFillingEntity(sticks, 20, mutator, crosser);
		ISteadyPopulation population = new BoxFillingPopulation(entity, populationSize);
		GeneticAlgorithmSteadyImpl ga = new GeneticAlgorithmSteadyImpl(population, acceptableFitness, maxIterNum, selectionOfBest, selectionOfWorst, 0.0, testChild);
		ISingleEntity solution = ga.start();
		System.out.println(((BoxFillingEntity)solution).boxes + " " + solution.toString());
	}
	
	public static ArrayList<Integer> parseSticks(Path filePath) {
		List<String> lines = null;
		try {
			lines = Files.readAllLines(filePath, StandardCharsets.UTF_8);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(-1);
		}
		String[] sticks = lines.get(0).substring(1, lines.get(0).length()-1).split(",");
		ArrayList<Integer> sticksLengths = new ArrayList<>();
		for (String stickLength : sticks) {
			sticksLengths.add(Integer.parseInt(stickLength.trim()));
		}
		return sticksLengths;
	}

}
