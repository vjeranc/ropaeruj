package hr.fer.zemris.optjava.dz4.ga;

import hr.fer.zemris.optjava.dz4.ga.model.ISelection;
import hr.fer.zemris.optjava.dz4.ga.model.ISingleEntity;
import hr.fer.zemris.optjava.dz4.ga.model.ISteadyPopulation;

import java.util.List;
import java.util.Random;

public class GeneticAlgorithmSteadyImpl {

	ISteadyPopulation population;
	Integer populationSize;
	Double minForStop;
	Integer maxNumOfGenerations;
	ISelection selectionOfBest;
	ISelection selectionOfWorst;
	Double sigma;
	Random rand = new Random();
	Boolean testChild;
	
	public GeneticAlgorithmSteadyImpl(ISteadyPopulation population, Double minForStop, Integer maxNumOfGenerations,
			ISelection selectionOfBest, ISelection selectionOfWorst, Double sigma, Boolean testChild) {
		super();
		this.population = population;
		this.populationSize = population.getSize();
		this.minForStop = minForStop;
		this.maxNumOfGenerations = maxNumOfGenerations;
		this.selectionOfBest = selectionOfBest;
		this.selectionOfWorst = selectionOfWorst;
		this.sigma = sigma;
		this.testChild = testChild;
	}
	
	public ISingleEntity start() {
		Integer generationNumber = 0;
		Double evaluation = population.getBestEntity().fitness();
		System.out.println(evaluation + " " + population.getBestEntity());
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while (Math.abs(minForStop - evaluation) > 1E-14 && generationNumber++ <= maxNumOfGenerations) {
			population = buildNewPopulation(population.getSize());
			evaluation = population.getBestEntity().fitness();
			System.out.println(evaluation + " " + population.getBestEntity());
		}
		
		return population.getBestEntity();
	}

	private ISteadyPopulation buildNewPopulation(Integer size) {
		ISingleEntity parent1 = selectionOfBest.selectFrom(population);
		ISingleEntity parent2 = selectionOfBest.selectFrom(population);
		List<ISingleEntity> children = parent1.crossOver(parent2);
		mutateChildren(children); // mutation

		if (testChild) {
			ISingleEntity kickOut = selectionOfWorst.selectFrom(population);
			ISingleEntity child = children.get(0);
			if (child.fitness() > kickOut.fitness()) {
				int i = population.getEntities().indexOf(kickOut);
				population.setEntity(i, child);
			}
		}
		
		return population;
	}
	
	private void mutateChildren(List<ISingleEntity> children) {
		for (ISingleEntity child : children) {
			child.mutate(sigma);
		}
	}
	
}
