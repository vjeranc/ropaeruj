package hr.fer.zemris.optjava.dz4.ga.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import hr.fer.zemris.optjava.dz4.ga.model.ICrossOver;
import hr.fer.zemris.optjava.dz4.ga.model.IMutation;
import hr.fer.zemris.optjava.dz4.ga.model.ISingleEntity;

public class BoxFillingEntity implements ISingleEntity {

	
	private ArrayList<Integer> sticks;
	private Integer maxHeight;
	
	private Double fitness = null;
	public int boxes = 0;
	private IMutation mutator;
	private ICrossOver crosser;
	
	
	
	public BoxFillingEntity(ArrayList<Integer> sticks, Integer maxHeight, IMutation mutator,
			ICrossOver crosser) {
		super();
		this.sticks = sticks;
		this.maxHeight = maxHeight;
		this.mutator = mutator;
		this.crosser = crosser;
	}

	@Override
	public Double fitness() {
		if (fitness == null) {
			int numOfBoxes = 1;
			int sum = 0;
			for (Integer stickLength : sticks) {
				if (sum + stickLength > maxHeight) {
					numOfBoxes++;
					sum = 0;
				}
				sum += stickLength;
			}
			boxes = numOfBoxes;
			fitness = (double) -numOfBoxes;
			return -fitness;
		} else {
			return fitness;
		}
	}

	@Override
	public ISingleEntity copy() {
		return new BoxFillingEntity(new ArrayList<Integer>(sticks), maxHeight, mutator, crosser);
	}

	@Override
	public ISingleEntity randomInstance() {
		ArrayList<Integer> copy = new ArrayList<Integer>(sticks);
		Collections.shuffle(copy);
		return new BoxFillingEntity(copy, maxHeight, mutator, crosser);
	}

	@Override
	public List<ISingleEntity> crossOver(ISingleEntity entity) {
		return crosser.crossOver(this, entity);
	}

	@Override
	public void mutate(double sigma) {
		mutator.mutate(sticks);
	}

	@Override
	public Comparator<ISingleEntity> getComparator() {
		return new Comparator<ISingleEntity>() {

			@Override
			public int compare(ISingleEntity o1, ISingleEntity o2) {
				return o1.fitness().compareTo(o2.fitness());
			}
			
		};
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		int sum = 0;
		for(Integer stickLength: sticks) {
			if (sum + stickLength > maxHeight) {
				sb.append("|");
				sum = 0;
			}
			sb.append(stickLength+" ");
			sum += stickLength;
		}
		return sb.toString().trim();
	}

}
