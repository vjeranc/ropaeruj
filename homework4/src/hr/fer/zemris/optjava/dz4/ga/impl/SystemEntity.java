package hr.fer.zemris.optjava.dz4.ga.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

import hr.fer.zemris.optjava.dz2.function.model.IFunction;
import hr.fer.zemris.optjava.dz4.ga.model.ISingleEntity;

public class SystemEntity implements ISingleEntity {

	private IFunction function;
	protected RealVector solution;
	private static Random rand = new Random();
	
	
	public SystemEntity(IFunction function) {
		this(function, getRandomRealVector(function.getNumberOfVariables())); 
	}
	
	public SystemEntity(IFunction function, RealVector solution) {
		super();
		this.function = function;
		this.solution = solution;
	}

	@Override
	public Double fitness() {
		return -function.getValueAt(solution);
	}

	@Override
	public ISingleEntity copy() {
		return new SystemEntity(function, solution);
	}

	@Override
	public ISingleEntity randomInstance() {
		return new SystemEntity(function);
	}

	@Override
	public List<ISingleEntity> crossOver(ISingleEntity entity) {
		if (entity instanceof SystemEntity) {
			SystemEntity ent = (SystemEntity) entity;
			RealVector sol1 = this.solution;
			RealVector sol2 = ent.solution;
			int n = sol1.getDimension();
			RealVector rv = new ArrayRealVector(function.getNumberOfVariables());
			for(int i = 0; i < n; i++) {
				double c1 = sol1.getEntry(i);
				double c2 = sol2.getEntry(i);
				double min = c1 > c2 ? c2 : c1;
				double max = c1 > c2 ? c1 : c2;
				double I = max - min;
				
				double val = getRandomFromInterval(rand, min-I, max+I);
				rv.setEntry(i, val);
			}
			List<ISingleEntity> child = new ArrayList<>();
			child.add(new SystemEntity(function, rv));
			return child;
		} else {
			throw new IllegalArgumentException("Entities must be same!");
		}
	}

	@Override
	public void mutate(double sigma) {
		int n = solution.getDimension();
		for (int i = 0; i < n; i++) {
			solution.addToEntry(i, rand.nextGaussian()*sigma-sigma/2);
		}
	}

	@Override
	public Comparator<ISingleEntity> getComparator() {
		return new Comparator<ISingleEntity>() {

			@Override
			public int compare(ISingleEntity o1, ISingleEntity o2) {
				return o1.fitness().compareTo(o2.fitness());
			}
		};
	}
	
	private double getRandomFromInterval(Random rand, double minimum, double maximum) {
		return (minimum + (maximum - minimum)*rand.nextDouble());
	}
	
	public static double getRandomFromInterval(Random rand, int minimum,
			int maximum) {
		return minimum + rand.nextInt(maximum - minimum + 1)
				- rand.nextDouble() + 1;
	}
	
	public static RealVector getRandomRealVector(int n) {
		RealVector rv = new ArrayRealVector(n);
		for (int i = 0; i < n; i++) {
			rv.setEntry(i, getRandomFromInterval(rand, -1, 1));
		}
		return rv;
	}
	
	@Override
	public String toString() {
		return solution.toString();
	}

}
