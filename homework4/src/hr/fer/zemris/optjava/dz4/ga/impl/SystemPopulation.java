package hr.fer.zemris.optjava.dz4.ga.impl;

import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.optjava.dz2.function.model.IFunction;
import hr.fer.zemris.optjava.dz4.ga.model.IPopulation;
import hr.fer.zemris.optjava.dz4.ga.model.ISingleEntity;

public class SystemPopulation extends AbstractPopulation {

	public SystemPopulation(List<ISingleEntity> entities) {
		super(entities);
	}

	public SystemPopulation(IFunction function, Integer populationSize) {
		super(getEntities(function, populationSize));
	}

	@Override
	public IPopulation getNewInstance(List<ISingleEntity> entities) {
		return new SystemPopulation(entities);
	}

	private static List<ISingleEntity> getEntities(IFunction function, Integer popSize) {
		List<ISingleEntity> entities = new ArrayList<>();
		for (int i = 0; i < popSize; i++) {
			entities.add(new SystemEntity(function));
		}
		return entities;
	}
}
