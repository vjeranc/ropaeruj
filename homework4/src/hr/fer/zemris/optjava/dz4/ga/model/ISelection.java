package hr.fer.zemris.optjava.dz4.ga.model;

public interface ISelection {

	ISingleEntity selectFrom(IPopulation population);
	
}
