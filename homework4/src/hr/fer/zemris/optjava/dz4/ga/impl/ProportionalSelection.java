package hr.fer.zemris.optjava.dz4.ga.impl;

import hr.fer.zemris.optjava.dz4.ga.model.IPopulation;
import hr.fer.zemris.optjava.dz4.ga.model.ISelection;
import hr.fer.zemris.optjava.dz4.ga.model.ISingleEntity;

import java.util.List;
import java.util.Random;

public class ProportionalSelection implements ISelection {

	private Random rand = new Random();
	
	public ProportionalSelection() {
		super();
	}
	
	@Override
	public ISingleEntity selectFrom(IPopulation population) {
		Double[] scale = getScale(population);
		Double index = rand.nextDouble();
		for (int i = 0; i < scale.length; i++) {
			if (index < scale[i]) {
				return population.getEntity(i);
			}
		}
		// never reached
		return population.getEntity(scale.length-1);
	}

	
	private Double[] getScale(IPopulation population) {
		Double worstFitness = population.getWorstEntity().fitness();
		List<ISingleEntity> entities = population.getEntities();
		Double[] scale = new Double[entities.size()];
		int i = 0;
		double sum = 0.0;
		for (ISingleEntity entity : entities) {
			scale[i] = worstFitness - entity.fitness();
			sum += scale[i];
			i++;
		}

		scale[0] /= sum;
		for (int j = 1; j < scale.length; j++) {
			scale[j] /= sum;
			scale[j] += scale[j-1];
		}
		return scale;
	}
}
