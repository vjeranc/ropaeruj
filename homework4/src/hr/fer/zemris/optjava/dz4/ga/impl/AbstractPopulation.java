package hr.fer.zemris.optjava.dz4.ga.impl;

import hr.fer.zemris.optjava.dz4.ga.model.IPopulation;
import hr.fer.zemris.optjava.dz4.ga.model.ISingleEntity;

import java.util.Collections;
import java.util.List;
import java.util.Random;

public abstract class AbstractPopulation implements IPopulation {

	protected List<ISingleEntity> entities;
	private Random rand = new Random();
	private ISingleEntity best = null;
	private ISingleEntity worst = null;
	
	public AbstractPopulation(List<ISingleEntity> entities) {
		super();
		this.entities = entities;
	}

	@Override
	public ISingleEntity getRandomEntity() {
		return entities.get(rand.nextInt(entities.size()));
	}

	@Override
	public List<ISingleEntity> getNRandomEntities(int n) {
		Collections.shuffle(entities, rand);
		return Collections.unmodifiableList(entities.subList(0, n));
	}

	@Override
	public Integer getSize() {
		return entities.size();
	}

	@Override
	public ISingleEntity getBestEntity() {
		if (best != null) {
			return best;
		} else {
			return Collections.max(entities, entities.get(0).getComparator());
		}
	}

	@Override
	public ISingleEntity getNthBestEntity(int n) {
		Collections.sort(entities, entities.get(0).getComparator());
		return entities.get(n);
	}

	@Override
	public ISingleEntity getWorstEntity() {
		if (worst != null) {
			return worst;
		} else {
			return Collections.min(entities, entities.get(0).getComparator());
		}
	}

	@Override
	public List<ISingleEntity> getEntities() {
		return Collections.unmodifiableList(entities);
	}

	@Override
	public ISingleEntity getEntity(int i) {
		return entities.get(i);
	}

}
