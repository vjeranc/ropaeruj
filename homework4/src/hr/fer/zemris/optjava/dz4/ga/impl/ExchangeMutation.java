package hr.fer.zemris.optjava.dz4.ga.impl;

import hr.fer.zemris.optjava.dz4.ga.model.IMutation;

import java.util.ArrayList;
import java.util.Random;

public class ExchangeMutation implements IMutation {

	private Random rand = new Random();
	private int numOfExchanges = 1000;
	
	
	public ExchangeMutation() {
		super();
	}



	@Override
	public <T> void mutate(ArrayList<T> solution) {
		int n = solution.size();
		int i = rand.nextInt(n);
		int j = rand.nextInt(n);
		if (i == j) {
			mutate(solution);
		}
		T first = solution.get(i);
		T second = solution.get(j);
		solution.set(j, first);
		solution.set(i, second);
		if (numOfExchanges>0) {
			numOfExchanges--;
			mutate(solution);
		} else {
			numOfExchanges = 10;
		}
	}

}
