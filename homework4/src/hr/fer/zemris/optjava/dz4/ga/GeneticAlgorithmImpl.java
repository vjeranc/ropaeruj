package hr.fer.zemris.optjava.dz4.ga;

import hr.fer.zemris.optjava.dz4.ga.model.IPopulation;
import hr.fer.zemris.optjava.dz4.ga.model.ISelection;
import hr.fer.zemris.optjava.dz4.ga.model.ISingleEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GeneticAlgorithmImpl {

	IPopulation population;
	Integer populationSize;
	Double minForStop;
	Integer maxNumOfGenerations;
	ISelection selection;
	Double sigma;
	Random rand = new Random();
	
	public GeneticAlgorithmImpl(IPopulation population, Double minForStop, Integer maxNumOfGenerations,
			ISelection selection, Double sigma) {
		super();
		this.population = population;
		this.populationSize = population.getSize();
		this.minForStop = minForStop;
		this.maxNumOfGenerations = maxNumOfGenerations;
		this.selection = selection;
		this.sigma = sigma;
	}
	
	public ISingleEntity start() {
		Integer generationNumber = 0;
		Double evaluation = population.getBestEntity().fitness();
		System.out.println(evaluation + " " + population.getBestEntity());
		while (Math.abs(minForStop - evaluation) > 1E-14 && generationNumber++ <= maxNumOfGenerations) {
			population = buildNewPopulation(population.getSize());
			evaluation = population.getBestEntity().fitness();
			System.out.println(evaluation + " " + population.getBestEntity());
		}
		
		return population.getBestEntity();
	}

	private IPopulation buildNewPopulation(Integer size) {
		List<ISingleEntity> entities = new ArrayList<>();
		// elitism
		entities.add(population.getBestEntity());
		entities.add(population.getNthBestEntity(1));
		
		while(entities.size() < size) {
			ISingleEntity parent1 = selection.selectFrom(population);
			ISingleEntity parent2 = selection.selectFrom(population);
			List<ISingleEntity> children = parent1.crossOver(parent2);
			mutateChildren(children); // mutation
			entities.addAll(children);
		}
		
		return population.getNewInstance(entities);
	}
	
	private void mutateChildren(List<ISingleEntity> children) {
		for (ISingleEntity child : children) {
			child.mutate(sigma);
		}
	}
	
}
