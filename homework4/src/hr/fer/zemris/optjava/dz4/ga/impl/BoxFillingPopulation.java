package hr.fer.zemris.optjava.dz4.ga.impl;

import hr.fer.zemris.optjava.dz4.ga.model.IPopulation;
import hr.fer.zemris.optjava.dz4.ga.model.ISingleEntity;

import java.util.ArrayList;
import java.util.List;

public class BoxFillingPopulation extends AbstractSteadyPopulation {

	public BoxFillingPopulation(List<ISingleEntity> entities) {
		super(entities);
	}
	
	public BoxFillingPopulation(ISingleEntity entity, Integer popSize) {
		this(getEntities(entity, popSize));
	}

	private static List<ISingleEntity> getEntities(ISingleEntity entity, Integer popSize) {
		List<ISingleEntity> entities = new ArrayList<>();
		for(int i = 0; i < popSize; i++) {
			entities.add(entity.randomInstance());
		}
		
		return entities;
	}

	@Override
	public IPopulation getNewInstance(List<ISingleEntity> entities) {
		return new BoxFillingPopulation(entities);
	}

}
