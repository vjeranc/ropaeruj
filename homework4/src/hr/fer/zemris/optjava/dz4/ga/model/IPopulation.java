package hr.fer.zemris.optjava.dz4.ga.model;

import java.util.List;

public interface IPopulation {

	ISingleEntity getRandomEntity();
	
	List<ISingleEntity> getNRandomEntities(int n);
	
	Integer getSize();
	
	
	ISingleEntity getBestEntity();
	/**
	 * Indexing starts from 0;
	 * @param n which best to take
	 * @return nth best entity
	 */
	ISingleEntity getNthBestEntity(int n);
	
	ISingleEntity getWorstEntity();
	
	List<ISingleEntity> getEntities();
	
	ISingleEntity getEntity(int i);
	
	IPopulation getNewInstance(List<ISingleEntity> entities);
	
}
