package hr.fer.zemris.optjava.dz4.ga.impl;

import java.util.Collections;
import java.util.List;

import hr.fer.zemris.optjava.dz4.ga.model.IPopulation;
import hr.fer.zemris.optjava.dz4.ga.model.ISelection;
import hr.fer.zemris.optjava.dz4.ga.model.ISingleEntity;

public class TournamentWorstSelection implements ISelection {
	
	/** Tournament size. */
	private Integer n;
	
	public TournamentWorstSelection(Integer size) {
		super();
		this.n = size;
	}

	@Override
	public ISingleEntity selectFrom(IPopulation population) {
		List<ISingleEntity> tournamentMembers = population.getNRandomEntities(n);
		ISingleEntity best = 
				Collections.min(tournamentMembers, population.getEntity(0).getComparator());
		
		return best;
	}

}
