package hr.fer.zemris.optjava.dz4.ga.impl;

import java.util.List;

import hr.fer.zemris.optjava.dz4.ga.model.ISingleEntity;
import hr.fer.zemris.optjava.dz4.ga.model.ISteadyPopulation;

public abstract class AbstractSteadyPopulation extends AbstractPopulation implements ISteadyPopulation {

	public AbstractSteadyPopulation(List<ISingleEntity> entities) {
		super(entities);
	}

	@Override
	public void setEntity(int i, ISingleEntity entity) {
		this.entities.set(i, entity);
	}

}
