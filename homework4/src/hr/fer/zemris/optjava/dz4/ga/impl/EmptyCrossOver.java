package hr.fer.zemris.optjava.dz4.ga.impl;

import hr.fer.zemris.optjava.dz4.ga.model.ICrossOver;
import hr.fer.zemris.optjava.dz4.ga.model.ISingleEntity;

import java.util.ArrayList;
import java.util.List;

public class EmptyCrossOver implements ICrossOver {

	@Override
	public List<ISingleEntity> crossOver(ISingleEntity entity1, ISingleEntity entity2) {
		ISingleEntity ent = entity1.fitness() > entity2.fitness() ? entity1.copy() : entity2.copy();
		List<ISingleEntity> list = new ArrayList<>();
		list.add(ent);
		return list;
	}

}
