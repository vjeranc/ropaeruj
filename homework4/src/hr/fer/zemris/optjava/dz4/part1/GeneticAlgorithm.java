package hr.fer.zemris.optjava.dz4.part1;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import hr.fer.zemris.optjava.dz2.function.SystemMinimizationFunction;
import hr.fer.zemris.optjava.dz2.function.TransferEquationFunction;
import hr.fer.zemris.optjava.dz2.function.model.IHFunction;
import hr.fer.zemris.optjava.dz4.ga.GeneticAlgorithmImpl;
import hr.fer.zemris.optjava.dz4.ga.impl.ProportionalSelection;
import hr.fer.zemris.optjava.dz4.ga.impl.SystemPopulation;
import hr.fer.zemris.optjava.dz4.ga.impl.TournamentSelection;
import hr.fer.zemris.optjava.dz4.ga.model.ISelection;

public class GeneticAlgorithm {
	
	private static Path filePath = Paths.get("./data/prijenosna/zad-prijenosna.txt");

	public static void main(String[] args) {
		Integer populationSize = Integer.parseInt(args[0]);
		Double minEps = Double.parseDouble(args[1]);
		Integer maxGeneration = Integer.parseInt(args[2]);
		ISelection selection = null;
		switch (args[3]) {
		case "rouletteWheel":
			selection = new ProportionalSelection();
			break;
		default: {
			int colon = args[3].indexOf(':');
			Integer tourSize = Integer.parseInt(args[3].substring(colon + 1));
			selection = new TournamentSelection(tourSize);
		}
		}
		
		Double sigma = Double.parseDouble(args[4]);
		
		IHFunction function = null;
		try {
			function = SystemMinimizationFunction.parseSystem(Files.readAllLines(filePath,
					StandardCharsets.UTF_8), new TransferEquationFunction(new double[]{1.0,1.0}));
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		
		SystemPopulation population = new SystemPopulation(function, populationSize);
		
		GeneticAlgorithmImpl ga = new GeneticAlgorithmImpl(population, minEps, maxGeneration, selection, sigma);
	
		ga.start();
	}
}
