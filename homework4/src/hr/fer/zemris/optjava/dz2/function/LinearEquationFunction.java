package hr.fer.zemris.optjava.dz2.function;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

public class LinearEquationFunction extends EquationFunction {

	
	public LinearEquationFunction(double[] coeff) {
		super(coeff);
	}
	
	@Override
	public double getEquationValueAt(RealVector point) {
		double sum = -y;
		for (int i = 0; i < coeff.length; i++) {
			sum += coeff[i] * point.getEntry(i);
		}
		return sum;
	}
	
	@Override
	public RealVector getSystemGradientComponent(RealVector point) {
		RealVector comp = new ArrayRealVector(getNumberOfVariables());
		double sum = getEquationValueAt(point);
		for (int i = 0; i < coeff.length; i++) {
			comp.addToEntry(i, 2*coeff[i]*sum);
		}
		return comp;
	}
	
	@Override
	public RealMatrix getSystemHessianComponent(RealVector point) {
		RealMatrix comp = new Array2DRowRealMatrix(getNumberOfVariables(), getNumberOfVariables());
		for (int i = 0; i < coeff.length; i++) {
			for (int j = 0; j < coeff.length; j++) {
				comp.addToEntry(i, j, 2*coeff[i]*coeff[j]);
			}
		}
		return comp;
	}

	@Override
	public RealVector getGradientValueAt(RealVector point) {
		return new ArrayRealVector(coeff);
	}

	@Override
	public RealMatrix getHessianMatrixOf(RealVector point) {
		return new Array2DRowRealMatrix(getNumberOfVariables(), getNumberOfVariables());
	}

	@Override
	public EquationFunction parseEquation(String line) {
		String[] nums = line.substring(1, line.length()-1).split(",");
		double[] numbers = new double[nums.length];
		for (int i = 0; i < nums.length; i++) {
			numbers[i] = Double.parseDouble(nums[i].trim());
		}
		return new LinearEquationFunction(numbers);
	}
}
