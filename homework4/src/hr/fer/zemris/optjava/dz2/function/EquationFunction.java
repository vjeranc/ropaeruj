package hr.fer.zemris.optjava.dz2.function;

import java.util.Arrays;

import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import hr.fer.zemris.optjava.dz2.function.model.IHFunction;

public abstract class EquationFunction implements IHFunction {

	protected double[] coeff;
	protected double y;
	
	public EquationFunction(double[] coeff) {
		this.coeff = Arrays.copyOf(coeff, coeff.length-1);
		this.y = coeff[coeff.length-1];
	}
	
	@Override
	public int getNumberOfVariables() {
		return coeff.length;
	}

	@Override
	public double getValueAt(RealVector point) {
		double sum = getEquationValueAt(point);
		return sum*sum;
	}
	
	public abstract double getEquationValueAt(RealVector point);
	
	public abstract RealVector getSystemGradientComponent(RealVector point);
	
	public abstract RealMatrix getSystemHessianComponent(RealVector point);

	@Override
	public abstract RealVector getGradientValueAt(RealVector point);

	@Override
	public abstract RealMatrix getHessianMatrixOf(RealVector point);

	public abstract EquationFunction parseEquation(String line);
}
