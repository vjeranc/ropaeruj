package hr.fer.zemris.optjava.dz3;

public abstract class SingleObjectiveSolution {

	public double fitness;
	public double value;
	
	public SingleObjectiveSolution() {
	}
	
	int compareTo(SingleObjectiveSolution solution) {
		return (int) (this.fitness - solution.fitness);
	};
}
