package hr.fer.zemris.optjava.dz2.function.model;

import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

public interface IHFunction extends IFunction {

	RealMatrix getHessianMatrixOf(RealVector point);
	
}
