package hr.fer.zemris.optjava.dz2.function.model;

import org.apache.commons.math3.linear.RealVector;

public interface IFunction {

	int getNumberOfVariables();
	
	double getValueAt(RealVector point);
	
	RealVector getGradientValueAt(RealVector point);
	
}
