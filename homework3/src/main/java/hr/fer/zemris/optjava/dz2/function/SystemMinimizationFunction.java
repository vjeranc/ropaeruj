package hr.fer.zemris.optjava.dz2.function;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import hr.fer.zemris.optjava.dz2.function.model.IHFunction;

/**
 * Function of a system of equation.
 * 
 * @author vjeran
 * 
 */
public class SystemMinimizationFunction implements IHFunction {

	private EquationFunction[] functions;

	public SystemMinimizationFunction(EquationFunction... functions) {
		this.functions = functions;
	}

	@Override
	public int getNumberOfVariables() {
		return functions[0].getNumberOfVariables();
	}

	@Override
	public double getValueAt(RealVector point) {
		double sum = 0.0;
		for (int i = 0; i < functions.length; i++) {
			sum += functions[i].getValueAt(point);
		}
		return sum;
	}

	@Override
	public RealVector getGradientValueAt(RealVector point) {
		RealVector gradient = new ArrayRealVector(getNumberOfVariables());
		for (int i = 0; i < functions.length; i++) {
			gradient.combineToSelf(1.0, 1.0, functions[i].getSystemGradientComponent(point));
		}
		return gradient;
	}

	@Override
	public RealMatrix getHessianMatrixOf(RealVector point) {
		RealMatrix hessian = new Array2DRowRealMatrix(getNumberOfVariables(), getNumberOfVariables());
		for (int i = 0; i < functions.length; i++) {
			hessian = hessian.add(functions[i].getSystemHessianComponent(point));
		}

		return hessian;
	}

	public static SystemMinimizationFunction parseSystem(String input, EquationFunction parser) {
		BufferedReader br = new BufferedReader(new StringReader(input));
		List<EquationFunction> functions = new ArrayList<>();
		while (true) {
			try {
				String line = br.readLine();
				if (line == null || line.isEmpty()) {
					break;
				}
				if (line.charAt(0) != '[') {
					continue;
				}
				functions.add(parser.parseEquation(line));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return new SystemMinimizationFunction(functions.toArray(new EquationFunction[0]));
	}

	public static SystemMinimizationFunction parseSystem(Collection<String> lines, EquationFunction parser) {
		List<EquationFunction> functions = new ArrayList<>();
		for (String line : lines) {

			if (line.isEmpty()) {
				break;
			}
			if (line.charAt(0) != '[') {
				continue;
			}
			functions.add(parser.parseEquation(line));

		}

		return new SystemMinimizationFunction(functions.toArray(new EquationFunction[0]));
	}

}
