package hr.fer.zemris.optjava.dz3;

import java.util.Arrays;
import java.util.Random;

public class DoubleArraySolution extends SingleObjectiveSolution {

	public double[] values;
	
	public DoubleArraySolution(int n) {
		values = new double[n];
	}
	
	private DoubleArraySolution(double[] values) {
		this.values = values;
	}
	
	DoubleArraySolution newLikeThis() {
		return new DoubleArraySolution(Arrays.copyOf(values, values.length));
	}
	
	void randomize(Random rand, double[] deltas, double[] array2) {
		
	}
	
}
