package hr.fer.zemris.optjava.dz2.function;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import hr.fer.zemris.optjava.dz2.function.model.IHFunction;

public class Function2 implements IHFunction {

	public Function2() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getNumberOfVariables() {
		return 2;
	}

	@Override
	public double getValueAt(RealVector point) {
		double x1 = point.getEntry(0);
		double x2 = point.getEntry(1);

		return (x1 - 1) * (x1 - 1) + 10 * (x2 - 2) * (x2 - 2);
	}

	@Override
	public RealVector getGradientValueAt(RealVector point) {
		double x1 = point.getEntry(0);
		double x2 = point.getEntry(1);

		return new ArrayRealVector(new double[] { 2*(x1 - 1), 20 * (x2 - 2) });
	}

	@Override
	public RealMatrix getHessianMatrixOf(RealVector point) {
		return new Array2DRowRealMatrix(new double[][] { { 2.0, 0.0 },
				                                         { 0.0, 20.0 } });
	}

}
