package hr.fer.zemris.optjava.moop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.Set;

public class NGSA2 {
	private int popSize;
	private boolean decision; // decision space or objective space distancing
	private List<Unit> population;
	private int maxiter;
	TournamentSelection selection;
	ICrossOver crossover;
	IMutation mutation;
	MOOPProblem pp;
	
	
	
	public NGSA2(int popSize, TournamentSelection selection, ICrossOver crossover, IMutation mutation, MOOPProblem pp, int maxiter) {
		super();
		this.popSize = popSize;
		this.pp = pp;
		this.population = initializePopulation(popSize, pp.getNumberOfArguments(), pp.getNumberOfObjectives());
		this.selection = selection;
		this.crossover = crossover;
		this.mutation = mutation;
		this.maxiter = maxiter;
	}

	public List<Unit> start() {
		
		int iter = 0;
		// first iteration
		List<Unit> Pt = new ArrayList<>(population);
		for (Unit unit : population) {
			unit.evaluate(pp);
		}
		constructParetoSets(Pt); // just need it for non-domination ranks
		List<Unit> Qt = createNewPopulation(Pt);
		while (iter++ < maxiter) {
			List<Unit> Rt = new ArrayList<>();
			Rt.addAll(Pt);
			Rt.addAll(Qt);
			for (Unit unit : Rt) {
				unit.evaluate(pp);
			}
			List<List<Unit>> fronts = constructParetoSets(Rt);
			List<Unit> nPt = new ArrayList<>();
			int k = fronts.size();
			for (int i = 0; i < k; i++) {
				List<Unit> front = fronts.get(i);
				calculateCrowdingDistance(front);
				int diff = nPt.size() + front.size() - popSize;
				if (diff < 0) nPt.addAll(front);
				else {
					Collections.sort(front, Collections.reverseOrder(Unit.getCrowdingCompare()));
					nPt.addAll(front.subList(0, front.size()-diff));
					break;
				}
			}
			List<Unit> nQt = new ArrayList<>(createNewPopulation(nPt));
			Pt = population = nPt;
			Qt = nQt;
		}
		//population.addAll(Qt);
		for (Unit unit : population) {
			unit.evaluate(pp);
		}
		return constructParetoSets(population).get(0);
	}


	private void calculateCrowdingDistance(List<Unit> front) {
		int n = pp.getNumberOfObjectives();
		int l = front.size();
		double[] max = pp.getMaxBounds();
		double[] min = pp.getMinBounds();
		for (Unit u : front) {
			u.setCrowdingDistance(0.0);
		}
		for(int i = 0; i < n; i++) {
			Collections.sort(front, Unit.getIthCompare(i));
			front.get(0).setCrowdingDistance(Double.POSITIVE_INFINITY);
			front.get(l-1).setCrowdingDistance(Double.POSITIVE_INFINITY);
			for(int j = 1; j < l-1; j++) {
				double prevCd = front.get(j).getCrowdingDistance();
				double prev = front.get(j-1).getObjectives()[i];
				double succ = front.get(j+1).getObjectives()[i];
				front.get(j).setCrowdingDistance(prevCd + (succ - prev)/(max[i] - min[i]));
			}
		}
		
	}

	private List<Unit> createNewPopulation(List<Unit> pop) {
		List<Unit> npop = new ArrayList<>();
	
		while(npop.size() < popSize) {
			List<Unit> parents = selection.selectFrom(pop,2);
			Unit parent1 = parents.get(0);
			Unit parent2 = parents.get(1);
			List<Unit> children = crossover.crossOver(parent1, parent2);
			mutateChildren(children); // mutation
			npop.addAll(children);
		}
		
		return npop;
	}

	private void mutateChildren(List<Unit> children) {
		for (Unit unit : children) {
			mutation.mutate(unit, pp.getMinBounds(), pp.getMaxBounds());
		}
		
	}

	/**
	 * Sets the non-domination degree and constructs Pareto fronts.
	 * @param pop of population
	 * @return fronts
	 */
	private List<List<Unit>> constructParetoSets(List<Unit> pop) {
		Map<Integer, Set<Integer>> sets = new HashMap<>();
		
		int n = pop.size();
		int[] nondominationDegree = new int[n];
		for(int i = 0; i < n; i++) {
			sets.put(i, new HashSet<Integer>());
		}
		
		for (int i = 0; i < n; i++) {
			Unit unit1 = pop.get(i);
			for (int j = 0; j < n; j++) {
				Unit unit2 = pop.get(j);
				if (unit1 != unit2) {
					if (unit2.dominates(unit1)) {
						sets.get(i).add(j);
						nondominationDegree[i]++;
					}
				}
			}
		}
		
		setFitnessToNonDominationDegree(pop, nondominationDegree);
		
		// O(M*N^2) 
		List<List<Integer>> fronts = new ArrayList<>();
		// after we have for each unit a list of its dominators, we'll find the first front and continue from there
		int k = 0; // front index
		while(!sets.isEmpty()) {
			// building a front
			for (Entry<Integer, Set<Integer>> entry : sets.entrySet()) {
				Set<Integer> set = entry.getValue();
				if (set.isEmpty()) {
					Integer key = entry.getKey();
					if (fronts.size() == k) { // if front doesn't exist
						fronts.add(k, new ArrayList<Integer>());
					}
					fronts.get(k).add(key);
				}
			}
			if (fronts.size() > k) {
				// removing empty sets of added elements
				for (Integer num : fronts.get(k)) {
					sets.remove(num);
				}
				// removing elements in created front from sets
				for (Entry<Integer, Set<Integer>> entry : sets.entrySet()) {
					Set<Integer> set = entry.getValue();
					for (Integer num : fronts.get(k)) {
						set.remove(num);
					}
				}
			}
			k++; // next front
		}
		
		// mapping integer to Unit
		List<List<Unit>> frontsR = new ArrayList<>();
		int j = 0;
		for (List<Integer> list : fronts) {
			frontsR.add(new ArrayList<Unit>());
			for(Integer num : list) {
				frontsR.get(j).add(pop.get(num));
			}
			j++;
		}
		// mapToUnit = map (map (get population)), beautiful haskell sintax, where get population index is the function
		return frontsR;
	}

	private void setFitnessToNonDominationDegree(List<Unit> population2, int[] nondominationDegree) {
		int n = population2.size();
		for(int i = 0; i < n; i++) {
			population2.get(i).setDD(nondominationDegree[i]);
		}
		
	}

	private List<Unit> initializePopulation(int popSize2, int n, int m) {
		List<Unit> pop = new ArrayList<>();
		for(int i = 0; i < popSize2; i++) {
			pop.add(new Unit(n, m, 
					pp.getMinBounds(), 
					pp.getMaxBounds())
			);
		}
		return pop;
	}

}
