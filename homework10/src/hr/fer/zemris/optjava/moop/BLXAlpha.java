package hr.fer.zemris.optjava.moop;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BLXAlpha implements ICrossOver {

	private static Random rand = new Random();
	
	@Override
	public List<Unit> crossOver(Unit u1, Unit u2) {
		int n = u1.solution.length;
		double[] childSol = new double[n];
		for (int i = 0; i < n; i++) {
			double c1 = u1.solution[i];
			double c2 = u2.solution[i];
			double min = c1 > c2 ? c2 : c1;
			double max = c1 > c2 ? c1 : c2;
			double I = max - min;

			double val = getRandomFromInterval(rand, min, max);
			childSol[i] = val;
		}
		List<Unit> child = new ArrayList<>();
		child.add(new Unit(childSol, u1.objectives.length));
		return child;
	}
	
	private double getRandomFromInterval(Random rand, double minimum, double maximum) {
		return (minimum + (maximum - minimum)*rand.nextDouble());
	}

}
