package hr.fer.zemris.optjava.moop;

import java.util.Random;

public class FieldMutation implements IMutation {

	private static Random rand = new Random();
	
	@Override
	public void mutate(Unit u, double[] minBounds, double[] maxBounds) {
		int i = rand.nextInt(u.solution.length);
		
		u.solution[i] += rand.nextGaussian();
		if (u.solution[i] < minBounds[i]) u.solution[i] = minBounds[i];
		if (u.solution[i] > maxBounds[i]) u.solution[i] = maxBounds[i];
	}

}
