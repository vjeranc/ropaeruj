package hr.fer.zemris.optjava.moop;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

public class Unit implements Comparable<Unit> {

	double[] solution;
	double[] objectives;
	private Double crowdingDistance;
	/* Domination degree */
	private Integer dd;

	private static Random rand = new Random();
	
	/**
	 * 
	 * @param n size of domain
	 * @param m size of codomain
	 */
	public Unit(int n, int m, double[] minBounds, double[] maxBounds) {
		this.solution = random(new double[n], minBounds, maxBounds);
		this.objectives = new double[m];
		this.dd = Integer.MAX_VALUE;
		this.crowdingDistance = 0.0;
	}
	
	private double[] random(double[] ds, double[] minBounds, double[] maxBounds) {
		for(int i = 0; i < ds.length; i++) {
			ds[i] = getRandomFromInterval(rand, minBounds[i], maxBounds[i]);
		}
		return ds;
	}
	
	private double getRandomFromInterval(Random rand, double minimum, double maximum) {
		return (minimum + (maximum - minimum)*rand.nextDouble());
	}

	Unit(double[] solution, int m) {
		this.solution = solution;
		this.objectives = new double[m];
		this.dd = Integer.MAX_VALUE;
		this.crowdingDistance = 0.0;
	}

	public void evaluate(MOOPProblem pp) {
		pp.evaluateSolution(solution, objectives);
	}
	public double getFitness() {
		return dd;
	}
	
	public void setFitness(double num) {
		dd =(int) num;
	}

	
	public boolean dominates(Unit that) {
		int count = 0;
		for(int i = 0; i < objectives.length; i++) {
			if (this.objectives[i] > that.objectives[i]) {
				return false; // this means we cannot decide
			}
			if (this.objectives[i] == that.objectives[i]) {
				count++;
			}
		}
		return count != objectives.length; // this means that this dominates that, if they are equal then there's no domination
	}

	@Override
	public int compareTo(Unit that) {
		return this.dd.compareTo(that.dd);
	}

	public static Comparator<Unit> getIthCompare(final int i) {
		return new Comparator<Unit>() {

			@Override
			public int compare(Unit o1, Unit o2) {
				return Double.valueOf(o1.objectives[i]).compareTo(o2.objectives[i]);
			}
		};
	}
	
	public static Comparator<Unit> getCrowdingCompare() {
		return new Comparator<Unit>() {

			@Override
			public int compare(Unit o1, Unit o2) {
				return o1.crowdingDistance.compareTo(o2.crowdingDistance);
			}
		};
	}
	
	public double[] getSolution() {
		return solution;
	}
	
	public double[] getObjectives() {
		return objectives;
	}
	
	public int getDD() {
		return dd;
	}

	public void setDD(int i) {
		this.dd = i;
	}

	public Double getCrowdingDistance() {
		return crowdingDistance;
	}

	public void setCrowdingDistance(Double crowdingDistance) {
		this.crowdingDistance = crowdingDistance;
	}
	
	@Override
	public String toString() {
		return Arrays.toString(objectives) + dd + " " + crowdingDistance ;
	}
	
	
}
