package hr.fer.zemris.optjava.moop;

public interface MOOPProblem {
	
	int getNumberOfArguments();
	
	int getNumberOfObjectives();

	void evaluateSolution(double[] solution, double[] objectives);
	
	double[] getMaxBounds();
	
	double[] getMinBounds();
}
