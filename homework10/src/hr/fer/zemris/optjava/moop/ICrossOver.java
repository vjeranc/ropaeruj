package hr.fer.zemris.optjava.moop;

import java.util.List;

public interface ICrossOver {
	List<Unit> crossOver(Unit u1, Unit u2);
}
