package hr.fer.zemris.optjava.moop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class TournamentSelection {

	private static final Random rand = new Random();
	int k;
	public TournamentSelection(int size) {
		this.k = size;
	}
	
	/**
	 * Selects {@code b} best from population. 
	 * @param population
	 * @param k tournament size
	 * @param b
	 * @return
	 */
	public List<Unit> selectFrom(List<Unit> population, int b) {
		List<Unit> tournament = new ArrayList<>(population);
		Collections.shuffle(tournament);
		tournament = tournament.subList(0, b);

		Collections.sort(tournament);
		
		return tournament.subList(0, b);
	}

}
