package hr.fer.zemris.optjava.moop;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PMXCrossOver implements ICrossOver {

	private static final Random rand = new Random();
	
	@Override
	public List<Unit> crossOver(Unit u1, Unit u2) {
		int n = u1.solution.length;
		double[] childSol = new double[n];
		for (int i = 0; i < n; i++) {
			double c1 = u1.solution[i];
			double c2 = u2.solution[i];
			childSol[i] = rand.nextBoolean() ? c1 : c2;
		}
		List<Unit> child = new ArrayList<>();
		child.add(new Unit(childSol, u1.objectives.length));
		return child;
	}

}
