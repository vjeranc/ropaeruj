package hr.fer.zemris.optjava.dz10;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hr.fer.zemris.optjava.moop.BLXAlpha;
import hr.fer.zemris.optjava.moop.FieldMutation;
import hr.fer.zemris.optjava.moop.ICrossOver;
import hr.fer.zemris.optjava.moop.IMutation;
import hr.fer.zemris.optjava.moop.ISelection;
import hr.fer.zemris.optjava.moop.MOOPProblem;
import hr.fer.zemris.optjava.moop.NGSA2;
import hr.fer.zemris.optjava.moop.PMXCrossOver;
import hr.fer.zemris.optjava.moop.Problem1;
import hr.fer.zemris.optjava.moop.Problem2;
import hr.fer.zemris.optjava.moop.ProportionalSelection;
import hr.fer.zemris.optjava.moop.TournamentSelection;
import hr.fer.zemris.optjava.moop.Unit;

public class MOOP {
	
	public static Path file1 = Paths.get("izlaz-dec.txt");
	public static Path file2 = Paths.get("izlaz-objective.txt");
	public static void main(String[] args) throws IOException {

		Integer f = Integer.parseInt(args[0]);
		Integer popSize = Integer.parseInt(args[1]);
		Integer maxiter = Integer.parseInt(args[2]);
		
		TournamentSelection selection = new TournamentSelection(55);
		ICrossOver crossover = new PMXCrossOver();
		IMutation mutation = new FieldMutation();
		MOOPProblem pp = f == 1 ? new Problem1() : new Problem2();
				
		NGSA2 ga = new NGSA2(popSize, selection, crossover, mutation, pp, maxiter); 				
		
		List<Unit> solutions = ga.start();
		
		printSolutions(solutions);
	}
	private static void printSolutions(List<Unit> solutions) throws IOException {
		List<String> lines1 = mapToSolLines(solutions);
		List<String> lines2 = mapToObjLines(solutions);
		
		Files.write(file1, lines1, StandardCharsets.UTF_8);
		Files.write(file2, lines2,  StandardCharsets.UTF_8);
		
	}
	private static List<String> mapToObjLines(List<Unit> solutions) {
		List<String> list = new ArrayList<>();
		for (Unit sol : solutions) {
			String line = Arrays.toString(sol.getObjectives());
			line = line.substring(1, line.length()-1);
			line = line.replace(',', '\t');
			line = line.replace(" ", "");
			list.add(line);
		}
		return list;
	}
	private static List<String> mapToSolLines(List<Unit> solutions) {
		List<String> list = new ArrayList<>();
		for (Unit sol : solutions) {
			String line = Arrays.toString(sol.getSolution());
			line = line.substring(1, line.length()-1);
			line = line.replace(',', '\t');
			line = line.replace(" ", "");
			list.add(line);
		}
		return list;
	}
}
